module gitlab.com/beoran/ctogo

go 1.12

require (
	github.com/pointlander/compress v1.1.0 // indirect
	github.com/pointlander/jetset v1.0.0 // indirect
	github.com/pointlander/peg v1.0.0
	github.com/yhirose/go-peg v0.0.0-20190710015414-7eb2cf046928 // indirect
)
