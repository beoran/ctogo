package c

import "testing"
import "fmt"

func TestParse(t *testing.T) {
	parser, err := ParseFile("testdata/xrandr.c")
	if err != nil {
		t.Fatalf("Parse Error: %s", err)
	}
	
	parser.WalkAst(func(c *C, node * Node) bool {
		fmt.Printf("%s, %s\n", node.Kind(), node.Value(c.buffer))
		return true
	})
  
	parser.WalkerAst(TestWalker)
	// t.Logf("Parser: %v", parser)
}

func HandleStatement(c * C, node * Node) bool {
	fmt.Printf("%s\n", node.Value(c.buffer))
	return true
}

func HandlePreprocessing(c * C, node * Node) bool {
	fmt.Printf("## %s\n", node.Value(c.buffer))
	return true
}


var TestWalker = BasicWalker {
	/* "Statement": HandleStatement, */
	"Preprocessing" : HandlePreprocessing,
}



