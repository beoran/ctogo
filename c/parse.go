package c

import "io/ioutil"
// import "fmt"

type Node = node32
type Token = token32 

func (n *Node) Value(buffer []rune) string {
	return string((buffer[n.begin:n.end]))
}

func (n *Node) Kind() string {
	return rul3s[n.pegRule]
}


func ParseBuffer(buffer string) (*C, error) {
	parser := &C{ Buffer : buffer }
	parser.Init()
	err := parser.Parse(int(ruleTranslationUnit))
	if err != nil {
		return nil, err
	}
	return parser, nil
}

func ParseFile(name string) (*C, error) {
	cont, err := ioutil.ReadFile(name)
	if err != nil {
		return nil, err
	}
	return ParseBuffer(string(cont))
}


func (c *C) WalkAstRec(ast *Node, walker func(c *C, n * Node) bool) {
	node := ast 
	for node != nil {
		if !walker(c, node) { 
			return;
		}
		c.WalkAstRec(node.up, walker)
		
		node = node.next
	}
}

func (c *C) WalkAst(walker func(c *C, n * Node) bool) {
	c.WalkAstRec(c.AST(), walker)
}

type Walker interface {
	Handler(kind string) (func(c *C, n *Node) bool)
}

func (c *C) WalkerAstRec(ast *Node, walker Walker) {
	node := ast 
	for node != nil {
		handler := walker.Handler(node.Kind())
		if handler != nil { 
			if !handler(c, node) { 
				return;
			}
		}
		c.WalkerAstRec(node.up, walker)
		node = node.next
	}
}

func (c *C) WalkerAst(walker Walker) {
	c.WalkerAstRec(c.AST(), walker)
}

type BasicWalker map[string] func(c*C, node*Node) bool

func (bw BasicWalker) Handler(kind string) (func(c*C, node*Node) bool) {
	handler, ok := bw[kind]
	if ok {
		return handler
	}
	return nil 
}




