/*
#include "allegro5/allegro.h"
#include "allegro5/internal/aintern_x.h"
#include "allegro5/internal/aintern_xdisplay.h"
#include "allegro5/internal/aintern_xfullscreen.h"
#include "allegro5/internal/aintern_xsystem.h"

#ifdef ALLEGRO_XWINDOWS_WITH_XRANDR
*/

typedef struct xrandr_screen xrandr_screen;
typedef struct xrandr_crtc xrandr_crtc;
typedef struct xrandr_output xrandr_output;
typedef struct xrandr_mode xrandr_mode;

struct xrandr_screen {
   int id;
   Time timestamp;
   Time configTimestamp;
   _AL_VECTOR crtcs; // xrandr_crtc
   _AL_VECTOR outputs; // xrandr_output
   _AL_VECTOR modes; // xrandr_mode
   
   XRRScreenResources *res;
};

enum xrandr_crtc_position {
   CRTC_POS_NONE = 0,
   CRTC_POS_ABOVE,
   CRTC_POS_LEFTOF,
   CRTC_POS_BELOW,
   CRTC_POS_RIGHTOF,
};

struct xrandr_crtc {
   RRCrtc id;
   Time timestamp;
   int x, y;
   unsigned int width, height;
   RRMode mode;
   Rotation rotation;
   _AL_VECTOR connected;
   _AL_VECTOR possible;
   
   RRMode original_mode;
   int original_xoff;
   int original_yoff;
   RRCrtc align_to;
   int align;
};

struct xrandr_output {
   RROutput id;
   Time timestamp;
   RRCrtc crtc;
   char *name;
   int namelen;
   unsigned long mm_width;
   unsigned long mm_height;
   Connection connection;
   SubpixelOrder subpixel_order;
   _AL_VECTOR crtcs; // RRCrtc
   _AL_VECTOR clones; // RROutput
   RRMode prefered_mode;
   _AL_VECTOR modes; // RRMode
};

struct xrandr_mode {
   RRMode id;
   unsigned int width;
   unsigned int height;
   unsigned int refresh;
};

struct xrandr_rect {
   int x1;
   int y1;
   int x2;
   int y2;
};

