package c

import (
	"fmt"
	"math"
	"sort"
	"strconv"
)

const endSymbol rune = 1114112

/* The rule types inferred from the grammar are below. */
type pegRule uint8

const (
	ruleUnknown pegRule = iota
	ruleTranslationUnit
	ruleExternalDeclaration
	ruleFunctionDefinition
	ruleDeclarationList
	rulePreprocessing
	rulePPSpacing
	rulePPInstruction
	rulePPDefine
	rulePPElse
	rulePPEndif
	rulePPIf
	rulePPIfdef
	rulePPInclude
	rulePPLine
	rulePPPragma
	ruleDeclaration
	ruleDeclarationSpecifiers
	ruleInitDeclaratorList
	ruleInitDeclarator
	ruleStorageClassSpecifier
	ruleTypeSpecifier
	ruleStructOrUnionSpecifier
	ruleStructOrUnion
	ruleStructDeclaration
	ruleSpecifierQualifierList
	ruleStructDeclaratorList
	ruleStructDeclarator
	ruleEnumSpecifier
	ruleEnumeratorList
	ruleEnumerator
	ruleTypeQualifier
	ruleFunctionSpecifier
	ruleDeclarator
	ruleDirectDeclarator
	rulePointer
	ruleParameterTypeList
	ruleParameterList
	ruleParameterDeclaration
	ruleIdentifierList
	ruleTypeName
	ruleAbstractDeclarator
	ruleDirectAbstractDeclarator
	ruleTypedefName
	ruleInitializer
	ruleInitializerList
	ruleDesignation
	ruleDesignator
	ruleStatement
	ruleLabeledStatement
	ruleCompoundStatement
	ruleExpressionStatement
	ruleSelectionStatement
	ruleIterationStatement
	ruleJumpStatement
	rulePrimaryExpression
	rulePostfixExpression
	ruleArgumentExpressionList
	ruleUnaryExpression
	ruleUnaryOperator
	ruleCastExpression
	ruleMultiplicativeExpression
	ruleAdditiveExpression
	ruleShiftExpression
	ruleRelationalExpression
	ruleEqualityExpression
	ruleANDExpression
	ruleExclusiveORExpression
	ruleInclusiveORExpression
	ruleLogicalANDExpression
	ruleLogicalORExpression
	ruleConditionalExpression
	ruleAssignmentExpression
	ruleAssignmentOperator
	ruleExpression
	ruleConstantExpression
	ruleSpacing
	ruleWhiteSpace
	ruleLongComment
	ruleLineComment
	ruleAUTO
	ruleBREAK
	ruleCASE
	ruleCHAR
	ruleCONST
	ruleCONTINUE
	ruleDEFAULT
	ruleDOUBLE
	ruleDO
	ruleELSE
	ruleENUM
	ruleEXTERN
	ruleFLOAT
	ruleFOR
	ruleGOTO
	ruleIF
	ruleINT
	ruleINLINE
	ruleLONG
	ruleREGISTER
	ruleRESTRICT
	ruleRETURN
	ruleSHORT
	ruleSIGNED
	ruleSIZEOF
	ruleSTATIC
	ruleSTRUCT
	ruleSWITCH
	ruleTYPEDEF
	ruleUNION
	ruleUNSIGNED
	ruleVOID
	ruleVOLATILE
	ruleWHILE
	ruleBOOL
	ruleCOMPLEX
	ruleSTDCALL
	ruleDECLSPEC
	ruleATTRIBUTE
	ruleKeyword
	ruleIdentifier
	ruleIdNondigit
	ruleIdChar
	ruleUniversalCharacter
	ruleHexQuad
	ruleConstant
	ruleIntegerConstant
	ruleDecimalConstant
	ruleOctalConstant
	ruleHexConstant
	ruleHexPrefix
	ruleHexDigit
	ruleIntegerSuffix
	ruleLsuffix
	ruleFloatConstant
	ruleDecimalFloatConstant
	ruleHexFloatConstant
	ruleFraction
	ruleHexFraction
	ruleExponent
	ruleBinaryExponent
	ruleFloatSuffix
	ruleEnumerationConstant
	ruleCharacterConstant
	ruleChar
	ruleEscape
	ruleSimpleEscape
	ruleOctalEscape
	ruleHexEscape
	ruleStringLiteral
	ruleStringChar
	ruleLBRK
	ruleRBRK
	ruleLPAR
	ruleRPAR
	ruleLWING
	ruleRWING
	ruleDOT
	rulePTR
	ruleINC
	ruleDEC
	ruleAND
	ruleSTAR
	rulePLUS
	ruleMINUS
	ruleTILDA
	ruleBANG
	ruleDIV
	ruleMOD
	ruleLEFT
	ruleRIGHT
	ruleLT
	ruleGT
	ruleLE
	ruleGE
	ruleEQUEQU
	ruleBANGEQU
	ruleHAT
	ruleOR
	ruleANDAND
	ruleOROR
	ruleQUERY
	ruleCOLON
	ruleSEMI
	ruleELLIPSIS
	ruleEQU
	ruleSTAREQU
	ruleDIVEQU
	ruleMODEQU
	rulePLUSEQU
	ruleMINUSEQU
	ruleLEFTEQU
	ruleRIGHTEQU
	ruleANDEQU
	ruleHATEQU
	ruleOREQU
	ruleCOMMA
	ruleEOT
	rulePegText

	rulePre
	ruleIn
	ruleSuf
)

var rul3s = [...]string{
	"Unknown",
	"TranslationUnit",
	"ExternalDeclaration",
	"FunctionDefinition",
	"DeclarationList",
	"Preprocessing",
	"PPSpacing",
	"PPInstruction",
	"PPDefine",
	"PPElse",
	"PPEndif",
	"PPIf",
	"PPIfdef",
	"PPInclude",
	"PPLine",
	"PPPragma",
	"Declaration",
	"DeclarationSpecifiers",
	"InitDeclaratorList",
	"InitDeclarator",
	"StorageClassSpecifier",
	"TypeSpecifier",
	"StructOrUnionSpecifier",
	"StructOrUnion",
	"StructDeclaration",
	"SpecifierQualifierList",
	"StructDeclaratorList",
	"StructDeclarator",
	"EnumSpecifier",
	"EnumeratorList",
	"Enumerator",
	"TypeQualifier",
	"FunctionSpecifier",
	"Declarator",
	"DirectDeclarator",
	"Pointer",
	"ParameterTypeList",
	"ParameterList",
	"ParameterDeclaration",
	"IdentifierList",
	"TypeName",
	"AbstractDeclarator",
	"DirectAbstractDeclarator",
	"TypedefName",
	"Initializer",
	"InitializerList",
	"Designation",
	"Designator",
	"Statement",
	"LabeledStatement",
	"CompoundStatement",
	"ExpressionStatement",
	"SelectionStatement",
	"IterationStatement",
	"JumpStatement",
	"PrimaryExpression",
	"PostfixExpression",
	"ArgumentExpressionList",
	"UnaryExpression",
	"UnaryOperator",
	"CastExpression",
	"MultiplicativeExpression",
	"AdditiveExpression",
	"ShiftExpression",
	"RelationalExpression",
	"EqualityExpression",
	"ANDExpression",
	"ExclusiveORExpression",
	"InclusiveORExpression",
	"LogicalANDExpression",
	"LogicalORExpression",
	"ConditionalExpression",
	"AssignmentExpression",
	"AssignmentOperator",
	"Expression",
	"ConstantExpression",
	"Spacing",
	"WhiteSpace",
	"LongComment",
	"LineComment",
	"AUTO",
	"BREAK",
	"CASE",
	"CHAR",
	"CONST",
	"CONTINUE",
	"DEFAULT",
	"DOUBLE",
	"DO",
	"ELSE",
	"ENUM",
	"EXTERN",
	"FLOAT",
	"FOR",
	"GOTO",
	"IF",
	"INT",
	"INLINE",
	"LONG",
	"REGISTER",
	"RESTRICT",
	"RETURN",
	"SHORT",
	"SIGNED",
	"SIZEOF",
	"STATIC",
	"STRUCT",
	"SWITCH",
	"TYPEDEF",
	"UNION",
	"UNSIGNED",
	"VOID",
	"VOLATILE",
	"WHILE",
	"BOOL",
	"COMPLEX",
	"STDCALL",
	"DECLSPEC",
	"ATTRIBUTE",
	"Keyword",
	"Identifier",
	"IdNondigit",
	"IdChar",
	"UniversalCharacter",
	"HexQuad",
	"Constant",
	"IntegerConstant",
	"DecimalConstant",
	"OctalConstant",
	"HexConstant",
	"HexPrefix",
	"HexDigit",
	"IntegerSuffix",
	"Lsuffix",
	"FloatConstant",
	"DecimalFloatConstant",
	"HexFloatConstant",
	"Fraction",
	"HexFraction",
	"Exponent",
	"BinaryExponent",
	"FloatSuffix",
	"EnumerationConstant",
	"CharacterConstant",
	"Char",
	"Escape",
	"SimpleEscape",
	"OctalEscape",
	"HexEscape",
	"StringLiteral",
	"StringChar",
	"LBRK",
	"RBRK",
	"LPAR",
	"RPAR",
	"LWING",
	"RWING",
	"DOT",
	"PTR",
	"INC",
	"DEC",
	"AND",
	"STAR",
	"PLUS",
	"MINUS",
	"TILDA",
	"BANG",
	"DIV",
	"MOD",
	"LEFT",
	"RIGHT",
	"LT",
	"GT",
	"LE",
	"GE",
	"EQUEQU",
	"BANGEQU",
	"HAT",
	"OR",
	"ANDAND",
	"OROR",
	"QUERY",
	"COLON",
	"SEMI",
	"ELLIPSIS",
	"EQU",
	"STAREQU",
	"DIVEQU",
	"MODEQU",
	"PLUSEQU",
	"MINUSEQU",
	"LEFTEQU",
	"RIGHTEQU",
	"ANDEQU",
	"HATEQU",
	"OREQU",
	"COMMA",
	"EOT",
	"PegText",

	"Pre_",
	"_In_",
	"_Suf",
}

type node32 struct {
	token32
	up, next *node32
}

func (node *node32) print(depth int, buffer string) {
	for node != nil {
		for c := 0; c < depth; c++ {
			fmt.Printf(" ")
		}
		fmt.Printf("\x1B[34m%v\x1B[m %v\n", rul3s[node.pegRule], strconv.Quote(string(([]rune(buffer)[node.begin:node.end]))))
		if node.up != nil {
			node.up.print(depth+1, buffer)
		}
		node = node.next
	}
}

func (node *node32) Print(buffer string) {
	node.print(0, buffer)
}

type element struct {
	node *node32
	down *element
}

/* ${@} bit structure for abstract syntax tree */
type token32 struct {
	pegRule
	begin, end, next uint32
}

func (t *token32) isZero() bool {
	return t.pegRule == ruleUnknown && t.begin == 0 && t.end == 0 && t.next == 0
}

func (t *token32) isParentOf(u token32) bool {
	return t.begin <= u.begin && t.end >= u.end && t.next > u.next
}

func (t *token32) getToken32() token32 {
	return token32{pegRule: t.pegRule, begin: uint32(t.begin), end: uint32(t.end), next: uint32(t.next)}
}

func (t *token32) String() string {
	return fmt.Sprintf("\x1B[34m%v\x1B[m %v %v %v", rul3s[t.pegRule], t.begin, t.end, t.next)
}

type tokens32 struct {
	tree    []token32
	ordered [][]token32
}

func (t *tokens32) trim(length int) {
	t.tree = t.tree[0:length]
}

func (t *tokens32) Print() {
	for _, token := range t.tree {
		fmt.Println(token.String())
	}
}

func (t *tokens32) Order() [][]token32 {
	if t.ordered != nil {
		return t.ordered
	}

	depths := make([]int32, 1, math.MaxInt16)
	for i, token := range t.tree {
		if token.pegRule == ruleUnknown {
			t.tree = t.tree[:i]
			break
		}
		depth := int(token.next)
		if length := len(depths); depth >= length {
			depths = depths[:depth+1]
		}
		depths[depth]++
	}
	depths = append(depths, 0)

	ordered, pool := make([][]token32, len(depths)), make([]token32, len(t.tree)+len(depths))
	for i, depth := range depths {
		depth++
		ordered[i], pool, depths[i] = pool[:depth], pool[depth:], 0
	}

	for i, token := range t.tree {
		depth := token.next
		token.next = uint32(i)
		ordered[depth][depths[depth]] = token
		depths[depth]++
	}
	t.ordered = ordered
	return ordered
}

type state32 struct {
	token32
	depths []int32
	leaf   bool
}

func (t *tokens32) AST() *node32 {
	tokens := t.Tokens()
	stack := &element{node: &node32{token32: <-tokens}}
	for token := range tokens {
		if token.begin == token.end {
			continue
		}
		node := &node32{token32: token}
		for stack != nil && stack.node.begin >= token.begin && stack.node.end <= token.end {
			stack.node.next = node.up
			node.up = stack.node
			stack = stack.down
		}
		stack = &element{node: node, down: stack}
	}
	return stack.node
}

func (t *tokens32) PreOrder() (<-chan state32, [][]token32) {
	s, ordered := make(chan state32, 6), t.Order()
	go func() {
		var states [8]state32
		for i := range states {
			states[i].depths = make([]int32, len(ordered))
		}
		depths, state, depth := make([]int32, len(ordered)), 0, 1
		write := func(t token32, leaf bool) {
			S := states[state]
			state, S.pegRule, S.begin, S.end, S.next, S.leaf = (state+1)%8, t.pegRule, t.begin, t.end, uint32(depth), leaf
			copy(S.depths, depths)
			s <- S
		}

		states[state].token32 = ordered[0][0]
		depths[0]++
		state++
		a, b := ordered[depth-1][depths[depth-1]-1], ordered[depth][depths[depth]]
	depthFirstSearch:
		for {
			for {
				if i := depths[depth]; i > 0 {
					if c, j := ordered[depth][i-1], depths[depth-1]; a.isParentOf(c) &&
						(j < 2 || !ordered[depth-1][j-2].isParentOf(c)) {
						if c.end != b.begin {
							write(token32{pegRule: ruleIn, begin: c.end, end: b.begin}, true)
						}
						break
					}
				}

				if a.begin < b.begin {
					write(token32{pegRule: rulePre, begin: a.begin, end: b.begin}, true)
				}
				break
			}

			next := depth + 1
			if c := ordered[next][depths[next]]; c.pegRule != ruleUnknown && b.isParentOf(c) {
				write(b, false)
				depths[depth]++
				depth, a, b = next, b, c
				continue
			}

			write(b, true)
			depths[depth]++
			c, parent := ordered[depth][depths[depth]], true
			for {
				if c.pegRule != ruleUnknown && a.isParentOf(c) {
					b = c
					continue depthFirstSearch
				} else if parent && b.end != a.end {
					write(token32{pegRule: ruleSuf, begin: b.end, end: a.end}, true)
				}

				depth--
				if depth > 0 {
					a, b, c = ordered[depth-1][depths[depth-1]-1], a, ordered[depth][depths[depth]]
					parent = a.isParentOf(b)
					continue
				}

				break depthFirstSearch
			}
		}

		close(s)
	}()
	return s, ordered
}

func (t *tokens32) PrintSyntax() {
	tokens, ordered := t.PreOrder()
	max := -1
	for token := range tokens {
		if !token.leaf {
			fmt.Printf("%v", token.begin)
			for i, leaf, depths := 0, int(token.next), token.depths; i < leaf; i++ {
				fmt.Printf(" \x1B[36m%v\x1B[m", rul3s[ordered[i][depths[i]-1].pegRule])
			}
			fmt.Printf(" \x1B[36m%v\x1B[m\n", rul3s[token.pegRule])
		} else if token.begin == token.end {
			fmt.Printf("%v", token.begin)
			for i, leaf, depths := 0, int(token.next), token.depths; i < leaf; i++ {
				fmt.Printf(" \x1B[31m%v\x1B[m", rul3s[ordered[i][depths[i]-1].pegRule])
			}
			fmt.Printf(" \x1B[31m%v\x1B[m\n", rul3s[token.pegRule])
		} else {
			for c, end := token.begin, token.end; c < end; c++ {
				if i := int(c); max+1 < i {
					for j := max; j < i; j++ {
						fmt.Printf("skip %v %v\n", j, token.String())
					}
					max = i
				} else if i := int(c); i <= max {
					for j := i; j <= max; j++ {
						fmt.Printf("dupe %v %v\n", j, token.String())
					}
				} else {
					max = int(c)
				}
				fmt.Printf("%v", c)
				for i, leaf, depths := 0, int(token.next), token.depths; i < leaf; i++ {
					fmt.Printf(" \x1B[34m%v\x1B[m", rul3s[ordered[i][depths[i]-1].pegRule])
				}
				fmt.Printf(" \x1B[34m%v\x1B[m\n", rul3s[token.pegRule])
			}
			fmt.Printf("\n")
		}
	}
}

func (t *tokens32) PrintSyntaxTree(buffer string) {
	tokens, _ := t.PreOrder()
	for token := range tokens {
		for c := 0; c < int(token.next); c++ {
			fmt.Printf(" ")
		}
		fmt.Printf("\x1B[34m%v\x1B[m %v\n", rul3s[token.pegRule], strconv.Quote(string(([]rune(buffer)[token.begin:token.end]))))
	}
}

func (t *tokens32) Add(rule pegRule, begin, end, depth uint32, index int) {
	t.tree[index] = token32{pegRule: rule, begin: uint32(begin), end: uint32(end), next: uint32(depth)}
}

func (t *tokens32) Tokens() <-chan token32 {
	s := make(chan token32, 16)
	go func() {
		for _, v := range t.tree {
			s <- v.getToken32()
		}
		close(s)
	}()
	return s
}

func (t *tokens32) Error() []token32 {
	ordered := t.Order()
	length := len(ordered)
	tokens, length := make([]token32, length), length-1
	for i := range tokens {
		o := ordered[length-i]
		if len(o) > 1 {
			tokens[i] = o[len(o)-2].getToken32()
		}
	}
	return tokens
}

func (t *tokens32) Expand(index int) {
	tree := t.tree
	if index >= len(tree) {
		expanded := make([]token32, 2*len(tree))
		copy(expanded, tree)
		t.tree = expanded
	}
}

type C struct {
	Buffer string
	buffer []rune
	rules  [199]func() bool
	Parse  func(rule ...int) error
	Reset  func()
	Pretty bool
	tokens32
}

type textPosition struct {
	line, symbol int
}

type textPositionMap map[int]textPosition

func translatePositions(buffer []rune, positions []int) textPositionMap {
	length, translations, j, line, symbol := len(positions), make(textPositionMap, len(positions)), 0, 1, 0
	sort.Ints(positions)

search:
	for i, c := range buffer {
		if c == '\n' {
			line, symbol = line+1, 0
		} else {
			symbol++
		}
		if i == positions[j] {
			translations[positions[j]] = textPosition{line, symbol}
			for j++; j < length; j++ {
				if i != positions[j] {
					continue search
				}
			}
			break search
		}
	}

	return translations
}

type parseError struct {
	p   *C
	max token32
}

func (e *parseError) Error() string {
	tokens, error := []token32{e.max}, "\n"
	positions, p := make([]int, 2*len(tokens)), 0
	for _, token := range tokens {
		positions[p], p = int(token.begin), p+1
		positions[p], p = int(token.end), p+1
	}
	translations := translatePositions(e.p.buffer, positions)
	format := "parse error near %v (line %v symbol %v - line %v symbol %v):\n%v\n"
	if e.p.Pretty {
		format = "parse error near \x1B[34m%v\x1B[m (line %v symbol %v - line %v symbol %v):\n%v\n"
	}
	for _, token := range tokens {
		begin, end := int(token.begin), int(token.end)
		error += fmt.Sprintf(format,
			rul3s[token.pegRule],
			translations[begin].line, translations[begin].symbol,
			translations[end].line, translations[end].symbol,
			strconv.Quote(string(e.p.buffer[begin:end])))
	}

	return error
}

func (p *C) PrintSyntaxTree() {
	p.tokens32.PrintSyntaxTree(p.Buffer)
}

func (p *C) Highlighter() {
	p.PrintSyntax()
}

func (p *C) Init() {
	p.buffer = []rune(p.Buffer)
	if len(p.buffer) == 0 || p.buffer[len(p.buffer)-1] != endSymbol {
		p.buffer = append(p.buffer, endSymbol)
	}

	tree := tokens32{tree: make([]token32, math.MaxInt16)}
	var max token32
	position, depth, tokenIndex, buffer, _rules := uint32(0), uint32(0), 0, p.buffer, p.rules

	p.Parse = func(rule ...int) error {
		r := 1
		if len(rule) > 0 {
			r = rule[0]
		}
		matches := p.rules[r]()
		p.tokens32 = tree
		if matches {
			p.trim(tokenIndex)
			return nil
		}
		return &parseError{p, max}
	}

	p.Reset = func() {
		position, tokenIndex, depth = 0, 0, 0
	}

	add := func(rule pegRule, begin uint32) {
		tree.Expand(tokenIndex)
		tree.Add(rule, begin, position, depth, tokenIndex)
		tokenIndex++
		if begin != position && position > max.end {
			max = token32{rule, begin, position, depth}
		}
	}

	matchDot := func() bool {
		if buffer[position] != endSymbol {
			position++
			return true
		}
		return false
	}

	/*matchChar := func(c byte) bool {
		if buffer[position] == c {
			position++
			return true
		}
		return false
	}*/

	/*matchRange := func(lower byte, upper byte) bool {
		if c := buffer[position]; c >= lower && c <= upper {
			position++
			return true
		}
		return false
	}*/

	_rules = [...]func() bool{
		nil,
		/* 0 TranslationUnit <- <(Spacing (Preprocessing / ExternalDeclaration / SEMI)* EOT)> */
		func() bool {
			position0, tokenIndex0, depth0 := position, tokenIndex, depth
			{
				position1 := position
				depth++
				if !_rules[ruleSpacing]() {
					goto l0
				}
			l2:
				{
					position3, tokenIndex3, depth3 := position, tokenIndex, depth
					{
						position4, tokenIndex4, depth4 := position, tokenIndex, depth
						if !_rules[rulePreprocessing]() {
							goto l5
						}
						goto l4
					l5:
						position, tokenIndex, depth = position4, tokenIndex4, depth4
						if !_rules[ruleExternalDeclaration]() {
							goto l6
						}
						goto l4
					l6:
						position, tokenIndex, depth = position4, tokenIndex4, depth4
						if !_rules[ruleSEMI]() {
							goto l3
						}
					}
				l4:
					goto l2
				l3:
					position, tokenIndex, depth = position3, tokenIndex3, depth3
				}
				if !_rules[ruleEOT]() {
					goto l0
				}
				depth--
				add(ruleTranslationUnit, position1)
			}
			return true
		l0:
			position, tokenIndex, depth = position0, tokenIndex0, depth0
			return false
		},
		/* 1 ExternalDeclaration <- <(FunctionDefinition / Declaration)> */
		func() bool {
			position7, tokenIndex7, depth7 := position, tokenIndex, depth
			{
				position8 := position
				depth++
				{
					position9, tokenIndex9, depth9 := position, tokenIndex, depth
					if !_rules[ruleFunctionDefinition]() {
						goto l10
					}
					goto l9
				l10:
					position, tokenIndex, depth = position9, tokenIndex9, depth9
					if !_rules[ruleDeclaration]() {
						goto l7
					}
				}
			l9:
				depth--
				add(ruleExternalDeclaration, position8)
			}
			return true
		l7:
			position, tokenIndex, depth = position7, tokenIndex7, depth7
			return false
		},
		/* 2 FunctionDefinition <- <(DeclarationSpecifiers Declarator DeclarationList? CompoundStatement)> */
		func() bool {
			position11, tokenIndex11, depth11 := position, tokenIndex, depth
			{
				position12 := position
				depth++
				if !_rules[ruleDeclarationSpecifiers]() {
					goto l11
				}
				if !_rules[ruleDeclarator]() {
					goto l11
				}
				{
					position13, tokenIndex13, depth13 := position, tokenIndex, depth
					if !_rules[ruleDeclarationList]() {
						goto l13
					}
					goto l14
				l13:
					position, tokenIndex, depth = position13, tokenIndex13, depth13
				}
			l14:
				if !_rules[ruleCompoundStatement]() {
					goto l11
				}
				depth--
				add(ruleFunctionDefinition, position12)
			}
			return true
		l11:
			position, tokenIndex, depth = position11, tokenIndex11, depth11
			return false
		},
		/* 3 DeclarationList <- <Declaration+> */
		func() bool {
			position15, tokenIndex15, depth15 := position, tokenIndex, depth
			{
				position16 := position
				depth++
				if !_rules[ruleDeclaration]() {
					goto l15
				}
			l17:
				{
					position18, tokenIndex18, depth18 := position, tokenIndex, depth
					if !_rules[ruleDeclaration]() {
						goto l18
					}
					goto l17
				l18:
					position, tokenIndex, depth = position18, tokenIndex18, depth18
				}
				depth--
				add(ruleDeclarationList, position16)
			}
			return true
		l15:
			position, tokenIndex, depth = position15, tokenIndex15, depth15
			return false
		},
		/* 4 Preprocessing <- <('#' PPInstruction? PPSpacing ('\n' / ('\r' '\n') / '\r') Spacing)> */
		func() bool {
			position19, tokenIndex19, depth19 := position, tokenIndex, depth
			{
				position20 := position
				depth++
				if buffer[position] != rune('#') {
					goto l19
				}
				position++
				{
					position21, tokenIndex21, depth21 := position, tokenIndex, depth
					if !_rules[rulePPInstruction]() {
						goto l21
					}
					goto l22
				l21:
					position, tokenIndex, depth = position21, tokenIndex21, depth21
				}
			l22:
				if !_rules[rulePPSpacing]() {
					goto l19
				}
				{
					position23, tokenIndex23, depth23 := position, tokenIndex, depth
					if buffer[position] != rune('\n') {
						goto l24
					}
					position++
					goto l23
				l24:
					position, tokenIndex, depth = position23, tokenIndex23, depth23
					if buffer[position] != rune('\r') {
						goto l25
					}
					position++
					if buffer[position] != rune('\n') {
						goto l25
					}
					position++
					goto l23
				l25:
					position, tokenIndex, depth = position23, tokenIndex23, depth23
					if buffer[position] != rune('\r') {
						goto l19
					}
					position++
				}
			l23:
				if !_rules[ruleSpacing]() {
					goto l19
				}
				depth--
				add(rulePreprocessing, position20)
			}
			return true
		l19:
			position, tokenIndex, depth = position19, tokenIndex19, depth19
			return false
		},
		/* 5 PPSpacing <- <((' ' / '\t')+ / ('\\' '\n') / ('\\' '\r' '\n') / ('\\' '\r'))> */
		func() bool {
			position26, tokenIndex26, depth26 := position, tokenIndex, depth
			{
				position27 := position
				depth++
				{
					position28, tokenIndex28, depth28 := position, tokenIndex, depth
					{
						position32, tokenIndex32, depth32 := position, tokenIndex, depth
						if buffer[position] != rune(' ') {
							goto l33
						}
						position++
						goto l32
					l33:
						position, tokenIndex, depth = position32, tokenIndex32, depth32
						if buffer[position] != rune('\t') {
							goto l29
						}
						position++
					}
				l32:
				l30:
					{
						position31, tokenIndex31, depth31 := position, tokenIndex, depth
						{
							position34, tokenIndex34, depth34 := position, tokenIndex, depth
							if buffer[position] != rune(' ') {
								goto l35
							}
							position++
							goto l34
						l35:
							position, tokenIndex, depth = position34, tokenIndex34, depth34
							if buffer[position] != rune('\t') {
								goto l31
							}
							position++
						}
					l34:
						goto l30
					l31:
						position, tokenIndex, depth = position31, tokenIndex31, depth31
					}
					goto l28
				l29:
					position, tokenIndex, depth = position28, tokenIndex28, depth28
					if buffer[position] != rune('\\') {
						goto l36
					}
					position++
					if buffer[position] != rune('\n') {
						goto l36
					}
					position++
					goto l28
				l36:
					position, tokenIndex, depth = position28, tokenIndex28, depth28
					if buffer[position] != rune('\\') {
						goto l37
					}
					position++
					if buffer[position] != rune('\r') {
						goto l37
					}
					position++
					if buffer[position] != rune('\n') {
						goto l37
					}
					position++
					goto l28
				l37:
					position, tokenIndex, depth = position28, tokenIndex28, depth28
					if buffer[position] != rune('\\') {
						goto l26
					}
					position++
					if buffer[position] != rune('\r') {
						goto l26
					}
					position++
				}
			l28:
				depth--
				add(rulePPSpacing, position27)
			}
			return true
		l26:
			position, tokenIndex, depth = position26, tokenIndex26, depth26
			return false
		},
		/* 6 PPInstruction <- <(PPDefine / PPElse / PPEndif / PPIf / PPIfdef / PPInclude / PPLine / PPPragma)> */
		func() bool {
			position38, tokenIndex38, depth38 := position, tokenIndex, depth
			{
				position39 := position
				depth++
				{
					position40, tokenIndex40, depth40 := position, tokenIndex, depth
					if !_rules[rulePPDefine]() {
						goto l41
					}
					goto l40
				l41:
					position, tokenIndex, depth = position40, tokenIndex40, depth40
					if !_rules[rulePPElse]() {
						goto l42
					}
					goto l40
				l42:
					position, tokenIndex, depth = position40, tokenIndex40, depth40
					if !_rules[rulePPEndif]() {
						goto l43
					}
					goto l40
				l43:
					position, tokenIndex, depth = position40, tokenIndex40, depth40
					if !_rules[rulePPIf]() {
						goto l44
					}
					goto l40
				l44:
					position, tokenIndex, depth = position40, tokenIndex40, depth40
					if !_rules[rulePPIfdef]() {
						goto l45
					}
					goto l40
				l45:
					position, tokenIndex, depth = position40, tokenIndex40, depth40
					if !_rules[rulePPInclude]() {
						goto l46
					}
					goto l40
				l46:
					position, tokenIndex, depth = position40, tokenIndex40, depth40
					if !_rules[rulePPLine]() {
						goto l47
					}
					goto l40
				l47:
					position, tokenIndex, depth = position40, tokenIndex40, depth40
					if !_rules[rulePPPragma]() {
						goto l38
					}
				}
			l40:
				depth--
				add(rulePPInstruction, position39)
			}
			return true
		l38:
			position, tokenIndex, depth = position38, tokenIndex38, depth38
			return false
		},
		/* 7 PPDefine <- <('d' 'e' 'f' 'i' 'n' 'e' PPSpacing+ Identifier PPSpacing+ ConstantExpression)> */
		func() bool {
			position48, tokenIndex48, depth48 := position, tokenIndex, depth
			{
				position49 := position
				depth++
				if buffer[position] != rune('d') {
					goto l48
				}
				position++
				if buffer[position] != rune('e') {
					goto l48
				}
				position++
				if buffer[position] != rune('f') {
					goto l48
				}
				position++
				if buffer[position] != rune('i') {
					goto l48
				}
				position++
				if buffer[position] != rune('n') {
					goto l48
				}
				position++
				if buffer[position] != rune('e') {
					goto l48
				}
				position++
				if !_rules[rulePPSpacing]() {
					goto l48
				}
			l50:
				{
					position51, tokenIndex51, depth51 := position, tokenIndex, depth
					if !_rules[rulePPSpacing]() {
						goto l51
					}
					goto l50
				l51:
					position, tokenIndex, depth = position51, tokenIndex51, depth51
				}
				if !_rules[ruleIdentifier]() {
					goto l48
				}
				if !_rules[rulePPSpacing]() {
					goto l48
				}
			l52:
				{
					position53, tokenIndex53, depth53 := position, tokenIndex, depth
					if !_rules[rulePPSpacing]() {
						goto l53
					}
					goto l52
				l53:
					position, tokenIndex, depth = position53, tokenIndex53, depth53
				}
				if !_rules[ruleConstantExpression]() {
					goto l48
				}
				depth--
				add(rulePPDefine, position49)
			}
			return true
		l48:
			position, tokenIndex, depth = position48, tokenIndex48, depth48
			return false
		},
		/* 8 PPElse <- <('e' 'l' 's' 'e')> */
		func() bool {
			position54, tokenIndex54, depth54 := position, tokenIndex, depth
			{
				position55 := position
				depth++
				if buffer[position] != rune('e') {
					goto l54
				}
				position++
				if buffer[position] != rune('l') {
					goto l54
				}
				position++
				if buffer[position] != rune('s') {
					goto l54
				}
				position++
				if buffer[position] != rune('e') {
					goto l54
				}
				position++
				depth--
				add(rulePPElse, position55)
			}
			return true
		l54:
			position, tokenIndex, depth = position54, tokenIndex54, depth54
			return false
		},
		/* 9 PPEndif <- <('e' 'n' 'd' 'i' 'f')> */
		func() bool {
			position56, tokenIndex56, depth56 := position, tokenIndex, depth
			{
				position57 := position
				depth++
				if buffer[position] != rune('e') {
					goto l56
				}
				position++
				if buffer[position] != rune('n') {
					goto l56
				}
				position++
				if buffer[position] != rune('d') {
					goto l56
				}
				position++
				if buffer[position] != rune('i') {
					goto l56
				}
				position++
				if buffer[position] != rune('f') {
					goto l56
				}
				position++
				depth--
				add(rulePPEndif, position57)
			}
			return true
		l56:
			position, tokenIndex, depth = position56, tokenIndex56, depth56
			return false
		},
		/* 10 PPIf <- <('i' 'f' PPSpacing+ ConstantExpression)> */
		func() bool {
			position58, tokenIndex58, depth58 := position, tokenIndex, depth
			{
				position59 := position
				depth++
				if buffer[position] != rune('i') {
					goto l58
				}
				position++
				if buffer[position] != rune('f') {
					goto l58
				}
				position++
				if !_rules[rulePPSpacing]() {
					goto l58
				}
			l60:
				{
					position61, tokenIndex61, depth61 := position, tokenIndex, depth
					if !_rules[rulePPSpacing]() {
						goto l61
					}
					goto l60
				l61:
					position, tokenIndex, depth = position61, tokenIndex61, depth61
				}
				if !_rules[ruleConstantExpression]() {
					goto l58
				}
				depth--
				add(rulePPIf, position59)
			}
			return true
		l58:
			position, tokenIndex, depth = position58, tokenIndex58, depth58
			return false
		},
		/* 11 PPIfdef <- <('i' 'f' 'd' 'e' 'f' PPSpacing+ Identifier)> */
		func() bool {
			position62, tokenIndex62, depth62 := position, tokenIndex, depth
			{
				position63 := position
				depth++
				if buffer[position] != rune('i') {
					goto l62
				}
				position++
				if buffer[position] != rune('f') {
					goto l62
				}
				position++
				if buffer[position] != rune('d') {
					goto l62
				}
				position++
				if buffer[position] != rune('e') {
					goto l62
				}
				position++
				if buffer[position] != rune('f') {
					goto l62
				}
				position++
				if !_rules[rulePPSpacing]() {
					goto l62
				}
			l64:
				{
					position65, tokenIndex65, depth65 := position, tokenIndex, depth
					if !_rules[rulePPSpacing]() {
						goto l65
					}
					goto l64
				l65:
					position, tokenIndex, depth = position65, tokenIndex65, depth65
				}
				if !_rules[ruleIdentifier]() {
					goto l62
				}
				depth--
				add(rulePPIfdef, position63)
			}
			return true
		l62:
			position, tokenIndex, depth = position62, tokenIndex62, depth62
			return false
		},
		/* 12 PPInclude <- <('i' 'n' 'c' 'l' 'u' 'd' 'e' PPSpacing+ StringLiteral)> */
		func() bool {
			position66, tokenIndex66, depth66 := position, tokenIndex, depth
			{
				position67 := position
				depth++
				if buffer[position] != rune('i') {
					goto l66
				}
				position++
				if buffer[position] != rune('n') {
					goto l66
				}
				position++
				if buffer[position] != rune('c') {
					goto l66
				}
				position++
				if buffer[position] != rune('l') {
					goto l66
				}
				position++
				if buffer[position] != rune('u') {
					goto l66
				}
				position++
				if buffer[position] != rune('d') {
					goto l66
				}
				position++
				if buffer[position] != rune('e') {
					goto l66
				}
				position++
				if !_rules[rulePPSpacing]() {
					goto l66
				}
			l68:
				{
					position69, tokenIndex69, depth69 := position, tokenIndex, depth
					if !_rules[rulePPSpacing]() {
						goto l69
					}
					goto l68
				l69:
					position, tokenIndex, depth = position69, tokenIndex69, depth69
				}
				if !_rules[ruleStringLiteral]() {
					goto l66
				}
				depth--
				add(rulePPInclude, position67)
			}
			return true
		l66:
			position, tokenIndex, depth = position66, tokenIndex66, depth66
			return false
		},
		/* 13 PPLine <- <('l' 'i' 'n' 'e' PPSpacing+ (!'\n' .)*)> */
		func() bool {
			position70, tokenIndex70, depth70 := position, tokenIndex, depth
			{
				position71 := position
				depth++
				if buffer[position] != rune('l') {
					goto l70
				}
				position++
				if buffer[position] != rune('i') {
					goto l70
				}
				position++
				if buffer[position] != rune('n') {
					goto l70
				}
				position++
				if buffer[position] != rune('e') {
					goto l70
				}
				position++
				if !_rules[rulePPSpacing]() {
					goto l70
				}
			l72:
				{
					position73, tokenIndex73, depth73 := position, tokenIndex, depth
					if !_rules[rulePPSpacing]() {
						goto l73
					}
					goto l72
				l73:
					position, tokenIndex, depth = position73, tokenIndex73, depth73
				}
			l74:
				{
					position75, tokenIndex75, depth75 := position, tokenIndex, depth
					{
						position76, tokenIndex76, depth76 := position, tokenIndex, depth
						if buffer[position] != rune('\n') {
							goto l76
						}
						position++
						goto l75
					l76:
						position, tokenIndex, depth = position76, tokenIndex76, depth76
					}
					if !matchDot() {
						goto l75
					}
					goto l74
				l75:
					position, tokenIndex, depth = position75, tokenIndex75, depth75
				}
				depth--
				add(rulePPLine, position71)
			}
			return true
		l70:
			position, tokenIndex, depth = position70, tokenIndex70, depth70
			return false
		},
		/* 14 PPPragma <- <('p' 'r' 'a' 'g' 'm' 'a' (!'\n' .)*)> */
		func() bool {
			position77, tokenIndex77, depth77 := position, tokenIndex, depth
			{
				position78 := position
				depth++
				if buffer[position] != rune('p') {
					goto l77
				}
				position++
				if buffer[position] != rune('r') {
					goto l77
				}
				position++
				if buffer[position] != rune('a') {
					goto l77
				}
				position++
				if buffer[position] != rune('g') {
					goto l77
				}
				position++
				if buffer[position] != rune('m') {
					goto l77
				}
				position++
				if buffer[position] != rune('a') {
					goto l77
				}
				position++
			l79:
				{
					position80, tokenIndex80, depth80 := position, tokenIndex, depth
					{
						position81, tokenIndex81, depth81 := position, tokenIndex, depth
						if buffer[position] != rune('\n') {
							goto l81
						}
						position++
						goto l80
					l81:
						position, tokenIndex, depth = position81, tokenIndex81, depth81
					}
					if !matchDot() {
						goto l80
					}
					goto l79
				l80:
					position, tokenIndex, depth = position80, tokenIndex80, depth80
				}
				depth--
				add(rulePPPragma, position78)
			}
			return true
		l77:
			position, tokenIndex, depth = position77, tokenIndex77, depth77
			return false
		},
		/* 15 Declaration <- <(DeclarationSpecifiers InitDeclaratorList? SEMI)> */
		func() bool {
			position82, tokenIndex82, depth82 := position, tokenIndex, depth
			{
				position83 := position
				depth++
				if !_rules[ruleDeclarationSpecifiers]() {
					goto l82
				}
				{
					position84, tokenIndex84, depth84 := position, tokenIndex, depth
					if !_rules[ruleInitDeclaratorList]() {
						goto l84
					}
					goto l85
				l84:
					position, tokenIndex, depth = position84, tokenIndex84, depth84
				}
			l85:
				if !_rules[ruleSEMI]() {
					goto l82
				}
				depth--
				add(ruleDeclaration, position83)
			}
			return true
		l82:
			position, tokenIndex, depth = position82, tokenIndex82, depth82
			return false
		},
		/* 16 DeclarationSpecifiers <- <(((StorageClassSpecifier / TypeQualifier / FunctionSpecifier)* TypedefName (StorageClassSpecifier / TypeQualifier / FunctionSpecifier)*) / (StorageClassSpecifier / TypeSpecifier / TypeQualifier / FunctionSpecifier)+)> */
		func() bool {
			position86, tokenIndex86, depth86 := position, tokenIndex, depth
			{
				position87 := position
				depth++
				{
					position88, tokenIndex88, depth88 := position, tokenIndex, depth
				l90:
					{
						position91, tokenIndex91, depth91 := position, tokenIndex, depth
						{
							position92, tokenIndex92, depth92 := position, tokenIndex, depth
							if !_rules[ruleStorageClassSpecifier]() {
								goto l93
							}
							goto l92
						l93:
							position, tokenIndex, depth = position92, tokenIndex92, depth92
							if !_rules[ruleTypeQualifier]() {
								goto l94
							}
							goto l92
						l94:
							position, tokenIndex, depth = position92, tokenIndex92, depth92
							if !_rules[ruleFunctionSpecifier]() {
								goto l91
							}
						}
					l92:
						goto l90
					l91:
						position, tokenIndex, depth = position91, tokenIndex91, depth91
					}
					if !_rules[ruleTypedefName]() {
						goto l89
					}
				l95:
					{
						position96, tokenIndex96, depth96 := position, tokenIndex, depth
						{
							position97, tokenIndex97, depth97 := position, tokenIndex, depth
							if !_rules[ruleStorageClassSpecifier]() {
								goto l98
							}
							goto l97
						l98:
							position, tokenIndex, depth = position97, tokenIndex97, depth97
							if !_rules[ruleTypeQualifier]() {
								goto l99
							}
							goto l97
						l99:
							position, tokenIndex, depth = position97, tokenIndex97, depth97
							if !_rules[ruleFunctionSpecifier]() {
								goto l96
							}
						}
					l97:
						goto l95
					l96:
						position, tokenIndex, depth = position96, tokenIndex96, depth96
					}
					goto l88
				l89:
					position, tokenIndex, depth = position88, tokenIndex88, depth88
					{
						position102, tokenIndex102, depth102 := position, tokenIndex, depth
						if !_rules[ruleStorageClassSpecifier]() {
							goto l103
						}
						goto l102
					l103:
						position, tokenIndex, depth = position102, tokenIndex102, depth102
						if !_rules[ruleTypeSpecifier]() {
							goto l104
						}
						goto l102
					l104:
						position, tokenIndex, depth = position102, tokenIndex102, depth102
						if !_rules[ruleTypeQualifier]() {
							goto l105
						}
						goto l102
					l105:
						position, tokenIndex, depth = position102, tokenIndex102, depth102
						if !_rules[ruleFunctionSpecifier]() {
							goto l86
						}
					}
				l102:
				l100:
					{
						position101, tokenIndex101, depth101 := position, tokenIndex, depth
						{
							position106, tokenIndex106, depth106 := position, tokenIndex, depth
							if !_rules[ruleStorageClassSpecifier]() {
								goto l107
							}
							goto l106
						l107:
							position, tokenIndex, depth = position106, tokenIndex106, depth106
							if !_rules[ruleTypeSpecifier]() {
								goto l108
							}
							goto l106
						l108:
							position, tokenIndex, depth = position106, tokenIndex106, depth106
							if !_rules[ruleTypeQualifier]() {
								goto l109
							}
							goto l106
						l109:
							position, tokenIndex, depth = position106, tokenIndex106, depth106
							if !_rules[ruleFunctionSpecifier]() {
								goto l101
							}
						}
					l106:
						goto l100
					l101:
						position, tokenIndex, depth = position101, tokenIndex101, depth101
					}
				}
			l88:
				depth--
				add(ruleDeclarationSpecifiers, position87)
			}
			return true
		l86:
			position, tokenIndex, depth = position86, tokenIndex86, depth86
			return false
		},
		/* 17 InitDeclaratorList <- <(InitDeclarator (COMMA InitDeclarator)*)> */
		func() bool {
			position110, tokenIndex110, depth110 := position, tokenIndex, depth
			{
				position111 := position
				depth++
				if !_rules[ruleInitDeclarator]() {
					goto l110
				}
			l112:
				{
					position113, tokenIndex113, depth113 := position, tokenIndex, depth
					if !_rules[ruleCOMMA]() {
						goto l113
					}
					if !_rules[ruleInitDeclarator]() {
						goto l113
					}
					goto l112
				l113:
					position, tokenIndex, depth = position113, tokenIndex113, depth113
				}
				depth--
				add(ruleInitDeclaratorList, position111)
			}
			return true
		l110:
			position, tokenIndex, depth = position110, tokenIndex110, depth110
			return false
		},
		/* 18 InitDeclarator <- <(Declarator (EQU Initializer)?)> */
		func() bool {
			position114, tokenIndex114, depth114 := position, tokenIndex, depth
			{
				position115 := position
				depth++
				if !_rules[ruleDeclarator]() {
					goto l114
				}
				{
					position116, tokenIndex116, depth116 := position, tokenIndex, depth
					if !_rules[ruleEQU]() {
						goto l116
					}
					if !_rules[ruleInitializer]() {
						goto l116
					}
					goto l117
				l116:
					position, tokenIndex, depth = position116, tokenIndex116, depth116
				}
			l117:
				depth--
				add(ruleInitDeclarator, position115)
			}
			return true
		l114:
			position, tokenIndex, depth = position114, tokenIndex114, depth114
			return false
		},
		/* 19 StorageClassSpecifier <- <(TYPEDEF / EXTERN / STATIC / AUTO / REGISTER / (ATTRIBUTE LPAR LPAR (!RPAR .)* RPAR RPAR))> */
		func() bool {
			position118, tokenIndex118, depth118 := position, tokenIndex, depth
			{
				position119 := position
				depth++
				{
					position120, tokenIndex120, depth120 := position, tokenIndex, depth
					if !_rules[ruleTYPEDEF]() {
						goto l121
					}
					goto l120
				l121:
					position, tokenIndex, depth = position120, tokenIndex120, depth120
					if !_rules[ruleEXTERN]() {
						goto l122
					}
					goto l120
				l122:
					position, tokenIndex, depth = position120, tokenIndex120, depth120
					if !_rules[ruleSTATIC]() {
						goto l123
					}
					goto l120
				l123:
					position, tokenIndex, depth = position120, tokenIndex120, depth120
					if !_rules[ruleAUTO]() {
						goto l124
					}
					goto l120
				l124:
					position, tokenIndex, depth = position120, tokenIndex120, depth120
					if !_rules[ruleREGISTER]() {
						goto l125
					}
					goto l120
				l125:
					position, tokenIndex, depth = position120, tokenIndex120, depth120
					if !_rules[ruleATTRIBUTE]() {
						goto l118
					}
					if !_rules[ruleLPAR]() {
						goto l118
					}
					if !_rules[ruleLPAR]() {
						goto l118
					}
				l126:
					{
						position127, tokenIndex127, depth127 := position, tokenIndex, depth
						{
							position128, tokenIndex128, depth128 := position, tokenIndex, depth
							if !_rules[ruleRPAR]() {
								goto l128
							}
							goto l127
						l128:
							position, tokenIndex, depth = position128, tokenIndex128, depth128
						}
						if !matchDot() {
							goto l127
						}
						goto l126
					l127:
						position, tokenIndex, depth = position127, tokenIndex127, depth127
					}
					if !_rules[ruleRPAR]() {
						goto l118
					}
					if !_rules[ruleRPAR]() {
						goto l118
					}
				}
			l120:
				depth--
				add(ruleStorageClassSpecifier, position119)
			}
			return true
		l118:
			position, tokenIndex, depth = position118, tokenIndex118, depth118
			return false
		},
		/* 20 TypeSpecifier <- <(VOID / CHAR / SHORT / INT / LONG / FLOAT / DOUBLE / SIGNED / UNSIGNED / BOOL / COMPLEX / StructOrUnionSpecifier / EnumSpecifier)> */
		func() bool {
			position129, tokenIndex129, depth129 := position, tokenIndex, depth
			{
				position130 := position
				depth++
				{
					position131, tokenIndex131, depth131 := position, tokenIndex, depth
					if !_rules[ruleVOID]() {
						goto l132
					}
					goto l131
				l132:
					position, tokenIndex, depth = position131, tokenIndex131, depth131
					if !_rules[ruleCHAR]() {
						goto l133
					}
					goto l131
				l133:
					position, tokenIndex, depth = position131, tokenIndex131, depth131
					if !_rules[ruleSHORT]() {
						goto l134
					}
					goto l131
				l134:
					position, tokenIndex, depth = position131, tokenIndex131, depth131
					if !_rules[ruleINT]() {
						goto l135
					}
					goto l131
				l135:
					position, tokenIndex, depth = position131, tokenIndex131, depth131
					if !_rules[ruleLONG]() {
						goto l136
					}
					goto l131
				l136:
					position, tokenIndex, depth = position131, tokenIndex131, depth131
					if !_rules[ruleFLOAT]() {
						goto l137
					}
					goto l131
				l137:
					position, tokenIndex, depth = position131, tokenIndex131, depth131
					if !_rules[ruleDOUBLE]() {
						goto l138
					}
					goto l131
				l138:
					position, tokenIndex, depth = position131, tokenIndex131, depth131
					if !_rules[ruleSIGNED]() {
						goto l139
					}
					goto l131
				l139:
					position, tokenIndex, depth = position131, tokenIndex131, depth131
					if !_rules[ruleUNSIGNED]() {
						goto l140
					}
					goto l131
				l140:
					position, tokenIndex, depth = position131, tokenIndex131, depth131
					if !_rules[ruleBOOL]() {
						goto l141
					}
					goto l131
				l141:
					position, tokenIndex, depth = position131, tokenIndex131, depth131
					if !_rules[ruleCOMPLEX]() {
						goto l142
					}
					goto l131
				l142:
					position, tokenIndex, depth = position131, tokenIndex131, depth131
					if !_rules[ruleStructOrUnionSpecifier]() {
						goto l143
					}
					goto l131
				l143:
					position, tokenIndex, depth = position131, tokenIndex131, depth131
					if !_rules[ruleEnumSpecifier]() {
						goto l129
					}
				}
			l131:
				depth--
				add(ruleTypeSpecifier, position130)
			}
			return true
		l129:
			position, tokenIndex, depth = position129, tokenIndex129, depth129
			return false
		},
		/* 21 StructOrUnionSpecifier <- <(StructOrUnion ((Identifier? LWING StructDeclaration* RWING) / Identifier))> */
		func() bool {
			position144, tokenIndex144, depth144 := position, tokenIndex, depth
			{
				position145 := position
				depth++
				if !_rules[ruleStructOrUnion]() {
					goto l144
				}
				{
					position146, tokenIndex146, depth146 := position, tokenIndex, depth
					{
						position148, tokenIndex148, depth148 := position, tokenIndex, depth
						if !_rules[ruleIdentifier]() {
							goto l148
						}
						goto l149
					l148:
						position, tokenIndex, depth = position148, tokenIndex148, depth148
					}
				l149:
					if !_rules[ruleLWING]() {
						goto l147
					}
				l150:
					{
						position151, tokenIndex151, depth151 := position, tokenIndex, depth
						if !_rules[ruleStructDeclaration]() {
							goto l151
						}
						goto l150
					l151:
						position, tokenIndex, depth = position151, tokenIndex151, depth151
					}
					if !_rules[ruleRWING]() {
						goto l147
					}
					goto l146
				l147:
					position, tokenIndex, depth = position146, tokenIndex146, depth146
					if !_rules[ruleIdentifier]() {
						goto l144
					}
				}
			l146:
				depth--
				add(ruleStructOrUnionSpecifier, position145)
			}
			return true
		l144:
			position, tokenIndex, depth = position144, tokenIndex144, depth144
			return false
		},
		/* 22 StructOrUnion <- <(STRUCT / UNION)> */
		func() bool {
			position152, tokenIndex152, depth152 := position, tokenIndex, depth
			{
				position153 := position
				depth++
				{
					position154, tokenIndex154, depth154 := position, tokenIndex, depth
					if !_rules[ruleSTRUCT]() {
						goto l155
					}
					goto l154
				l155:
					position, tokenIndex, depth = position154, tokenIndex154, depth154
					if !_rules[ruleUNION]() {
						goto l152
					}
				}
			l154:
				depth--
				add(ruleStructOrUnion, position153)
			}
			return true
		l152:
			position, tokenIndex, depth = position152, tokenIndex152, depth152
			return false
		},
		/* 23 StructDeclaration <- <((SpecifierQualifierList StructDeclaratorList?)? SEMI)> */
		func() bool {
			position156, tokenIndex156, depth156 := position, tokenIndex, depth
			{
				position157 := position
				depth++
				{
					position158, tokenIndex158, depth158 := position, tokenIndex, depth
					if !_rules[ruleSpecifierQualifierList]() {
						goto l158
					}
					{
						position160, tokenIndex160, depth160 := position, tokenIndex, depth
						if !_rules[ruleStructDeclaratorList]() {
							goto l160
						}
						goto l161
					l160:
						position, tokenIndex, depth = position160, tokenIndex160, depth160
					}
				l161:
					goto l159
				l158:
					position, tokenIndex, depth = position158, tokenIndex158, depth158
				}
			l159:
				if !_rules[ruleSEMI]() {
					goto l156
				}
				depth--
				add(ruleStructDeclaration, position157)
			}
			return true
		l156:
			position, tokenIndex, depth = position156, tokenIndex156, depth156
			return false
		},
		/* 24 SpecifierQualifierList <- <((TypeQualifier* TypedefName TypeQualifier*) / (TypeSpecifier / TypeQualifier)+)> */
		func() bool {
			position162, tokenIndex162, depth162 := position, tokenIndex, depth
			{
				position163 := position
				depth++
				{
					position164, tokenIndex164, depth164 := position, tokenIndex, depth
				l166:
					{
						position167, tokenIndex167, depth167 := position, tokenIndex, depth
						if !_rules[ruleTypeQualifier]() {
							goto l167
						}
						goto l166
					l167:
						position, tokenIndex, depth = position167, tokenIndex167, depth167
					}
					if !_rules[ruleTypedefName]() {
						goto l165
					}
				l168:
					{
						position169, tokenIndex169, depth169 := position, tokenIndex, depth
						if !_rules[ruleTypeQualifier]() {
							goto l169
						}
						goto l168
					l169:
						position, tokenIndex, depth = position169, tokenIndex169, depth169
					}
					goto l164
				l165:
					position, tokenIndex, depth = position164, tokenIndex164, depth164
					{
						position172, tokenIndex172, depth172 := position, tokenIndex, depth
						if !_rules[ruleTypeSpecifier]() {
							goto l173
						}
						goto l172
					l173:
						position, tokenIndex, depth = position172, tokenIndex172, depth172
						if !_rules[ruleTypeQualifier]() {
							goto l162
						}
					}
				l172:
				l170:
					{
						position171, tokenIndex171, depth171 := position, tokenIndex, depth
						{
							position174, tokenIndex174, depth174 := position, tokenIndex, depth
							if !_rules[ruleTypeSpecifier]() {
								goto l175
							}
							goto l174
						l175:
							position, tokenIndex, depth = position174, tokenIndex174, depth174
							if !_rules[ruleTypeQualifier]() {
								goto l171
							}
						}
					l174:
						goto l170
					l171:
						position, tokenIndex, depth = position171, tokenIndex171, depth171
					}
				}
			l164:
				depth--
				add(ruleSpecifierQualifierList, position163)
			}
			return true
		l162:
			position, tokenIndex, depth = position162, tokenIndex162, depth162
			return false
		},
		/* 25 StructDeclaratorList <- <(StructDeclarator (COMMA StructDeclarator)*)> */
		func() bool {
			position176, tokenIndex176, depth176 := position, tokenIndex, depth
			{
				position177 := position
				depth++
				if !_rules[ruleStructDeclarator]() {
					goto l176
				}
			l178:
				{
					position179, tokenIndex179, depth179 := position, tokenIndex, depth
					if !_rules[ruleCOMMA]() {
						goto l179
					}
					if !_rules[ruleStructDeclarator]() {
						goto l179
					}
					goto l178
				l179:
					position, tokenIndex, depth = position179, tokenIndex179, depth179
				}
				depth--
				add(ruleStructDeclaratorList, position177)
			}
			return true
		l176:
			position, tokenIndex, depth = position176, tokenIndex176, depth176
			return false
		},
		/* 26 StructDeclarator <- <((Declarator? COLON ConstantExpression) / Declarator)> */
		func() bool {
			position180, tokenIndex180, depth180 := position, tokenIndex, depth
			{
				position181 := position
				depth++
				{
					position182, tokenIndex182, depth182 := position, tokenIndex, depth
					{
						position184, tokenIndex184, depth184 := position, tokenIndex, depth
						if !_rules[ruleDeclarator]() {
							goto l184
						}
						goto l185
					l184:
						position, tokenIndex, depth = position184, tokenIndex184, depth184
					}
				l185:
					if !_rules[ruleCOLON]() {
						goto l183
					}
					if !_rules[ruleConstantExpression]() {
						goto l183
					}
					goto l182
				l183:
					position, tokenIndex, depth = position182, tokenIndex182, depth182
					if !_rules[ruleDeclarator]() {
						goto l180
					}
				}
			l182:
				depth--
				add(ruleStructDeclarator, position181)
			}
			return true
		l180:
			position, tokenIndex, depth = position180, tokenIndex180, depth180
			return false
		},
		/* 27 EnumSpecifier <- <(ENUM ((<Identifier?> LWING EnumeratorList COMMA? RWING) / <Identifier>))> */
		func() bool {
			position186, tokenIndex186, depth186 := position, tokenIndex, depth
			{
				position187 := position
				depth++
				if !_rules[ruleENUM]() {
					goto l186
				}
				{
					position188, tokenIndex188, depth188 := position, tokenIndex, depth
					{
						position190 := position
						depth++
						{
							position191, tokenIndex191, depth191 := position, tokenIndex, depth
							if !_rules[ruleIdentifier]() {
								goto l191
							}
							goto l192
						l191:
							position, tokenIndex, depth = position191, tokenIndex191, depth191
						}
					l192:
						depth--
						add(rulePegText, position190)
					}
					if !_rules[ruleLWING]() {
						goto l189
					}
					if !_rules[ruleEnumeratorList]() {
						goto l189
					}
					{
						position193, tokenIndex193, depth193 := position, tokenIndex, depth
						if !_rules[ruleCOMMA]() {
							goto l193
						}
						goto l194
					l193:
						position, tokenIndex, depth = position193, tokenIndex193, depth193
					}
				l194:
					if !_rules[ruleRWING]() {
						goto l189
					}
					goto l188
				l189:
					position, tokenIndex, depth = position188, tokenIndex188, depth188
					{
						position195 := position
						depth++
						if !_rules[ruleIdentifier]() {
							goto l186
						}
						depth--
						add(rulePegText, position195)
					}
				}
			l188:
				depth--
				add(ruleEnumSpecifier, position187)
			}
			return true
		l186:
			position, tokenIndex, depth = position186, tokenIndex186, depth186
			return false
		},
		/* 28 EnumeratorList <- <(Enumerator (COMMA Enumerator)*)> */
		func() bool {
			position196, tokenIndex196, depth196 := position, tokenIndex, depth
			{
				position197 := position
				depth++
				if !_rules[ruleEnumerator]() {
					goto l196
				}
			l198:
				{
					position199, tokenIndex199, depth199 := position, tokenIndex, depth
					if !_rules[ruleCOMMA]() {
						goto l199
					}
					if !_rules[ruleEnumerator]() {
						goto l199
					}
					goto l198
				l199:
					position, tokenIndex, depth = position199, tokenIndex199, depth199
				}
				depth--
				add(ruleEnumeratorList, position197)
			}
			return true
		l196:
			position, tokenIndex, depth = position196, tokenIndex196, depth196
			return false
		},
		/* 29 Enumerator <- <<(EnumerationConstant (EQU ConstantExpression)?)>> */
		func() bool {
			position200, tokenIndex200, depth200 := position, tokenIndex, depth
			{
				position201 := position
				depth++
				{
					position202 := position
					depth++
					if !_rules[ruleEnumerationConstant]() {
						goto l200
					}
					{
						position203, tokenIndex203, depth203 := position, tokenIndex, depth
						if !_rules[ruleEQU]() {
							goto l203
						}
						if !_rules[ruleConstantExpression]() {
							goto l203
						}
						goto l204
					l203:
						position, tokenIndex, depth = position203, tokenIndex203, depth203
					}
				l204:
					depth--
					add(rulePegText, position202)
				}
				depth--
				add(ruleEnumerator, position201)
			}
			return true
		l200:
			position, tokenIndex, depth = position200, tokenIndex200, depth200
			return false
		},
		/* 30 TypeQualifier <- <(CONST / RESTRICT / VOLATILE / (DECLSPEC LPAR Identifier RPAR))> */
		func() bool {
			position205, tokenIndex205, depth205 := position, tokenIndex, depth
			{
				position206 := position
				depth++
				{
					position207, tokenIndex207, depth207 := position, tokenIndex, depth
					if !_rules[ruleCONST]() {
						goto l208
					}
					goto l207
				l208:
					position, tokenIndex, depth = position207, tokenIndex207, depth207
					if !_rules[ruleRESTRICT]() {
						goto l209
					}
					goto l207
				l209:
					position, tokenIndex, depth = position207, tokenIndex207, depth207
					if !_rules[ruleVOLATILE]() {
						goto l210
					}
					goto l207
				l210:
					position, tokenIndex, depth = position207, tokenIndex207, depth207
					if !_rules[ruleDECLSPEC]() {
						goto l205
					}
					if !_rules[ruleLPAR]() {
						goto l205
					}
					if !_rules[ruleIdentifier]() {
						goto l205
					}
					if !_rules[ruleRPAR]() {
						goto l205
					}
				}
			l207:
				depth--
				add(ruleTypeQualifier, position206)
			}
			return true
		l205:
			position, tokenIndex, depth = position205, tokenIndex205, depth205
			return false
		},
		/* 31 FunctionSpecifier <- <(INLINE / STDCALL)> */
		func() bool {
			position211, tokenIndex211, depth211 := position, tokenIndex, depth
			{
				position212 := position
				depth++
				{
					position213, tokenIndex213, depth213 := position, tokenIndex, depth
					if !_rules[ruleINLINE]() {
						goto l214
					}
					goto l213
				l214:
					position, tokenIndex, depth = position213, tokenIndex213, depth213
					if !_rules[ruleSTDCALL]() {
						goto l211
					}
				}
			l213:
				depth--
				add(ruleFunctionSpecifier, position212)
			}
			return true
		l211:
			position, tokenIndex, depth = position211, tokenIndex211, depth211
			return false
		},
		/* 32 Declarator <- <(Pointer? DirectDeclarator)> */
		func() bool {
			position215, tokenIndex215, depth215 := position, tokenIndex, depth
			{
				position216 := position
				depth++
				{
					position217, tokenIndex217, depth217 := position, tokenIndex, depth
					if !_rules[rulePointer]() {
						goto l217
					}
					goto l218
				l217:
					position, tokenIndex, depth = position217, tokenIndex217, depth217
				}
			l218:
				if !_rules[ruleDirectDeclarator]() {
					goto l215
				}
				depth--
				add(ruleDeclarator, position216)
			}
			return true
		l215:
			position, tokenIndex, depth = position215, tokenIndex215, depth215
			return false
		},
		/* 33 DirectDeclarator <- <((Identifier / (LPAR Declarator RPAR)) ((LBRK TypeQualifier* AssignmentExpression? RBRK) / (LBRK STATIC TypeQualifier* AssignmentExpression RBRK) / (LBRK TypeQualifier+ STATIC AssignmentExpression RBRK) / (LBRK TypeQualifier* STAR RBRK) / (LPAR ParameterTypeList RPAR) / (LPAR IdentifierList? RPAR))*)> */
		func() bool {
			position219, tokenIndex219, depth219 := position, tokenIndex, depth
			{
				position220 := position
				depth++
				{
					position221, tokenIndex221, depth221 := position, tokenIndex, depth
					if !_rules[ruleIdentifier]() {
						goto l222
					}
					goto l221
				l222:
					position, tokenIndex, depth = position221, tokenIndex221, depth221
					if !_rules[ruleLPAR]() {
						goto l219
					}
					if !_rules[ruleDeclarator]() {
						goto l219
					}
					if !_rules[ruleRPAR]() {
						goto l219
					}
				}
			l221:
			l223:
				{
					position224, tokenIndex224, depth224 := position, tokenIndex, depth
					{
						position225, tokenIndex225, depth225 := position, tokenIndex, depth
						if !_rules[ruleLBRK]() {
							goto l226
						}
					l227:
						{
							position228, tokenIndex228, depth228 := position, tokenIndex, depth
							if !_rules[ruleTypeQualifier]() {
								goto l228
							}
							goto l227
						l228:
							position, tokenIndex, depth = position228, tokenIndex228, depth228
						}
						{
							position229, tokenIndex229, depth229 := position, tokenIndex, depth
							if !_rules[ruleAssignmentExpression]() {
								goto l229
							}
							goto l230
						l229:
							position, tokenIndex, depth = position229, tokenIndex229, depth229
						}
					l230:
						if !_rules[ruleRBRK]() {
							goto l226
						}
						goto l225
					l226:
						position, tokenIndex, depth = position225, tokenIndex225, depth225
						if !_rules[ruleLBRK]() {
							goto l231
						}
						if !_rules[ruleSTATIC]() {
							goto l231
						}
					l232:
						{
							position233, tokenIndex233, depth233 := position, tokenIndex, depth
							if !_rules[ruleTypeQualifier]() {
								goto l233
							}
							goto l232
						l233:
							position, tokenIndex, depth = position233, tokenIndex233, depth233
						}
						if !_rules[ruleAssignmentExpression]() {
							goto l231
						}
						if !_rules[ruleRBRK]() {
							goto l231
						}
						goto l225
					l231:
						position, tokenIndex, depth = position225, tokenIndex225, depth225
						if !_rules[ruleLBRK]() {
							goto l234
						}
						if !_rules[ruleTypeQualifier]() {
							goto l234
						}
					l235:
						{
							position236, tokenIndex236, depth236 := position, tokenIndex, depth
							if !_rules[ruleTypeQualifier]() {
								goto l236
							}
							goto l235
						l236:
							position, tokenIndex, depth = position236, tokenIndex236, depth236
						}
						if !_rules[ruleSTATIC]() {
							goto l234
						}
						if !_rules[ruleAssignmentExpression]() {
							goto l234
						}
						if !_rules[ruleRBRK]() {
							goto l234
						}
						goto l225
					l234:
						position, tokenIndex, depth = position225, tokenIndex225, depth225
						if !_rules[ruleLBRK]() {
							goto l237
						}
					l238:
						{
							position239, tokenIndex239, depth239 := position, tokenIndex, depth
							if !_rules[ruleTypeQualifier]() {
								goto l239
							}
							goto l238
						l239:
							position, tokenIndex, depth = position239, tokenIndex239, depth239
						}
						if !_rules[ruleSTAR]() {
							goto l237
						}
						if !_rules[ruleRBRK]() {
							goto l237
						}
						goto l225
					l237:
						position, tokenIndex, depth = position225, tokenIndex225, depth225
						if !_rules[ruleLPAR]() {
							goto l240
						}
						if !_rules[ruleParameterTypeList]() {
							goto l240
						}
						if !_rules[ruleRPAR]() {
							goto l240
						}
						goto l225
					l240:
						position, tokenIndex, depth = position225, tokenIndex225, depth225
						if !_rules[ruleLPAR]() {
							goto l224
						}
						{
							position241, tokenIndex241, depth241 := position, tokenIndex, depth
							if !_rules[ruleIdentifierList]() {
								goto l241
							}
							goto l242
						l241:
							position, tokenIndex, depth = position241, tokenIndex241, depth241
						}
					l242:
						if !_rules[ruleRPAR]() {
							goto l224
						}
					}
				l225:
					goto l223
				l224:
					position, tokenIndex, depth = position224, tokenIndex224, depth224
				}
				depth--
				add(ruleDirectDeclarator, position220)
			}
			return true
		l219:
			position, tokenIndex, depth = position219, tokenIndex219, depth219
			return false
		},
		/* 34 Pointer <- <(STAR TypeQualifier*)+> */
		func() bool {
			position243, tokenIndex243, depth243 := position, tokenIndex, depth
			{
				position244 := position
				depth++
				if !_rules[ruleSTAR]() {
					goto l243
				}
			l247:
				{
					position248, tokenIndex248, depth248 := position, tokenIndex, depth
					if !_rules[ruleTypeQualifier]() {
						goto l248
					}
					goto l247
				l248:
					position, tokenIndex, depth = position248, tokenIndex248, depth248
				}
			l245:
				{
					position246, tokenIndex246, depth246 := position, tokenIndex, depth
					if !_rules[ruleSTAR]() {
						goto l246
					}
				l249:
					{
						position250, tokenIndex250, depth250 := position, tokenIndex, depth
						if !_rules[ruleTypeQualifier]() {
							goto l250
						}
						goto l249
					l250:
						position, tokenIndex, depth = position250, tokenIndex250, depth250
					}
					goto l245
				l246:
					position, tokenIndex, depth = position246, tokenIndex246, depth246
				}
				depth--
				add(rulePointer, position244)
			}
			return true
		l243:
			position, tokenIndex, depth = position243, tokenIndex243, depth243
			return false
		},
		/* 35 ParameterTypeList <- <(ParameterList (COMMA ELLIPSIS)?)> */
		func() bool {
			position251, tokenIndex251, depth251 := position, tokenIndex, depth
			{
				position252 := position
				depth++
				if !_rules[ruleParameterList]() {
					goto l251
				}
				{
					position253, tokenIndex253, depth253 := position, tokenIndex, depth
					if !_rules[ruleCOMMA]() {
						goto l253
					}
					if !_rules[ruleELLIPSIS]() {
						goto l253
					}
					goto l254
				l253:
					position, tokenIndex, depth = position253, tokenIndex253, depth253
				}
			l254:
				depth--
				add(ruleParameterTypeList, position252)
			}
			return true
		l251:
			position, tokenIndex, depth = position251, tokenIndex251, depth251
			return false
		},
		/* 36 ParameterList <- <(ParameterDeclaration (COMMA ParameterDeclaration)*)> */
		func() bool {
			position255, tokenIndex255, depth255 := position, tokenIndex, depth
			{
				position256 := position
				depth++
				if !_rules[ruleParameterDeclaration]() {
					goto l255
				}
			l257:
				{
					position258, tokenIndex258, depth258 := position, tokenIndex, depth
					if !_rules[ruleCOMMA]() {
						goto l258
					}
					if !_rules[ruleParameterDeclaration]() {
						goto l258
					}
					goto l257
				l258:
					position, tokenIndex, depth = position258, tokenIndex258, depth258
				}
				depth--
				add(ruleParameterList, position256)
			}
			return true
		l255:
			position, tokenIndex, depth = position255, tokenIndex255, depth255
			return false
		},
		/* 37 ParameterDeclaration <- <(DeclarationSpecifiers (Declarator / AbstractDeclarator)?)> */
		func() bool {
			position259, tokenIndex259, depth259 := position, tokenIndex, depth
			{
				position260 := position
				depth++
				if !_rules[ruleDeclarationSpecifiers]() {
					goto l259
				}
				{
					position261, tokenIndex261, depth261 := position, tokenIndex, depth
					{
						position263, tokenIndex263, depth263 := position, tokenIndex, depth
						if !_rules[ruleDeclarator]() {
							goto l264
						}
						goto l263
					l264:
						position, tokenIndex, depth = position263, tokenIndex263, depth263
						if !_rules[ruleAbstractDeclarator]() {
							goto l261
						}
					}
				l263:
					goto l262
				l261:
					position, tokenIndex, depth = position261, tokenIndex261, depth261
				}
			l262:
				depth--
				add(ruleParameterDeclaration, position260)
			}
			return true
		l259:
			position, tokenIndex, depth = position259, tokenIndex259, depth259
			return false
		},
		/* 38 IdentifierList <- <(Identifier (COMMA Identifier)*)> */
		func() bool {
			position265, tokenIndex265, depth265 := position, tokenIndex, depth
			{
				position266 := position
				depth++
				if !_rules[ruleIdentifier]() {
					goto l265
				}
			l267:
				{
					position268, tokenIndex268, depth268 := position, tokenIndex, depth
					if !_rules[ruleCOMMA]() {
						goto l268
					}
					if !_rules[ruleIdentifier]() {
						goto l268
					}
					goto l267
				l268:
					position, tokenIndex, depth = position268, tokenIndex268, depth268
				}
				depth--
				add(ruleIdentifierList, position266)
			}
			return true
		l265:
			position, tokenIndex, depth = position265, tokenIndex265, depth265
			return false
		},
		/* 39 TypeName <- <(SpecifierQualifierList AbstractDeclarator?)> */
		func() bool {
			position269, tokenIndex269, depth269 := position, tokenIndex, depth
			{
				position270 := position
				depth++
				if !_rules[ruleSpecifierQualifierList]() {
					goto l269
				}
				{
					position271, tokenIndex271, depth271 := position, tokenIndex, depth
					if !_rules[ruleAbstractDeclarator]() {
						goto l271
					}
					goto l272
				l271:
					position, tokenIndex, depth = position271, tokenIndex271, depth271
				}
			l272:
				depth--
				add(ruleTypeName, position270)
			}
			return true
		l269:
			position, tokenIndex, depth = position269, tokenIndex269, depth269
			return false
		},
		/* 40 AbstractDeclarator <- <((Pointer? DirectAbstractDeclarator) / Pointer)> */
		func() bool {
			position273, tokenIndex273, depth273 := position, tokenIndex, depth
			{
				position274 := position
				depth++
				{
					position275, tokenIndex275, depth275 := position, tokenIndex, depth
					{
						position277, tokenIndex277, depth277 := position, tokenIndex, depth
						if !_rules[rulePointer]() {
							goto l277
						}
						goto l278
					l277:
						position, tokenIndex, depth = position277, tokenIndex277, depth277
					}
				l278:
					if !_rules[ruleDirectAbstractDeclarator]() {
						goto l276
					}
					goto l275
				l276:
					position, tokenIndex, depth = position275, tokenIndex275, depth275
					if !_rules[rulePointer]() {
						goto l273
					}
				}
			l275:
				depth--
				add(ruleAbstractDeclarator, position274)
			}
			return true
		l273:
			position, tokenIndex, depth = position273, tokenIndex273, depth273
			return false
		},
		/* 41 DirectAbstractDeclarator <- <(((LPAR AbstractDeclarator RPAR) / (LBRK (AssignmentExpression / STAR)? RBRK) / (LPAR ParameterTypeList? RPAR)) ((LBRK (AssignmentExpression / STAR)? RBRK) / (LPAR ParameterTypeList? RPAR))*)> */
		func() bool {
			position279, tokenIndex279, depth279 := position, tokenIndex, depth
			{
				position280 := position
				depth++
				{
					position281, tokenIndex281, depth281 := position, tokenIndex, depth
					if !_rules[ruleLPAR]() {
						goto l282
					}
					if !_rules[ruleAbstractDeclarator]() {
						goto l282
					}
					if !_rules[ruleRPAR]() {
						goto l282
					}
					goto l281
				l282:
					position, tokenIndex, depth = position281, tokenIndex281, depth281
					if !_rules[ruleLBRK]() {
						goto l283
					}
					{
						position284, tokenIndex284, depth284 := position, tokenIndex, depth
						{
							position286, tokenIndex286, depth286 := position, tokenIndex, depth
							if !_rules[ruleAssignmentExpression]() {
								goto l287
							}
							goto l286
						l287:
							position, tokenIndex, depth = position286, tokenIndex286, depth286
							if !_rules[ruleSTAR]() {
								goto l284
							}
						}
					l286:
						goto l285
					l284:
						position, tokenIndex, depth = position284, tokenIndex284, depth284
					}
				l285:
					if !_rules[ruleRBRK]() {
						goto l283
					}
					goto l281
				l283:
					position, tokenIndex, depth = position281, tokenIndex281, depth281
					if !_rules[ruleLPAR]() {
						goto l279
					}
					{
						position288, tokenIndex288, depth288 := position, tokenIndex, depth
						if !_rules[ruleParameterTypeList]() {
							goto l288
						}
						goto l289
					l288:
						position, tokenIndex, depth = position288, tokenIndex288, depth288
					}
				l289:
					if !_rules[ruleRPAR]() {
						goto l279
					}
				}
			l281:
			l290:
				{
					position291, tokenIndex291, depth291 := position, tokenIndex, depth
					{
						position292, tokenIndex292, depth292 := position, tokenIndex, depth
						if !_rules[ruleLBRK]() {
							goto l293
						}
						{
							position294, tokenIndex294, depth294 := position, tokenIndex, depth
							{
								position296, tokenIndex296, depth296 := position, tokenIndex, depth
								if !_rules[ruleAssignmentExpression]() {
									goto l297
								}
								goto l296
							l297:
								position, tokenIndex, depth = position296, tokenIndex296, depth296
								if !_rules[ruleSTAR]() {
									goto l294
								}
							}
						l296:
							goto l295
						l294:
							position, tokenIndex, depth = position294, tokenIndex294, depth294
						}
					l295:
						if !_rules[ruleRBRK]() {
							goto l293
						}
						goto l292
					l293:
						position, tokenIndex, depth = position292, tokenIndex292, depth292
						if !_rules[ruleLPAR]() {
							goto l291
						}
						{
							position298, tokenIndex298, depth298 := position, tokenIndex, depth
							if !_rules[ruleParameterTypeList]() {
								goto l298
							}
							goto l299
						l298:
							position, tokenIndex, depth = position298, tokenIndex298, depth298
						}
					l299:
						if !_rules[ruleRPAR]() {
							goto l291
						}
					}
				l292:
					goto l290
				l291:
					position, tokenIndex, depth = position291, tokenIndex291, depth291
				}
				depth--
				add(ruleDirectAbstractDeclarator, position280)
			}
			return true
		l279:
			position, tokenIndex, depth = position279, tokenIndex279, depth279
			return false
		},
		/* 42 TypedefName <- <Identifier> */
		func() bool {
			position300, tokenIndex300, depth300 := position, tokenIndex, depth
			{
				position301 := position
				depth++
				if !_rules[ruleIdentifier]() {
					goto l300
				}
				depth--
				add(ruleTypedefName, position301)
			}
			return true
		l300:
			position, tokenIndex, depth = position300, tokenIndex300, depth300
			return false
		},
		/* 43 Initializer <- <(AssignmentExpression / (LWING InitializerList COMMA? RWING))> */
		func() bool {
			position302, tokenIndex302, depth302 := position, tokenIndex, depth
			{
				position303 := position
				depth++
				{
					position304, tokenIndex304, depth304 := position, tokenIndex, depth
					if !_rules[ruleAssignmentExpression]() {
						goto l305
					}
					goto l304
				l305:
					position, tokenIndex, depth = position304, tokenIndex304, depth304
					if !_rules[ruleLWING]() {
						goto l302
					}
					if !_rules[ruleInitializerList]() {
						goto l302
					}
					{
						position306, tokenIndex306, depth306 := position, tokenIndex, depth
						if !_rules[ruleCOMMA]() {
							goto l306
						}
						goto l307
					l306:
						position, tokenIndex, depth = position306, tokenIndex306, depth306
					}
				l307:
					if !_rules[ruleRWING]() {
						goto l302
					}
				}
			l304:
				depth--
				add(ruleInitializer, position303)
			}
			return true
		l302:
			position, tokenIndex, depth = position302, tokenIndex302, depth302
			return false
		},
		/* 44 InitializerList <- <(Designation? Initializer (COMMA Designation? Initializer)*)> */
		func() bool {
			position308, tokenIndex308, depth308 := position, tokenIndex, depth
			{
				position309 := position
				depth++
				{
					position310, tokenIndex310, depth310 := position, tokenIndex, depth
					if !_rules[ruleDesignation]() {
						goto l310
					}
					goto l311
				l310:
					position, tokenIndex, depth = position310, tokenIndex310, depth310
				}
			l311:
				if !_rules[ruleInitializer]() {
					goto l308
				}
			l312:
				{
					position313, tokenIndex313, depth313 := position, tokenIndex, depth
					if !_rules[ruleCOMMA]() {
						goto l313
					}
					{
						position314, tokenIndex314, depth314 := position, tokenIndex, depth
						if !_rules[ruleDesignation]() {
							goto l314
						}
						goto l315
					l314:
						position, tokenIndex, depth = position314, tokenIndex314, depth314
					}
				l315:
					if !_rules[ruleInitializer]() {
						goto l313
					}
					goto l312
				l313:
					position, tokenIndex, depth = position313, tokenIndex313, depth313
				}
				depth--
				add(ruleInitializerList, position309)
			}
			return true
		l308:
			position, tokenIndex, depth = position308, tokenIndex308, depth308
			return false
		},
		/* 45 Designation <- <(Designator+ EQU)> */
		func() bool {
			position316, tokenIndex316, depth316 := position, tokenIndex, depth
			{
				position317 := position
				depth++
				if !_rules[ruleDesignator]() {
					goto l316
				}
			l318:
				{
					position319, tokenIndex319, depth319 := position, tokenIndex, depth
					if !_rules[ruleDesignator]() {
						goto l319
					}
					goto l318
				l319:
					position, tokenIndex, depth = position319, tokenIndex319, depth319
				}
				if !_rules[ruleEQU]() {
					goto l316
				}
				depth--
				add(ruleDesignation, position317)
			}
			return true
		l316:
			position, tokenIndex, depth = position316, tokenIndex316, depth316
			return false
		},
		/* 46 Designator <- <((LBRK ConstantExpression RBRK) / (DOT Identifier))> */
		func() bool {
			position320, tokenIndex320, depth320 := position, tokenIndex, depth
			{
				position321 := position
				depth++
				{
					position322, tokenIndex322, depth322 := position, tokenIndex, depth
					if !_rules[ruleLBRK]() {
						goto l323
					}
					if !_rules[ruleConstantExpression]() {
						goto l323
					}
					if !_rules[ruleRBRK]() {
						goto l323
					}
					goto l322
				l323:
					position, tokenIndex, depth = position322, tokenIndex322, depth322
					if !_rules[ruleDOT]() {
						goto l320
					}
					if !_rules[ruleIdentifier]() {
						goto l320
					}
				}
			l322:
				depth--
				add(ruleDesignator, position321)
			}
			return true
		l320:
			position, tokenIndex, depth = position320, tokenIndex320, depth320
			return false
		},
		/* 47 Statement <- <(LabeledStatement / CompoundStatement / ExpressionStatement / SelectionStatement / IterationStatement / JumpStatement)> */
		func() bool {
			position324, tokenIndex324, depth324 := position, tokenIndex, depth
			{
				position325 := position
				depth++
				{
					position326, tokenIndex326, depth326 := position, tokenIndex, depth
					if !_rules[ruleLabeledStatement]() {
						goto l327
					}
					goto l326
				l327:
					position, tokenIndex, depth = position326, tokenIndex326, depth326
					if !_rules[ruleCompoundStatement]() {
						goto l328
					}
					goto l326
				l328:
					position, tokenIndex, depth = position326, tokenIndex326, depth326
					if !_rules[ruleExpressionStatement]() {
						goto l329
					}
					goto l326
				l329:
					position, tokenIndex, depth = position326, tokenIndex326, depth326
					if !_rules[ruleSelectionStatement]() {
						goto l330
					}
					goto l326
				l330:
					position, tokenIndex, depth = position326, tokenIndex326, depth326
					if !_rules[ruleIterationStatement]() {
						goto l331
					}
					goto l326
				l331:
					position, tokenIndex, depth = position326, tokenIndex326, depth326
					if !_rules[ruleJumpStatement]() {
						goto l324
					}
				}
			l326:
				depth--
				add(ruleStatement, position325)
			}
			return true
		l324:
			position, tokenIndex, depth = position324, tokenIndex324, depth324
			return false
		},
		/* 48 LabeledStatement <- <((Identifier COLON Statement) / (CASE ConstantExpression COLON Statement) / (DEFAULT COLON Statement))> */
		func() bool {
			position332, tokenIndex332, depth332 := position, tokenIndex, depth
			{
				position333 := position
				depth++
				{
					position334, tokenIndex334, depth334 := position, tokenIndex, depth
					if !_rules[ruleIdentifier]() {
						goto l335
					}
					if !_rules[ruleCOLON]() {
						goto l335
					}
					if !_rules[ruleStatement]() {
						goto l335
					}
					goto l334
				l335:
					position, tokenIndex, depth = position334, tokenIndex334, depth334
					if !_rules[ruleCASE]() {
						goto l336
					}
					if !_rules[ruleConstantExpression]() {
						goto l336
					}
					if !_rules[ruleCOLON]() {
						goto l336
					}
					if !_rules[ruleStatement]() {
						goto l336
					}
					goto l334
				l336:
					position, tokenIndex, depth = position334, tokenIndex334, depth334
					if !_rules[ruleDEFAULT]() {
						goto l332
					}
					if !_rules[ruleCOLON]() {
						goto l332
					}
					if !_rules[ruleStatement]() {
						goto l332
					}
				}
			l334:
				depth--
				add(ruleLabeledStatement, position333)
			}
			return true
		l332:
			position, tokenIndex, depth = position332, tokenIndex332, depth332
			return false
		},
		/* 49 CompoundStatement <- <(LWING (Declaration / Statement)* RWING)> */
		func() bool {
			position337, tokenIndex337, depth337 := position, tokenIndex, depth
			{
				position338 := position
				depth++
				if !_rules[ruleLWING]() {
					goto l337
				}
			l339:
				{
					position340, tokenIndex340, depth340 := position, tokenIndex, depth
					{
						position341, tokenIndex341, depth341 := position, tokenIndex, depth
						if !_rules[ruleDeclaration]() {
							goto l342
						}
						goto l341
					l342:
						position, tokenIndex, depth = position341, tokenIndex341, depth341
						if !_rules[ruleStatement]() {
							goto l340
						}
					}
				l341:
					goto l339
				l340:
					position, tokenIndex, depth = position340, tokenIndex340, depth340
				}
				if !_rules[ruleRWING]() {
					goto l337
				}
				depth--
				add(ruleCompoundStatement, position338)
			}
			return true
		l337:
			position, tokenIndex, depth = position337, tokenIndex337, depth337
			return false
		},
		/* 50 ExpressionStatement <- <(Expression? SEMI)> */
		func() bool {
			position343, tokenIndex343, depth343 := position, tokenIndex, depth
			{
				position344 := position
				depth++
				{
					position345, tokenIndex345, depth345 := position, tokenIndex, depth
					if !_rules[ruleExpression]() {
						goto l345
					}
					goto l346
				l345:
					position, tokenIndex, depth = position345, tokenIndex345, depth345
				}
			l346:
				if !_rules[ruleSEMI]() {
					goto l343
				}
				depth--
				add(ruleExpressionStatement, position344)
			}
			return true
		l343:
			position, tokenIndex, depth = position343, tokenIndex343, depth343
			return false
		},
		/* 51 SelectionStatement <- <((IF LPAR Expression RPAR Statement (ELSE Statement)?) / (SWITCH LPAR Expression RPAR Statement))> */
		func() bool {
			position347, tokenIndex347, depth347 := position, tokenIndex, depth
			{
				position348 := position
				depth++
				{
					position349, tokenIndex349, depth349 := position, tokenIndex, depth
					if !_rules[ruleIF]() {
						goto l350
					}
					if !_rules[ruleLPAR]() {
						goto l350
					}
					if !_rules[ruleExpression]() {
						goto l350
					}
					if !_rules[ruleRPAR]() {
						goto l350
					}
					if !_rules[ruleStatement]() {
						goto l350
					}
					{
						position351, tokenIndex351, depth351 := position, tokenIndex, depth
						if !_rules[ruleELSE]() {
							goto l351
						}
						if !_rules[ruleStatement]() {
							goto l351
						}
						goto l352
					l351:
						position, tokenIndex, depth = position351, tokenIndex351, depth351
					}
				l352:
					goto l349
				l350:
					position, tokenIndex, depth = position349, tokenIndex349, depth349
					if !_rules[ruleSWITCH]() {
						goto l347
					}
					if !_rules[ruleLPAR]() {
						goto l347
					}
					if !_rules[ruleExpression]() {
						goto l347
					}
					if !_rules[ruleRPAR]() {
						goto l347
					}
					if !_rules[ruleStatement]() {
						goto l347
					}
				}
			l349:
				depth--
				add(ruleSelectionStatement, position348)
			}
			return true
		l347:
			position, tokenIndex, depth = position347, tokenIndex347, depth347
			return false
		},
		/* 52 IterationStatement <- <((WHILE LPAR Expression RPAR Statement) / (DO Statement WHILE LPAR Expression RPAR SEMI) / (FOR LPAR Expression? SEMI Expression? SEMI Expression? RPAR Statement) / (FOR LPAR Declaration Expression? SEMI Expression? RPAR Statement))> */
		func() bool {
			position353, tokenIndex353, depth353 := position, tokenIndex, depth
			{
				position354 := position
				depth++
				{
					position355, tokenIndex355, depth355 := position, tokenIndex, depth
					if !_rules[ruleWHILE]() {
						goto l356
					}
					if !_rules[ruleLPAR]() {
						goto l356
					}
					if !_rules[ruleExpression]() {
						goto l356
					}
					if !_rules[ruleRPAR]() {
						goto l356
					}
					if !_rules[ruleStatement]() {
						goto l356
					}
					goto l355
				l356:
					position, tokenIndex, depth = position355, tokenIndex355, depth355
					if !_rules[ruleDO]() {
						goto l357
					}
					if !_rules[ruleStatement]() {
						goto l357
					}
					if !_rules[ruleWHILE]() {
						goto l357
					}
					if !_rules[ruleLPAR]() {
						goto l357
					}
					if !_rules[ruleExpression]() {
						goto l357
					}
					if !_rules[ruleRPAR]() {
						goto l357
					}
					if !_rules[ruleSEMI]() {
						goto l357
					}
					goto l355
				l357:
					position, tokenIndex, depth = position355, tokenIndex355, depth355
					if !_rules[ruleFOR]() {
						goto l358
					}
					if !_rules[ruleLPAR]() {
						goto l358
					}
					{
						position359, tokenIndex359, depth359 := position, tokenIndex, depth
						if !_rules[ruleExpression]() {
							goto l359
						}
						goto l360
					l359:
						position, tokenIndex, depth = position359, tokenIndex359, depth359
					}
				l360:
					if !_rules[ruleSEMI]() {
						goto l358
					}
					{
						position361, tokenIndex361, depth361 := position, tokenIndex, depth
						if !_rules[ruleExpression]() {
							goto l361
						}
						goto l362
					l361:
						position, tokenIndex, depth = position361, tokenIndex361, depth361
					}
				l362:
					if !_rules[ruleSEMI]() {
						goto l358
					}
					{
						position363, tokenIndex363, depth363 := position, tokenIndex, depth
						if !_rules[ruleExpression]() {
							goto l363
						}
						goto l364
					l363:
						position, tokenIndex, depth = position363, tokenIndex363, depth363
					}
				l364:
					if !_rules[ruleRPAR]() {
						goto l358
					}
					if !_rules[ruleStatement]() {
						goto l358
					}
					goto l355
				l358:
					position, tokenIndex, depth = position355, tokenIndex355, depth355
					if !_rules[ruleFOR]() {
						goto l353
					}
					if !_rules[ruleLPAR]() {
						goto l353
					}
					if !_rules[ruleDeclaration]() {
						goto l353
					}
					{
						position365, tokenIndex365, depth365 := position, tokenIndex, depth
						if !_rules[ruleExpression]() {
							goto l365
						}
						goto l366
					l365:
						position, tokenIndex, depth = position365, tokenIndex365, depth365
					}
				l366:
					if !_rules[ruleSEMI]() {
						goto l353
					}
					{
						position367, tokenIndex367, depth367 := position, tokenIndex, depth
						if !_rules[ruleExpression]() {
							goto l367
						}
						goto l368
					l367:
						position, tokenIndex, depth = position367, tokenIndex367, depth367
					}
				l368:
					if !_rules[ruleRPAR]() {
						goto l353
					}
					if !_rules[ruleStatement]() {
						goto l353
					}
				}
			l355:
				depth--
				add(ruleIterationStatement, position354)
			}
			return true
		l353:
			position, tokenIndex, depth = position353, tokenIndex353, depth353
			return false
		},
		/* 53 JumpStatement <- <((GOTO Identifier SEMI) / (CONTINUE SEMI) / (BREAK SEMI) / (RETURN Expression? SEMI))> */
		func() bool {
			position369, tokenIndex369, depth369 := position, tokenIndex, depth
			{
				position370 := position
				depth++
				{
					position371, tokenIndex371, depth371 := position, tokenIndex, depth
					if !_rules[ruleGOTO]() {
						goto l372
					}
					if !_rules[ruleIdentifier]() {
						goto l372
					}
					if !_rules[ruleSEMI]() {
						goto l372
					}
					goto l371
				l372:
					position, tokenIndex, depth = position371, tokenIndex371, depth371
					if !_rules[ruleCONTINUE]() {
						goto l373
					}
					if !_rules[ruleSEMI]() {
						goto l373
					}
					goto l371
				l373:
					position, tokenIndex, depth = position371, tokenIndex371, depth371
					if !_rules[ruleBREAK]() {
						goto l374
					}
					if !_rules[ruleSEMI]() {
						goto l374
					}
					goto l371
				l374:
					position, tokenIndex, depth = position371, tokenIndex371, depth371
					if !_rules[ruleRETURN]() {
						goto l369
					}
					{
						position375, tokenIndex375, depth375 := position, tokenIndex, depth
						if !_rules[ruleExpression]() {
							goto l375
						}
						goto l376
					l375:
						position, tokenIndex, depth = position375, tokenIndex375, depth375
					}
				l376:
					if !_rules[ruleSEMI]() {
						goto l369
					}
				}
			l371:
				depth--
				add(ruleJumpStatement, position370)
			}
			return true
		l369:
			position, tokenIndex, depth = position369, tokenIndex369, depth369
			return false
		},
		/* 54 PrimaryExpression <- <(Identifier / Constant / StringLiteral / (LPAR Expression RPAR))> */
		func() bool {
			position377, tokenIndex377, depth377 := position, tokenIndex, depth
			{
				position378 := position
				depth++
				{
					position379, tokenIndex379, depth379 := position, tokenIndex, depth
					if !_rules[ruleIdentifier]() {
						goto l380
					}
					goto l379
				l380:
					position, tokenIndex, depth = position379, tokenIndex379, depth379
					if !_rules[ruleConstant]() {
						goto l381
					}
					goto l379
				l381:
					position, tokenIndex, depth = position379, tokenIndex379, depth379
					if !_rules[ruleStringLiteral]() {
						goto l382
					}
					goto l379
				l382:
					position, tokenIndex, depth = position379, tokenIndex379, depth379
					if !_rules[ruleLPAR]() {
						goto l377
					}
					if !_rules[ruleExpression]() {
						goto l377
					}
					if !_rules[ruleRPAR]() {
						goto l377
					}
				}
			l379:
				depth--
				add(rulePrimaryExpression, position378)
			}
			return true
		l377:
			position, tokenIndex, depth = position377, tokenIndex377, depth377
			return false
		},
		/* 55 PostfixExpression <- <((PrimaryExpression / (LPAR TypeName RPAR LWING InitializerList COMMA? RWING)) ((LBRK Expression RBRK) / (LPAR ArgumentExpressionList? RPAR) / (DOT Identifier) / (PTR Identifier) / INC / DEC)*)> */
		func() bool {
			position383, tokenIndex383, depth383 := position, tokenIndex, depth
			{
				position384 := position
				depth++
				{
					position385, tokenIndex385, depth385 := position, tokenIndex, depth
					if !_rules[rulePrimaryExpression]() {
						goto l386
					}
					goto l385
				l386:
					position, tokenIndex, depth = position385, tokenIndex385, depth385
					if !_rules[ruleLPAR]() {
						goto l383
					}
					if !_rules[ruleTypeName]() {
						goto l383
					}
					if !_rules[ruleRPAR]() {
						goto l383
					}
					if !_rules[ruleLWING]() {
						goto l383
					}
					if !_rules[ruleInitializerList]() {
						goto l383
					}
					{
						position387, tokenIndex387, depth387 := position, tokenIndex, depth
						if !_rules[ruleCOMMA]() {
							goto l387
						}
						goto l388
					l387:
						position, tokenIndex, depth = position387, tokenIndex387, depth387
					}
				l388:
					if !_rules[ruleRWING]() {
						goto l383
					}
				}
			l385:
			l389:
				{
					position390, tokenIndex390, depth390 := position, tokenIndex, depth
					{
						position391, tokenIndex391, depth391 := position, tokenIndex, depth
						if !_rules[ruleLBRK]() {
							goto l392
						}
						if !_rules[ruleExpression]() {
							goto l392
						}
						if !_rules[ruleRBRK]() {
							goto l392
						}
						goto l391
					l392:
						position, tokenIndex, depth = position391, tokenIndex391, depth391
						if !_rules[ruleLPAR]() {
							goto l393
						}
						{
							position394, tokenIndex394, depth394 := position, tokenIndex, depth
							if !_rules[ruleArgumentExpressionList]() {
								goto l394
							}
							goto l395
						l394:
							position, tokenIndex, depth = position394, tokenIndex394, depth394
						}
					l395:
						if !_rules[ruleRPAR]() {
							goto l393
						}
						goto l391
					l393:
						position, tokenIndex, depth = position391, tokenIndex391, depth391
						if !_rules[ruleDOT]() {
							goto l396
						}
						if !_rules[ruleIdentifier]() {
							goto l396
						}
						goto l391
					l396:
						position, tokenIndex, depth = position391, tokenIndex391, depth391
						if !_rules[rulePTR]() {
							goto l397
						}
						if !_rules[ruleIdentifier]() {
							goto l397
						}
						goto l391
					l397:
						position, tokenIndex, depth = position391, tokenIndex391, depth391
						if !_rules[ruleINC]() {
							goto l398
						}
						goto l391
					l398:
						position, tokenIndex, depth = position391, tokenIndex391, depth391
						if !_rules[ruleDEC]() {
							goto l390
						}
					}
				l391:
					goto l389
				l390:
					position, tokenIndex, depth = position390, tokenIndex390, depth390
				}
				depth--
				add(rulePostfixExpression, position384)
			}
			return true
		l383:
			position, tokenIndex, depth = position383, tokenIndex383, depth383
			return false
		},
		/* 56 ArgumentExpressionList <- <(AssignmentExpression (COMMA AssignmentExpression)*)> */
		func() bool {
			position399, tokenIndex399, depth399 := position, tokenIndex, depth
			{
				position400 := position
				depth++
				if !_rules[ruleAssignmentExpression]() {
					goto l399
				}
			l401:
				{
					position402, tokenIndex402, depth402 := position, tokenIndex, depth
					if !_rules[ruleCOMMA]() {
						goto l402
					}
					if !_rules[ruleAssignmentExpression]() {
						goto l402
					}
					goto l401
				l402:
					position, tokenIndex, depth = position402, tokenIndex402, depth402
				}
				depth--
				add(ruleArgumentExpressionList, position400)
			}
			return true
		l399:
			position, tokenIndex, depth = position399, tokenIndex399, depth399
			return false
		},
		/* 57 UnaryExpression <- <(PostfixExpression / (INC UnaryExpression) / (DEC UnaryExpression) / (UnaryOperator CastExpression) / (SIZEOF (UnaryExpression / (LPAR TypeName RPAR))))> */
		func() bool {
			position403, tokenIndex403, depth403 := position, tokenIndex, depth
			{
				position404 := position
				depth++
				{
					position405, tokenIndex405, depth405 := position, tokenIndex, depth
					if !_rules[rulePostfixExpression]() {
						goto l406
					}
					goto l405
				l406:
					position, tokenIndex, depth = position405, tokenIndex405, depth405
					if !_rules[ruleINC]() {
						goto l407
					}
					if !_rules[ruleUnaryExpression]() {
						goto l407
					}
					goto l405
				l407:
					position, tokenIndex, depth = position405, tokenIndex405, depth405
					if !_rules[ruleDEC]() {
						goto l408
					}
					if !_rules[ruleUnaryExpression]() {
						goto l408
					}
					goto l405
				l408:
					position, tokenIndex, depth = position405, tokenIndex405, depth405
					if !_rules[ruleUnaryOperator]() {
						goto l409
					}
					if !_rules[ruleCastExpression]() {
						goto l409
					}
					goto l405
				l409:
					position, tokenIndex, depth = position405, tokenIndex405, depth405
					if !_rules[ruleSIZEOF]() {
						goto l403
					}
					{
						position410, tokenIndex410, depth410 := position, tokenIndex, depth
						if !_rules[ruleUnaryExpression]() {
							goto l411
						}
						goto l410
					l411:
						position, tokenIndex, depth = position410, tokenIndex410, depth410
						if !_rules[ruleLPAR]() {
							goto l403
						}
						if !_rules[ruleTypeName]() {
							goto l403
						}
						if !_rules[ruleRPAR]() {
							goto l403
						}
					}
				l410:
				}
			l405:
				depth--
				add(ruleUnaryExpression, position404)
			}
			return true
		l403:
			position, tokenIndex, depth = position403, tokenIndex403, depth403
			return false
		},
		/* 58 UnaryOperator <- <(AND / STAR / PLUS / MINUS / TILDA / BANG)> */
		func() bool {
			position412, tokenIndex412, depth412 := position, tokenIndex, depth
			{
				position413 := position
				depth++
				{
					position414, tokenIndex414, depth414 := position, tokenIndex, depth
					if !_rules[ruleAND]() {
						goto l415
					}
					goto l414
				l415:
					position, tokenIndex, depth = position414, tokenIndex414, depth414
					if !_rules[ruleSTAR]() {
						goto l416
					}
					goto l414
				l416:
					position, tokenIndex, depth = position414, tokenIndex414, depth414
					if !_rules[rulePLUS]() {
						goto l417
					}
					goto l414
				l417:
					position, tokenIndex, depth = position414, tokenIndex414, depth414
					if !_rules[ruleMINUS]() {
						goto l418
					}
					goto l414
				l418:
					position, tokenIndex, depth = position414, tokenIndex414, depth414
					if !_rules[ruleTILDA]() {
						goto l419
					}
					goto l414
				l419:
					position, tokenIndex, depth = position414, tokenIndex414, depth414
					if !_rules[ruleBANG]() {
						goto l412
					}
				}
			l414:
				depth--
				add(ruleUnaryOperator, position413)
			}
			return true
		l412:
			position, tokenIndex, depth = position412, tokenIndex412, depth412
			return false
		},
		/* 59 CastExpression <- <((LPAR TypeName RPAR CastExpression) / UnaryExpression)> */
		func() bool {
			position420, tokenIndex420, depth420 := position, tokenIndex, depth
			{
				position421 := position
				depth++
				{
					position422, tokenIndex422, depth422 := position, tokenIndex, depth
					if !_rules[ruleLPAR]() {
						goto l423
					}
					if !_rules[ruleTypeName]() {
						goto l423
					}
					if !_rules[ruleRPAR]() {
						goto l423
					}
					if !_rules[ruleCastExpression]() {
						goto l423
					}
					goto l422
				l423:
					position, tokenIndex, depth = position422, tokenIndex422, depth422
					if !_rules[ruleUnaryExpression]() {
						goto l420
					}
				}
			l422:
				depth--
				add(ruleCastExpression, position421)
			}
			return true
		l420:
			position, tokenIndex, depth = position420, tokenIndex420, depth420
			return false
		},
		/* 60 MultiplicativeExpression <- <(CastExpression ((STAR / DIV / MOD) CastExpression)*)> */
		func() bool {
			position424, tokenIndex424, depth424 := position, tokenIndex, depth
			{
				position425 := position
				depth++
				if !_rules[ruleCastExpression]() {
					goto l424
				}
			l426:
				{
					position427, tokenIndex427, depth427 := position, tokenIndex, depth
					{
						position428, tokenIndex428, depth428 := position, tokenIndex, depth
						if !_rules[ruleSTAR]() {
							goto l429
						}
						goto l428
					l429:
						position, tokenIndex, depth = position428, tokenIndex428, depth428
						if !_rules[ruleDIV]() {
							goto l430
						}
						goto l428
					l430:
						position, tokenIndex, depth = position428, tokenIndex428, depth428
						if !_rules[ruleMOD]() {
							goto l427
						}
					}
				l428:
					if !_rules[ruleCastExpression]() {
						goto l427
					}
					goto l426
				l427:
					position, tokenIndex, depth = position427, tokenIndex427, depth427
				}
				depth--
				add(ruleMultiplicativeExpression, position425)
			}
			return true
		l424:
			position, tokenIndex, depth = position424, tokenIndex424, depth424
			return false
		},
		/* 61 AdditiveExpression <- <(MultiplicativeExpression ((PLUS / MINUS) MultiplicativeExpression)*)> */
		func() bool {
			position431, tokenIndex431, depth431 := position, tokenIndex, depth
			{
				position432 := position
				depth++
				if !_rules[ruleMultiplicativeExpression]() {
					goto l431
				}
			l433:
				{
					position434, tokenIndex434, depth434 := position, tokenIndex, depth
					{
						position435, tokenIndex435, depth435 := position, tokenIndex, depth
						if !_rules[rulePLUS]() {
							goto l436
						}
						goto l435
					l436:
						position, tokenIndex, depth = position435, tokenIndex435, depth435
						if !_rules[ruleMINUS]() {
							goto l434
						}
					}
				l435:
					if !_rules[ruleMultiplicativeExpression]() {
						goto l434
					}
					goto l433
				l434:
					position, tokenIndex, depth = position434, tokenIndex434, depth434
				}
				depth--
				add(ruleAdditiveExpression, position432)
			}
			return true
		l431:
			position, tokenIndex, depth = position431, tokenIndex431, depth431
			return false
		},
		/* 62 ShiftExpression <- <(AdditiveExpression ((LEFT / RIGHT) AdditiveExpression)*)> */
		func() bool {
			position437, tokenIndex437, depth437 := position, tokenIndex, depth
			{
				position438 := position
				depth++
				if !_rules[ruleAdditiveExpression]() {
					goto l437
				}
			l439:
				{
					position440, tokenIndex440, depth440 := position, tokenIndex, depth
					{
						position441, tokenIndex441, depth441 := position, tokenIndex, depth
						if !_rules[ruleLEFT]() {
							goto l442
						}
						goto l441
					l442:
						position, tokenIndex, depth = position441, tokenIndex441, depth441
						if !_rules[ruleRIGHT]() {
							goto l440
						}
					}
				l441:
					if !_rules[ruleAdditiveExpression]() {
						goto l440
					}
					goto l439
				l440:
					position, tokenIndex, depth = position440, tokenIndex440, depth440
				}
				depth--
				add(ruleShiftExpression, position438)
			}
			return true
		l437:
			position, tokenIndex, depth = position437, tokenIndex437, depth437
			return false
		},
		/* 63 RelationalExpression <- <(ShiftExpression ((LE / GE / LT / GT) ShiftExpression)*)> */
		func() bool {
			position443, tokenIndex443, depth443 := position, tokenIndex, depth
			{
				position444 := position
				depth++
				if !_rules[ruleShiftExpression]() {
					goto l443
				}
			l445:
				{
					position446, tokenIndex446, depth446 := position, tokenIndex, depth
					{
						position447, tokenIndex447, depth447 := position, tokenIndex, depth
						if !_rules[ruleLE]() {
							goto l448
						}
						goto l447
					l448:
						position, tokenIndex, depth = position447, tokenIndex447, depth447
						if !_rules[ruleGE]() {
							goto l449
						}
						goto l447
					l449:
						position, tokenIndex, depth = position447, tokenIndex447, depth447
						if !_rules[ruleLT]() {
							goto l450
						}
						goto l447
					l450:
						position, tokenIndex, depth = position447, tokenIndex447, depth447
						if !_rules[ruleGT]() {
							goto l446
						}
					}
				l447:
					if !_rules[ruleShiftExpression]() {
						goto l446
					}
					goto l445
				l446:
					position, tokenIndex, depth = position446, tokenIndex446, depth446
				}
				depth--
				add(ruleRelationalExpression, position444)
			}
			return true
		l443:
			position, tokenIndex, depth = position443, tokenIndex443, depth443
			return false
		},
		/* 64 EqualityExpression <- <(RelationalExpression ((EQUEQU / BANGEQU) RelationalExpression)*)> */
		func() bool {
			position451, tokenIndex451, depth451 := position, tokenIndex, depth
			{
				position452 := position
				depth++
				if !_rules[ruleRelationalExpression]() {
					goto l451
				}
			l453:
				{
					position454, tokenIndex454, depth454 := position, tokenIndex, depth
					{
						position455, tokenIndex455, depth455 := position, tokenIndex, depth
						if !_rules[ruleEQUEQU]() {
							goto l456
						}
						goto l455
					l456:
						position, tokenIndex, depth = position455, tokenIndex455, depth455
						if !_rules[ruleBANGEQU]() {
							goto l454
						}
					}
				l455:
					if !_rules[ruleRelationalExpression]() {
						goto l454
					}
					goto l453
				l454:
					position, tokenIndex, depth = position454, tokenIndex454, depth454
				}
				depth--
				add(ruleEqualityExpression, position452)
			}
			return true
		l451:
			position, tokenIndex, depth = position451, tokenIndex451, depth451
			return false
		},
		/* 65 ANDExpression <- <(EqualityExpression (AND EqualityExpression)*)> */
		func() bool {
			position457, tokenIndex457, depth457 := position, tokenIndex, depth
			{
				position458 := position
				depth++
				if !_rules[ruleEqualityExpression]() {
					goto l457
				}
			l459:
				{
					position460, tokenIndex460, depth460 := position, tokenIndex, depth
					if !_rules[ruleAND]() {
						goto l460
					}
					if !_rules[ruleEqualityExpression]() {
						goto l460
					}
					goto l459
				l460:
					position, tokenIndex, depth = position460, tokenIndex460, depth460
				}
				depth--
				add(ruleANDExpression, position458)
			}
			return true
		l457:
			position, tokenIndex, depth = position457, tokenIndex457, depth457
			return false
		},
		/* 66 ExclusiveORExpression <- <(ANDExpression (HAT ANDExpression)*)> */
		func() bool {
			position461, tokenIndex461, depth461 := position, tokenIndex, depth
			{
				position462 := position
				depth++
				if !_rules[ruleANDExpression]() {
					goto l461
				}
			l463:
				{
					position464, tokenIndex464, depth464 := position, tokenIndex, depth
					if !_rules[ruleHAT]() {
						goto l464
					}
					if !_rules[ruleANDExpression]() {
						goto l464
					}
					goto l463
				l464:
					position, tokenIndex, depth = position464, tokenIndex464, depth464
				}
				depth--
				add(ruleExclusiveORExpression, position462)
			}
			return true
		l461:
			position, tokenIndex, depth = position461, tokenIndex461, depth461
			return false
		},
		/* 67 InclusiveORExpression <- <(ExclusiveORExpression (OR ExclusiveORExpression)*)> */
		func() bool {
			position465, tokenIndex465, depth465 := position, tokenIndex, depth
			{
				position466 := position
				depth++
				if !_rules[ruleExclusiveORExpression]() {
					goto l465
				}
			l467:
				{
					position468, tokenIndex468, depth468 := position, tokenIndex, depth
					if !_rules[ruleOR]() {
						goto l468
					}
					if !_rules[ruleExclusiveORExpression]() {
						goto l468
					}
					goto l467
				l468:
					position, tokenIndex, depth = position468, tokenIndex468, depth468
				}
				depth--
				add(ruleInclusiveORExpression, position466)
			}
			return true
		l465:
			position, tokenIndex, depth = position465, tokenIndex465, depth465
			return false
		},
		/* 68 LogicalANDExpression <- <(InclusiveORExpression (ANDAND InclusiveORExpression)*)> */
		func() bool {
			position469, tokenIndex469, depth469 := position, tokenIndex, depth
			{
				position470 := position
				depth++
				if !_rules[ruleInclusiveORExpression]() {
					goto l469
				}
			l471:
				{
					position472, tokenIndex472, depth472 := position, tokenIndex, depth
					if !_rules[ruleANDAND]() {
						goto l472
					}
					if !_rules[ruleInclusiveORExpression]() {
						goto l472
					}
					goto l471
				l472:
					position, tokenIndex, depth = position472, tokenIndex472, depth472
				}
				depth--
				add(ruleLogicalANDExpression, position470)
			}
			return true
		l469:
			position, tokenIndex, depth = position469, tokenIndex469, depth469
			return false
		},
		/* 69 LogicalORExpression <- <(LogicalANDExpression (OROR LogicalANDExpression)*)> */
		func() bool {
			position473, tokenIndex473, depth473 := position, tokenIndex, depth
			{
				position474 := position
				depth++
				if !_rules[ruleLogicalANDExpression]() {
					goto l473
				}
			l475:
				{
					position476, tokenIndex476, depth476 := position, tokenIndex, depth
					if !_rules[ruleOROR]() {
						goto l476
					}
					if !_rules[ruleLogicalANDExpression]() {
						goto l476
					}
					goto l475
				l476:
					position, tokenIndex, depth = position476, tokenIndex476, depth476
				}
				depth--
				add(ruleLogicalORExpression, position474)
			}
			return true
		l473:
			position, tokenIndex, depth = position473, tokenIndex473, depth473
			return false
		},
		/* 70 ConditionalExpression <- <(LogicalORExpression (QUERY Expression COLON LogicalORExpression)*)> */
		func() bool {
			position477, tokenIndex477, depth477 := position, tokenIndex, depth
			{
				position478 := position
				depth++
				if !_rules[ruleLogicalORExpression]() {
					goto l477
				}
			l479:
				{
					position480, tokenIndex480, depth480 := position, tokenIndex, depth
					if !_rules[ruleQUERY]() {
						goto l480
					}
					if !_rules[ruleExpression]() {
						goto l480
					}
					if !_rules[ruleCOLON]() {
						goto l480
					}
					if !_rules[ruleLogicalORExpression]() {
						goto l480
					}
					goto l479
				l480:
					position, tokenIndex, depth = position480, tokenIndex480, depth480
				}
				depth--
				add(ruleConditionalExpression, position478)
			}
			return true
		l477:
			position, tokenIndex, depth = position477, tokenIndex477, depth477
			return false
		},
		/* 71 AssignmentExpression <- <((UnaryExpression AssignmentOperator AssignmentExpression) / ConditionalExpression)> */
		func() bool {
			position481, tokenIndex481, depth481 := position, tokenIndex, depth
			{
				position482 := position
				depth++
				{
					position483, tokenIndex483, depth483 := position, tokenIndex, depth
					if !_rules[ruleUnaryExpression]() {
						goto l484
					}
					if !_rules[ruleAssignmentOperator]() {
						goto l484
					}
					if !_rules[ruleAssignmentExpression]() {
						goto l484
					}
					goto l483
				l484:
					position, tokenIndex, depth = position483, tokenIndex483, depth483
					if !_rules[ruleConditionalExpression]() {
						goto l481
					}
				}
			l483:
				depth--
				add(ruleAssignmentExpression, position482)
			}
			return true
		l481:
			position, tokenIndex, depth = position481, tokenIndex481, depth481
			return false
		},
		/* 72 AssignmentOperator <- <(EQU / STAREQU / DIVEQU / MODEQU / PLUSEQU / MINUSEQU / LEFTEQU / RIGHTEQU / ANDEQU / HATEQU / OREQU)> */
		func() bool {
			position485, tokenIndex485, depth485 := position, tokenIndex, depth
			{
				position486 := position
				depth++
				{
					position487, tokenIndex487, depth487 := position, tokenIndex, depth
					if !_rules[ruleEQU]() {
						goto l488
					}
					goto l487
				l488:
					position, tokenIndex, depth = position487, tokenIndex487, depth487
					if !_rules[ruleSTAREQU]() {
						goto l489
					}
					goto l487
				l489:
					position, tokenIndex, depth = position487, tokenIndex487, depth487
					if !_rules[ruleDIVEQU]() {
						goto l490
					}
					goto l487
				l490:
					position, tokenIndex, depth = position487, tokenIndex487, depth487
					if !_rules[ruleMODEQU]() {
						goto l491
					}
					goto l487
				l491:
					position, tokenIndex, depth = position487, tokenIndex487, depth487
					if !_rules[rulePLUSEQU]() {
						goto l492
					}
					goto l487
				l492:
					position, tokenIndex, depth = position487, tokenIndex487, depth487
					if !_rules[ruleMINUSEQU]() {
						goto l493
					}
					goto l487
				l493:
					position, tokenIndex, depth = position487, tokenIndex487, depth487
					if !_rules[ruleLEFTEQU]() {
						goto l494
					}
					goto l487
				l494:
					position, tokenIndex, depth = position487, tokenIndex487, depth487
					if !_rules[ruleRIGHTEQU]() {
						goto l495
					}
					goto l487
				l495:
					position, tokenIndex, depth = position487, tokenIndex487, depth487
					if !_rules[ruleANDEQU]() {
						goto l496
					}
					goto l487
				l496:
					position, tokenIndex, depth = position487, tokenIndex487, depth487
					if !_rules[ruleHATEQU]() {
						goto l497
					}
					goto l487
				l497:
					position, tokenIndex, depth = position487, tokenIndex487, depth487
					if !_rules[ruleOREQU]() {
						goto l485
					}
				}
			l487:
				depth--
				add(ruleAssignmentOperator, position486)
			}
			return true
		l485:
			position, tokenIndex, depth = position485, tokenIndex485, depth485
			return false
		},
		/* 73 Expression <- <(AssignmentExpression (COMMA AssignmentExpression)*)> */
		func() bool {
			position498, tokenIndex498, depth498 := position, tokenIndex, depth
			{
				position499 := position
				depth++
				if !_rules[ruleAssignmentExpression]() {
					goto l498
				}
			l500:
				{
					position501, tokenIndex501, depth501 := position, tokenIndex, depth
					if !_rules[ruleCOMMA]() {
						goto l501
					}
					if !_rules[ruleAssignmentExpression]() {
						goto l501
					}
					goto l500
				l501:
					position, tokenIndex, depth = position501, tokenIndex501, depth501
				}
				depth--
				add(ruleExpression, position499)
			}
			return true
		l498:
			position, tokenIndex, depth = position498, tokenIndex498, depth498
			return false
		},
		/* 74 ConstantExpression <- <ConditionalExpression> */
		func() bool {
			position502, tokenIndex502, depth502 := position, tokenIndex, depth
			{
				position503 := position
				depth++
				if !_rules[ruleConditionalExpression]() {
					goto l502
				}
				depth--
				add(ruleConstantExpression, position503)
			}
			return true
		l502:
			position, tokenIndex, depth = position502, tokenIndex502, depth502
			return false
		},
		/* 75 Spacing <- <(WhiteSpace / LongComment / LineComment)*> */
		func() bool {
			{
				position505 := position
				depth++
			l506:
				{
					position507, tokenIndex507, depth507 := position, tokenIndex, depth
					{
						position508, tokenIndex508, depth508 := position, tokenIndex, depth
						if !_rules[ruleWhiteSpace]() {
							goto l509
						}
						goto l508
					l509:
						position, tokenIndex, depth = position508, tokenIndex508, depth508
						if !_rules[ruleLongComment]() {
							goto l510
						}
						goto l508
					l510:
						position, tokenIndex, depth = position508, tokenIndex508, depth508
						if !_rules[ruleLineComment]() {
							goto l507
						}
					}
				l508:
					goto l506
				l507:
					position, tokenIndex, depth = position507, tokenIndex507, depth507
				}
				depth--
				add(ruleSpacing, position505)
			}
			return true
		},
		/* 76 WhiteSpace <- <(' ' / '\n' / '\r' / '\t')> */
		func() bool {
			position511, tokenIndex511, depth511 := position, tokenIndex, depth
			{
				position512 := position
				depth++
				{
					position513, tokenIndex513, depth513 := position, tokenIndex, depth
					if buffer[position] != rune(' ') {
						goto l514
					}
					position++
					goto l513
				l514:
					position, tokenIndex, depth = position513, tokenIndex513, depth513
					if buffer[position] != rune('\n') {
						goto l515
					}
					position++
					goto l513
				l515:
					position, tokenIndex, depth = position513, tokenIndex513, depth513
					if buffer[position] != rune('\r') {
						goto l516
					}
					position++
					goto l513
				l516:
					position, tokenIndex, depth = position513, tokenIndex513, depth513
					if buffer[position] != rune('\t') {
						goto l511
					}
					position++
				}
			l513:
				depth--
				add(ruleWhiteSpace, position512)
			}
			return true
		l511:
			position, tokenIndex, depth = position511, tokenIndex511, depth511
			return false
		},
		/* 77 LongComment <- <('/' '*' (!('*' '/') .)* ('*' '/'))> */
		func() bool {
			position517, tokenIndex517, depth517 := position, tokenIndex, depth
			{
				position518 := position
				depth++
				if buffer[position] != rune('/') {
					goto l517
				}
				position++
				if buffer[position] != rune('*') {
					goto l517
				}
				position++
			l519:
				{
					position520, tokenIndex520, depth520 := position, tokenIndex, depth
					{
						position521, tokenIndex521, depth521 := position, tokenIndex, depth
						if buffer[position] != rune('*') {
							goto l521
						}
						position++
						if buffer[position] != rune('/') {
							goto l521
						}
						position++
						goto l520
					l521:
						position, tokenIndex, depth = position521, tokenIndex521, depth521
					}
					if !matchDot() {
						goto l520
					}
					goto l519
				l520:
					position, tokenIndex, depth = position520, tokenIndex520, depth520
				}
				if buffer[position] != rune('*') {
					goto l517
				}
				position++
				if buffer[position] != rune('/') {
					goto l517
				}
				position++
				depth--
				add(ruleLongComment, position518)
			}
			return true
		l517:
			position, tokenIndex, depth = position517, tokenIndex517, depth517
			return false
		},
		/* 78 LineComment <- <('/' '/' (!'\n' .)*)> */
		func() bool {
			position522, tokenIndex522, depth522 := position, tokenIndex, depth
			{
				position523 := position
				depth++
				if buffer[position] != rune('/') {
					goto l522
				}
				position++
				if buffer[position] != rune('/') {
					goto l522
				}
				position++
			l524:
				{
					position525, tokenIndex525, depth525 := position, tokenIndex, depth
					{
						position526, tokenIndex526, depth526 := position, tokenIndex, depth
						if buffer[position] != rune('\n') {
							goto l526
						}
						position++
						goto l525
					l526:
						position, tokenIndex, depth = position526, tokenIndex526, depth526
					}
					if !matchDot() {
						goto l525
					}
					goto l524
				l525:
					position, tokenIndex, depth = position525, tokenIndex525, depth525
				}
				depth--
				add(ruleLineComment, position523)
			}
			return true
		l522:
			position, tokenIndex, depth = position522, tokenIndex522, depth522
			return false
		},
		/* 79 AUTO <- <('a' 'u' 't' 'o' !IdChar Spacing)> */
		func() bool {
			position527, tokenIndex527, depth527 := position, tokenIndex, depth
			{
				position528 := position
				depth++
				if buffer[position] != rune('a') {
					goto l527
				}
				position++
				if buffer[position] != rune('u') {
					goto l527
				}
				position++
				if buffer[position] != rune('t') {
					goto l527
				}
				position++
				if buffer[position] != rune('o') {
					goto l527
				}
				position++
				{
					position529, tokenIndex529, depth529 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l529
					}
					goto l527
				l529:
					position, tokenIndex, depth = position529, tokenIndex529, depth529
				}
				if !_rules[ruleSpacing]() {
					goto l527
				}
				depth--
				add(ruleAUTO, position528)
			}
			return true
		l527:
			position, tokenIndex, depth = position527, tokenIndex527, depth527
			return false
		},
		/* 80 BREAK <- <('b' 'r' 'e' 'a' 'k' !IdChar Spacing)> */
		func() bool {
			position530, tokenIndex530, depth530 := position, tokenIndex, depth
			{
				position531 := position
				depth++
				if buffer[position] != rune('b') {
					goto l530
				}
				position++
				if buffer[position] != rune('r') {
					goto l530
				}
				position++
				if buffer[position] != rune('e') {
					goto l530
				}
				position++
				if buffer[position] != rune('a') {
					goto l530
				}
				position++
				if buffer[position] != rune('k') {
					goto l530
				}
				position++
				{
					position532, tokenIndex532, depth532 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l532
					}
					goto l530
				l532:
					position, tokenIndex, depth = position532, tokenIndex532, depth532
				}
				if !_rules[ruleSpacing]() {
					goto l530
				}
				depth--
				add(ruleBREAK, position531)
			}
			return true
		l530:
			position, tokenIndex, depth = position530, tokenIndex530, depth530
			return false
		},
		/* 81 CASE <- <('c' 'a' 's' 'e' !IdChar Spacing)> */
		func() bool {
			position533, tokenIndex533, depth533 := position, tokenIndex, depth
			{
				position534 := position
				depth++
				if buffer[position] != rune('c') {
					goto l533
				}
				position++
				if buffer[position] != rune('a') {
					goto l533
				}
				position++
				if buffer[position] != rune('s') {
					goto l533
				}
				position++
				if buffer[position] != rune('e') {
					goto l533
				}
				position++
				{
					position535, tokenIndex535, depth535 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l535
					}
					goto l533
				l535:
					position, tokenIndex, depth = position535, tokenIndex535, depth535
				}
				if !_rules[ruleSpacing]() {
					goto l533
				}
				depth--
				add(ruleCASE, position534)
			}
			return true
		l533:
			position, tokenIndex, depth = position533, tokenIndex533, depth533
			return false
		},
		/* 82 CHAR <- <('c' 'h' 'a' 'r' !IdChar Spacing)> */
		func() bool {
			position536, tokenIndex536, depth536 := position, tokenIndex, depth
			{
				position537 := position
				depth++
				if buffer[position] != rune('c') {
					goto l536
				}
				position++
				if buffer[position] != rune('h') {
					goto l536
				}
				position++
				if buffer[position] != rune('a') {
					goto l536
				}
				position++
				if buffer[position] != rune('r') {
					goto l536
				}
				position++
				{
					position538, tokenIndex538, depth538 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l538
					}
					goto l536
				l538:
					position, tokenIndex, depth = position538, tokenIndex538, depth538
				}
				if !_rules[ruleSpacing]() {
					goto l536
				}
				depth--
				add(ruleCHAR, position537)
			}
			return true
		l536:
			position, tokenIndex, depth = position536, tokenIndex536, depth536
			return false
		},
		/* 83 CONST <- <('c' 'o' 'n' 's' 't' !IdChar Spacing)> */
		func() bool {
			position539, tokenIndex539, depth539 := position, tokenIndex, depth
			{
				position540 := position
				depth++
				if buffer[position] != rune('c') {
					goto l539
				}
				position++
				if buffer[position] != rune('o') {
					goto l539
				}
				position++
				if buffer[position] != rune('n') {
					goto l539
				}
				position++
				if buffer[position] != rune('s') {
					goto l539
				}
				position++
				if buffer[position] != rune('t') {
					goto l539
				}
				position++
				{
					position541, tokenIndex541, depth541 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l541
					}
					goto l539
				l541:
					position, tokenIndex, depth = position541, tokenIndex541, depth541
				}
				if !_rules[ruleSpacing]() {
					goto l539
				}
				depth--
				add(ruleCONST, position540)
			}
			return true
		l539:
			position, tokenIndex, depth = position539, tokenIndex539, depth539
			return false
		},
		/* 84 CONTINUE <- <('c' 'o' 'n' 't' 'i' 'n' 'u' 'e' !IdChar Spacing)> */
		func() bool {
			position542, tokenIndex542, depth542 := position, tokenIndex, depth
			{
				position543 := position
				depth++
				if buffer[position] != rune('c') {
					goto l542
				}
				position++
				if buffer[position] != rune('o') {
					goto l542
				}
				position++
				if buffer[position] != rune('n') {
					goto l542
				}
				position++
				if buffer[position] != rune('t') {
					goto l542
				}
				position++
				if buffer[position] != rune('i') {
					goto l542
				}
				position++
				if buffer[position] != rune('n') {
					goto l542
				}
				position++
				if buffer[position] != rune('u') {
					goto l542
				}
				position++
				if buffer[position] != rune('e') {
					goto l542
				}
				position++
				{
					position544, tokenIndex544, depth544 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l544
					}
					goto l542
				l544:
					position, tokenIndex, depth = position544, tokenIndex544, depth544
				}
				if !_rules[ruleSpacing]() {
					goto l542
				}
				depth--
				add(ruleCONTINUE, position543)
			}
			return true
		l542:
			position, tokenIndex, depth = position542, tokenIndex542, depth542
			return false
		},
		/* 85 DEFAULT <- <('d' 'e' 'f' 'a' 'u' 'l' 't' !IdChar Spacing)> */
		func() bool {
			position545, tokenIndex545, depth545 := position, tokenIndex, depth
			{
				position546 := position
				depth++
				if buffer[position] != rune('d') {
					goto l545
				}
				position++
				if buffer[position] != rune('e') {
					goto l545
				}
				position++
				if buffer[position] != rune('f') {
					goto l545
				}
				position++
				if buffer[position] != rune('a') {
					goto l545
				}
				position++
				if buffer[position] != rune('u') {
					goto l545
				}
				position++
				if buffer[position] != rune('l') {
					goto l545
				}
				position++
				if buffer[position] != rune('t') {
					goto l545
				}
				position++
				{
					position547, tokenIndex547, depth547 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l547
					}
					goto l545
				l547:
					position, tokenIndex, depth = position547, tokenIndex547, depth547
				}
				if !_rules[ruleSpacing]() {
					goto l545
				}
				depth--
				add(ruleDEFAULT, position546)
			}
			return true
		l545:
			position, tokenIndex, depth = position545, tokenIndex545, depth545
			return false
		},
		/* 86 DOUBLE <- <('d' 'o' 'u' 'b' 'l' 'e' !IdChar Spacing)> */
		func() bool {
			position548, tokenIndex548, depth548 := position, tokenIndex, depth
			{
				position549 := position
				depth++
				if buffer[position] != rune('d') {
					goto l548
				}
				position++
				if buffer[position] != rune('o') {
					goto l548
				}
				position++
				if buffer[position] != rune('u') {
					goto l548
				}
				position++
				if buffer[position] != rune('b') {
					goto l548
				}
				position++
				if buffer[position] != rune('l') {
					goto l548
				}
				position++
				if buffer[position] != rune('e') {
					goto l548
				}
				position++
				{
					position550, tokenIndex550, depth550 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l550
					}
					goto l548
				l550:
					position, tokenIndex, depth = position550, tokenIndex550, depth550
				}
				if !_rules[ruleSpacing]() {
					goto l548
				}
				depth--
				add(ruleDOUBLE, position549)
			}
			return true
		l548:
			position, tokenIndex, depth = position548, tokenIndex548, depth548
			return false
		},
		/* 87 DO <- <('d' 'o' !IdChar Spacing)> */
		func() bool {
			position551, tokenIndex551, depth551 := position, tokenIndex, depth
			{
				position552 := position
				depth++
				if buffer[position] != rune('d') {
					goto l551
				}
				position++
				if buffer[position] != rune('o') {
					goto l551
				}
				position++
				{
					position553, tokenIndex553, depth553 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l553
					}
					goto l551
				l553:
					position, tokenIndex, depth = position553, tokenIndex553, depth553
				}
				if !_rules[ruleSpacing]() {
					goto l551
				}
				depth--
				add(ruleDO, position552)
			}
			return true
		l551:
			position, tokenIndex, depth = position551, tokenIndex551, depth551
			return false
		},
		/* 88 ELSE <- <('e' 'l' 's' 'e' !IdChar Spacing)> */
		func() bool {
			position554, tokenIndex554, depth554 := position, tokenIndex, depth
			{
				position555 := position
				depth++
				if buffer[position] != rune('e') {
					goto l554
				}
				position++
				if buffer[position] != rune('l') {
					goto l554
				}
				position++
				if buffer[position] != rune('s') {
					goto l554
				}
				position++
				if buffer[position] != rune('e') {
					goto l554
				}
				position++
				{
					position556, tokenIndex556, depth556 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l556
					}
					goto l554
				l556:
					position, tokenIndex, depth = position556, tokenIndex556, depth556
				}
				if !_rules[ruleSpacing]() {
					goto l554
				}
				depth--
				add(ruleELSE, position555)
			}
			return true
		l554:
			position, tokenIndex, depth = position554, tokenIndex554, depth554
			return false
		},
		/* 89 ENUM <- <('e' 'n' 'u' 'm' !IdChar Spacing)> */
		func() bool {
			position557, tokenIndex557, depth557 := position, tokenIndex, depth
			{
				position558 := position
				depth++
				if buffer[position] != rune('e') {
					goto l557
				}
				position++
				if buffer[position] != rune('n') {
					goto l557
				}
				position++
				if buffer[position] != rune('u') {
					goto l557
				}
				position++
				if buffer[position] != rune('m') {
					goto l557
				}
				position++
				{
					position559, tokenIndex559, depth559 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l559
					}
					goto l557
				l559:
					position, tokenIndex, depth = position559, tokenIndex559, depth559
				}
				if !_rules[ruleSpacing]() {
					goto l557
				}
				depth--
				add(ruleENUM, position558)
			}
			return true
		l557:
			position, tokenIndex, depth = position557, tokenIndex557, depth557
			return false
		},
		/* 90 EXTERN <- <('e' 'x' 't' 'e' 'r' 'n' !IdChar Spacing)> */
		func() bool {
			position560, tokenIndex560, depth560 := position, tokenIndex, depth
			{
				position561 := position
				depth++
				if buffer[position] != rune('e') {
					goto l560
				}
				position++
				if buffer[position] != rune('x') {
					goto l560
				}
				position++
				if buffer[position] != rune('t') {
					goto l560
				}
				position++
				if buffer[position] != rune('e') {
					goto l560
				}
				position++
				if buffer[position] != rune('r') {
					goto l560
				}
				position++
				if buffer[position] != rune('n') {
					goto l560
				}
				position++
				{
					position562, tokenIndex562, depth562 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l562
					}
					goto l560
				l562:
					position, tokenIndex, depth = position562, tokenIndex562, depth562
				}
				if !_rules[ruleSpacing]() {
					goto l560
				}
				depth--
				add(ruleEXTERN, position561)
			}
			return true
		l560:
			position, tokenIndex, depth = position560, tokenIndex560, depth560
			return false
		},
		/* 91 FLOAT <- <('f' 'l' 'o' 'a' 't' !IdChar Spacing)> */
		func() bool {
			position563, tokenIndex563, depth563 := position, tokenIndex, depth
			{
				position564 := position
				depth++
				if buffer[position] != rune('f') {
					goto l563
				}
				position++
				if buffer[position] != rune('l') {
					goto l563
				}
				position++
				if buffer[position] != rune('o') {
					goto l563
				}
				position++
				if buffer[position] != rune('a') {
					goto l563
				}
				position++
				if buffer[position] != rune('t') {
					goto l563
				}
				position++
				{
					position565, tokenIndex565, depth565 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l565
					}
					goto l563
				l565:
					position, tokenIndex, depth = position565, tokenIndex565, depth565
				}
				if !_rules[ruleSpacing]() {
					goto l563
				}
				depth--
				add(ruleFLOAT, position564)
			}
			return true
		l563:
			position, tokenIndex, depth = position563, tokenIndex563, depth563
			return false
		},
		/* 92 FOR <- <('f' 'o' 'r' !IdChar Spacing)> */
		func() bool {
			position566, tokenIndex566, depth566 := position, tokenIndex, depth
			{
				position567 := position
				depth++
				if buffer[position] != rune('f') {
					goto l566
				}
				position++
				if buffer[position] != rune('o') {
					goto l566
				}
				position++
				if buffer[position] != rune('r') {
					goto l566
				}
				position++
				{
					position568, tokenIndex568, depth568 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l568
					}
					goto l566
				l568:
					position, tokenIndex, depth = position568, tokenIndex568, depth568
				}
				if !_rules[ruleSpacing]() {
					goto l566
				}
				depth--
				add(ruleFOR, position567)
			}
			return true
		l566:
			position, tokenIndex, depth = position566, tokenIndex566, depth566
			return false
		},
		/* 93 GOTO <- <('g' 'o' 't' 'o' !IdChar Spacing)> */
		func() bool {
			position569, tokenIndex569, depth569 := position, tokenIndex, depth
			{
				position570 := position
				depth++
				if buffer[position] != rune('g') {
					goto l569
				}
				position++
				if buffer[position] != rune('o') {
					goto l569
				}
				position++
				if buffer[position] != rune('t') {
					goto l569
				}
				position++
				if buffer[position] != rune('o') {
					goto l569
				}
				position++
				{
					position571, tokenIndex571, depth571 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l571
					}
					goto l569
				l571:
					position, tokenIndex, depth = position571, tokenIndex571, depth571
				}
				if !_rules[ruleSpacing]() {
					goto l569
				}
				depth--
				add(ruleGOTO, position570)
			}
			return true
		l569:
			position, tokenIndex, depth = position569, tokenIndex569, depth569
			return false
		},
		/* 94 IF <- <('i' 'f' !IdChar Spacing)> */
		func() bool {
			position572, tokenIndex572, depth572 := position, tokenIndex, depth
			{
				position573 := position
				depth++
				if buffer[position] != rune('i') {
					goto l572
				}
				position++
				if buffer[position] != rune('f') {
					goto l572
				}
				position++
				{
					position574, tokenIndex574, depth574 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l574
					}
					goto l572
				l574:
					position, tokenIndex, depth = position574, tokenIndex574, depth574
				}
				if !_rules[ruleSpacing]() {
					goto l572
				}
				depth--
				add(ruleIF, position573)
			}
			return true
		l572:
			position, tokenIndex, depth = position572, tokenIndex572, depth572
			return false
		},
		/* 95 INT <- <('i' 'n' 't' !IdChar Spacing)> */
		func() bool {
			position575, tokenIndex575, depth575 := position, tokenIndex, depth
			{
				position576 := position
				depth++
				if buffer[position] != rune('i') {
					goto l575
				}
				position++
				if buffer[position] != rune('n') {
					goto l575
				}
				position++
				if buffer[position] != rune('t') {
					goto l575
				}
				position++
				{
					position577, tokenIndex577, depth577 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l577
					}
					goto l575
				l577:
					position, tokenIndex, depth = position577, tokenIndex577, depth577
				}
				if !_rules[ruleSpacing]() {
					goto l575
				}
				depth--
				add(ruleINT, position576)
			}
			return true
		l575:
			position, tokenIndex, depth = position575, tokenIndex575, depth575
			return false
		},
		/* 96 INLINE <- <('i' 'n' 'l' 'i' 'n' 'e' !IdChar Spacing)> */
		func() bool {
			position578, tokenIndex578, depth578 := position, tokenIndex, depth
			{
				position579 := position
				depth++
				if buffer[position] != rune('i') {
					goto l578
				}
				position++
				if buffer[position] != rune('n') {
					goto l578
				}
				position++
				if buffer[position] != rune('l') {
					goto l578
				}
				position++
				if buffer[position] != rune('i') {
					goto l578
				}
				position++
				if buffer[position] != rune('n') {
					goto l578
				}
				position++
				if buffer[position] != rune('e') {
					goto l578
				}
				position++
				{
					position580, tokenIndex580, depth580 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l580
					}
					goto l578
				l580:
					position, tokenIndex, depth = position580, tokenIndex580, depth580
				}
				if !_rules[ruleSpacing]() {
					goto l578
				}
				depth--
				add(ruleINLINE, position579)
			}
			return true
		l578:
			position, tokenIndex, depth = position578, tokenIndex578, depth578
			return false
		},
		/* 97 LONG <- <('l' 'o' 'n' 'g' !IdChar Spacing)> */
		func() bool {
			position581, tokenIndex581, depth581 := position, tokenIndex, depth
			{
				position582 := position
				depth++
				if buffer[position] != rune('l') {
					goto l581
				}
				position++
				if buffer[position] != rune('o') {
					goto l581
				}
				position++
				if buffer[position] != rune('n') {
					goto l581
				}
				position++
				if buffer[position] != rune('g') {
					goto l581
				}
				position++
				{
					position583, tokenIndex583, depth583 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l583
					}
					goto l581
				l583:
					position, tokenIndex, depth = position583, tokenIndex583, depth583
				}
				if !_rules[ruleSpacing]() {
					goto l581
				}
				depth--
				add(ruleLONG, position582)
			}
			return true
		l581:
			position, tokenIndex, depth = position581, tokenIndex581, depth581
			return false
		},
		/* 98 REGISTER <- <('r' 'e' 'g' 'i' 's' 't' 'e' 'r' !IdChar Spacing)> */
		func() bool {
			position584, tokenIndex584, depth584 := position, tokenIndex, depth
			{
				position585 := position
				depth++
				if buffer[position] != rune('r') {
					goto l584
				}
				position++
				if buffer[position] != rune('e') {
					goto l584
				}
				position++
				if buffer[position] != rune('g') {
					goto l584
				}
				position++
				if buffer[position] != rune('i') {
					goto l584
				}
				position++
				if buffer[position] != rune('s') {
					goto l584
				}
				position++
				if buffer[position] != rune('t') {
					goto l584
				}
				position++
				if buffer[position] != rune('e') {
					goto l584
				}
				position++
				if buffer[position] != rune('r') {
					goto l584
				}
				position++
				{
					position586, tokenIndex586, depth586 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l586
					}
					goto l584
				l586:
					position, tokenIndex, depth = position586, tokenIndex586, depth586
				}
				if !_rules[ruleSpacing]() {
					goto l584
				}
				depth--
				add(ruleREGISTER, position585)
			}
			return true
		l584:
			position, tokenIndex, depth = position584, tokenIndex584, depth584
			return false
		},
		/* 99 RESTRICT <- <('r' 'e' 's' 't' 'r' 'i' 'c' 't' !IdChar Spacing)> */
		func() bool {
			position587, tokenIndex587, depth587 := position, tokenIndex, depth
			{
				position588 := position
				depth++
				if buffer[position] != rune('r') {
					goto l587
				}
				position++
				if buffer[position] != rune('e') {
					goto l587
				}
				position++
				if buffer[position] != rune('s') {
					goto l587
				}
				position++
				if buffer[position] != rune('t') {
					goto l587
				}
				position++
				if buffer[position] != rune('r') {
					goto l587
				}
				position++
				if buffer[position] != rune('i') {
					goto l587
				}
				position++
				if buffer[position] != rune('c') {
					goto l587
				}
				position++
				if buffer[position] != rune('t') {
					goto l587
				}
				position++
				{
					position589, tokenIndex589, depth589 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l589
					}
					goto l587
				l589:
					position, tokenIndex, depth = position589, tokenIndex589, depth589
				}
				if !_rules[ruleSpacing]() {
					goto l587
				}
				depth--
				add(ruleRESTRICT, position588)
			}
			return true
		l587:
			position, tokenIndex, depth = position587, tokenIndex587, depth587
			return false
		},
		/* 100 RETURN <- <('r' 'e' 't' 'u' 'r' 'n' !IdChar Spacing)> */
		func() bool {
			position590, tokenIndex590, depth590 := position, tokenIndex, depth
			{
				position591 := position
				depth++
				if buffer[position] != rune('r') {
					goto l590
				}
				position++
				if buffer[position] != rune('e') {
					goto l590
				}
				position++
				if buffer[position] != rune('t') {
					goto l590
				}
				position++
				if buffer[position] != rune('u') {
					goto l590
				}
				position++
				if buffer[position] != rune('r') {
					goto l590
				}
				position++
				if buffer[position] != rune('n') {
					goto l590
				}
				position++
				{
					position592, tokenIndex592, depth592 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l592
					}
					goto l590
				l592:
					position, tokenIndex, depth = position592, tokenIndex592, depth592
				}
				if !_rules[ruleSpacing]() {
					goto l590
				}
				depth--
				add(ruleRETURN, position591)
			}
			return true
		l590:
			position, tokenIndex, depth = position590, tokenIndex590, depth590
			return false
		},
		/* 101 SHORT <- <('s' 'h' 'o' 'r' 't' !IdChar Spacing)> */
		func() bool {
			position593, tokenIndex593, depth593 := position, tokenIndex, depth
			{
				position594 := position
				depth++
				if buffer[position] != rune('s') {
					goto l593
				}
				position++
				if buffer[position] != rune('h') {
					goto l593
				}
				position++
				if buffer[position] != rune('o') {
					goto l593
				}
				position++
				if buffer[position] != rune('r') {
					goto l593
				}
				position++
				if buffer[position] != rune('t') {
					goto l593
				}
				position++
				{
					position595, tokenIndex595, depth595 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l595
					}
					goto l593
				l595:
					position, tokenIndex, depth = position595, tokenIndex595, depth595
				}
				if !_rules[ruleSpacing]() {
					goto l593
				}
				depth--
				add(ruleSHORT, position594)
			}
			return true
		l593:
			position, tokenIndex, depth = position593, tokenIndex593, depth593
			return false
		},
		/* 102 SIGNED <- <('s' 'i' 'g' 'n' 'e' 'd' !IdChar Spacing)> */
		func() bool {
			position596, tokenIndex596, depth596 := position, tokenIndex, depth
			{
				position597 := position
				depth++
				if buffer[position] != rune('s') {
					goto l596
				}
				position++
				if buffer[position] != rune('i') {
					goto l596
				}
				position++
				if buffer[position] != rune('g') {
					goto l596
				}
				position++
				if buffer[position] != rune('n') {
					goto l596
				}
				position++
				if buffer[position] != rune('e') {
					goto l596
				}
				position++
				if buffer[position] != rune('d') {
					goto l596
				}
				position++
				{
					position598, tokenIndex598, depth598 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l598
					}
					goto l596
				l598:
					position, tokenIndex, depth = position598, tokenIndex598, depth598
				}
				if !_rules[ruleSpacing]() {
					goto l596
				}
				depth--
				add(ruleSIGNED, position597)
			}
			return true
		l596:
			position, tokenIndex, depth = position596, tokenIndex596, depth596
			return false
		},
		/* 103 SIZEOF <- <('s' 'i' 'z' 'e' 'o' 'f' !IdChar Spacing)> */
		func() bool {
			position599, tokenIndex599, depth599 := position, tokenIndex, depth
			{
				position600 := position
				depth++
				if buffer[position] != rune('s') {
					goto l599
				}
				position++
				if buffer[position] != rune('i') {
					goto l599
				}
				position++
				if buffer[position] != rune('z') {
					goto l599
				}
				position++
				if buffer[position] != rune('e') {
					goto l599
				}
				position++
				if buffer[position] != rune('o') {
					goto l599
				}
				position++
				if buffer[position] != rune('f') {
					goto l599
				}
				position++
				{
					position601, tokenIndex601, depth601 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l601
					}
					goto l599
				l601:
					position, tokenIndex, depth = position601, tokenIndex601, depth601
				}
				if !_rules[ruleSpacing]() {
					goto l599
				}
				depth--
				add(ruleSIZEOF, position600)
			}
			return true
		l599:
			position, tokenIndex, depth = position599, tokenIndex599, depth599
			return false
		},
		/* 104 STATIC <- <('s' 't' 'a' 't' 'i' 'c' !IdChar Spacing)> */
		func() bool {
			position602, tokenIndex602, depth602 := position, tokenIndex, depth
			{
				position603 := position
				depth++
				if buffer[position] != rune('s') {
					goto l602
				}
				position++
				if buffer[position] != rune('t') {
					goto l602
				}
				position++
				if buffer[position] != rune('a') {
					goto l602
				}
				position++
				if buffer[position] != rune('t') {
					goto l602
				}
				position++
				if buffer[position] != rune('i') {
					goto l602
				}
				position++
				if buffer[position] != rune('c') {
					goto l602
				}
				position++
				{
					position604, tokenIndex604, depth604 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l604
					}
					goto l602
				l604:
					position, tokenIndex, depth = position604, tokenIndex604, depth604
				}
				if !_rules[ruleSpacing]() {
					goto l602
				}
				depth--
				add(ruleSTATIC, position603)
			}
			return true
		l602:
			position, tokenIndex, depth = position602, tokenIndex602, depth602
			return false
		},
		/* 105 STRUCT <- <('s' 't' 'r' 'u' 'c' 't' !IdChar Spacing)> */
		func() bool {
			position605, tokenIndex605, depth605 := position, tokenIndex, depth
			{
				position606 := position
				depth++
				if buffer[position] != rune('s') {
					goto l605
				}
				position++
				if buffer[position] != rune('t') {
					goto l605
				}
				position++
				if buffer[position] != rune('r') {
					goto l605
				}
				position++
				if buffer[position] != rune('u') {
					goto l605
				}
				position++
				if buffer[position] != rune('c') {
					goto l605
				}
				position++
				if buffer[position] != rune('t') {
					goto l605
				}
				position++
				{
					position607, tokenIndex607, depth607 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l607
					}
					goto l605
				l607:
					position, tokenIndex, depth = position607, tokenIndex607, depth607
				}
				if !_rules[ruleSpacing]() {
					goto l605
				}
				depth--
				add(ruleSTRUCT, position606)
			}
			return true
		l605:
			position, tokenIndex, depth = position605, tokenIndex605, depth605
			return false
		},
		/* 106 SWITCH <- <('s' 'w' 'i' 't' 'c' 'h' !IdChar Spacing)> */
		func() bool {
			position608, tokenIndex608, depth608 := position, tokenIndex, depth
			{
				position609 := position
				depth++
				if buffer[position] != rune('s') {
					goto l608
				}
				position++
				if buffer[position] != rune('w') {
					goto l608
				}
				position++
				if buffer[position] != rune('i') {
					goto l608
				}
				position++
				if buffer[position] != rune('t') {
					goto l608
				}
				position++
				if buffer[position] != rune('c') {
					goto l608
				}
				position++
				if buffer[position] != rune('h') {
					goto l608
				}
				position++
				{
					position610, tokenIndex610, depth610 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l610
					}
					goto l608
				l610:
					position, tokenIndex, depth = position610, tokenIndex610, depth610
				}
				if !_rules[ruleSpacing]() {
					goto l608
				}
				depth--
				add(ruleSWITCH, position609)
			}
			return true
		l608:
			position, tokenIndex, depth = position608, tokenIndex608, depth608
			return false
		},
		/* 107 TYPEDEF <- <('t' 'y' 'p' 'e' 'd' 'e' 'f' !IdChar Spacing)> */
		func() bool {
			position611, tokenIndex611, depth611 := position, tokenIndex, depth
			{
				position612 := position
				depth++
				if buffer[position] != rune('t') {
					goto l611
				}
				position++
				if buffer[position] != rune('y') {
					goto l611
				}
				position++
				if buffer[position] != rune('p') {
					goto l611
				}
				position++
				if buffer[position] != rune('e') {
					goto l611
				}
				position++
				if buffer[position] != rune('d') {
					goto l611
				}
				position++
				if buffer[position] != rune('e') {
					goto l611
				}
				position++
				if buffer[position] != rune('f') {
					goto l611
				}
				position++
				{
					position613, tokenIndex613, depth613 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l613
					}
					goto l611
				l613:
					position, tokenIndex, depth = position613, tokenIndex613, depth613
				}
				if !_rules[ruleSpacing]() {
					goto l611
				}
				depth--
				add(ruleTYPEDEF, position612)
			}
			return true
		l611:
			position, tokenIndex, depth = position611, tokenIndex611, depth611
			return false
		},
		/* 108 UNION <- <('u' 'n' 'i' 'o' 'n' !IdChar Spacing)> */
		func() bool {
			position614, tokenIndex614, depth614 := position, tokenIndex, depth
			{
				position615 := position
				depth++
				if buffer[position] != rune('u') {
					goto l614
				}
				position++
				if buffer[position] != rune('n') {
					goto l614
				}
				position++
				if buffer[position] != rune('i') {
					goto l614
				}
				position++
				if buffer[position] != rune('o') {
					goto l614
				}
				position++
				if buffer[position] != rune('n') {
					goto l614
				}
				position++
				{
					position616, tokenIndex616, depth616 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l616
					}
					goto l614
				l616:
					position, tokenIndex, depth = position616, tokenIndex616, depth616
				}
				if !_rules[ruleSpacing]() {
					goto l614
				}
				depth--
				add(ruleUNION, position615)
			}
			return true
		l614:
			position, tokenIndex, depth = position614, tokenIndex614, depth614
			return false
		},
		/* 109 UNSIGNED <- <('u' 'n' 's' 'i' 'g' 'n' 'e' 'd' !IdChar Spacing)> */
		func() bool {
			position617, tokenIndex617, depth617 := position, tokenIndex, depth
			{
				position618 := position
				depth++
				if buffer[position] != rune('u') {
					goto l617
				}
				position++
				if buffer[position] != rune('n') {
					goto l617
				}
				position++
				if buffer[position] != rune('s') {
					goto l617
				}
				position++
				if buffer[position] != rune('i') {
					goto l617
				}
				position++
				if buffer[position] != rune('g') {
					goto l617
				}
				position++
				if buffer[position] != rune('n') {
					goto l617
				}
				position++
				if buffer[position] != rune('e') {
					goto l617
				}
				position++
				if buffer[position] != rune('d') {
					goto l617
				}
				position++
				{
					position619, tokenIndex619, depth619 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l619
					}
					goto l617
				l619:
					position, tokenIndex, depth = position619, tokenIndex619, depth619
				}
				if !_rules[ruleSpacing]() {
					goto l617
				}
				depth--
				add(ruleUNSIGNED, position618)
			}
			return true
		l617:
			position, tokenIndex, depth = position617, tokenIndex617, depth617
			return false
		},
		/* 110 VOID <- <('v' 'o' 'i' 'd' !IdChar Spacing)> */
		func() bool {
			position620, tokenIndex620, depth620 := position, tokenIndex, depth
			{
				position621 := position
				depth++
				if buffer[position] != rune('v') {
					goto l620
				}
				position++
				if buffer[position] != rune('o') {
					goto l620
				}
				position++
				if buffer[position] != rune('i') {
					goto l620
				}
				position++
				if buffer[position] != rune('d') {
					goto l620
				}
				position++
				{
					position622, tokenIndex622, depth622 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l622
					}
					goto l620
				l622:
					position, tokenIndex, depth = position622, tokenIndex622, depth622
				}
				if !_rules[ruleSpacing]() {
					goto l620
				}
				depth--
				add(ruleVOID, position621)
			}
			return true
		l620:
			position, tokenIndex, depth = position620, tokenIndex620, depth620
			return false
		},
		/* 111 VOLATILE <- <('v' 'o' 'l' 'a' 't' 'i' 'l' 'e' !IdChar Spacing)> */
		func() bool {
			position623, tokenIndex623, depth623 := position, tokenIndex, depth
			{
				position624 := position
				depth++
				if buffer[position] != rune('v') {
					goto l623
				}
				position++
				if buffer[position] != rune('o') {
					goto l623
				}
				position++
				if buffer[position] != rune('l') {
					goto l623
				}
				position++
				if buffer[position] != rune('a') {
					goto l623
				}
				position++
				if buffer[position] != rune('t') {
					goto l623
				}
				position++
				if buffer[position] != rune('i') {
					goto l623
				}
				position++
				if buffer[position] != rune('l') {
					goto l623
				}
				position++
				if buffer[position] != rune('e') {
					goto l623
				}
				position++
				{
					position625, tokenIndex625, depth625 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l625
					}
					goto l623
				l625:
					position, tokenIndex, depth = position625, tokenIndex625, depth625
				}
				if !_rules[ruleSpacing]() {
					goto l623
				}
				depth--
				add(ruleVOLATILE, position624)
			}
			return true
		l623:
			position, tokenIndex, depth = position623, tokenIndex623, depth623
			return false
		},
		/* 112 WHILE <- <('w' 'h' 'i' 'l' 'e' !IdChar Spacing)> */
		func() bool {
			position626, tokenIndex626, depth626 := position, tokenIndex, depth
			{
				position627 := position
				depth++
				if buffer[position] != rune('w') {
					goto l626
				}
				position++
				if buffer[position] != rune('h') {
					goto l626
				}
				position++
				if buffer[position] != rune('i') {
					goto l626
				}
				position++
				if buffer[position] != rune('l') {
					goto l626
				}
				position++
				if buffer[position] != rune('e') {
					goto l626
				}
				position++
				{
					position628, tokenIndex628, depth628 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l628
					}
					goto l626
				l628:
					position, tokenIndex, depth = position628, tokenIndex628, depth628
				}
				if !_rules[ruleSpacing]() {
					goto l626
				}
				depth--
				add(ruleWHILE, position627)
			}
			return true
		l626:
			position, tokenIndex, depth = position626, tokenIndex626, depth626
			return false
		},
		/* 113 BOOL <- <('_' 'B' 'o' 'o' 'l' !IdChar Spacing)> */
		func() bool {
			position629, tokenIndex629, depth629 := position, tokenIndex, depth
			{
				position630 := position
				depth++
				if buffer[position] != rune('_') {
					goto l629
				}
				position++
				if buffer[position] != rune('B') {
					goto l629
				}
				position++
				if buffer[position] != rune('o') {
					goto l629
				}
				position++
				if buffer[position] != rune('o') {
					goto l629
				}
				position++
				if buffer[position] != rune('l') {
					goto l629
				}
				position++
				{
					position631, tokenIndex631, depth631 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l631
					}
					goto l629
				l631:
					position, tokenIndex, depth = position631, tokenIndex631, depth631
				}
				if !_rules[ruleSpacing]() {
					goto l629
				}
				depth--
				add(ruleBOOL, position630)
			}
			return true
		l629:
			position, tokenIndex, depth = position629, tokenIndex629, depth629
			return false
		},
		/* 114 COMPLEX <- <('_' 'C' 'o' 'm' 'p' 'l' 'e' 'x' !IdChar Spacing)> */
		func() bool {
			position632, tokenIndex632, depth632 := position, tokenIndex, depth
			{
				position633 := position
				depth++
				if buffer[position] != rune('_') {
					goto l632
				}
				position++
				if buffer[position] != rune('C') {
					goto l632
				}
				position++
				if buffer[position] != rune('o') {
					goto l632
				}
				position++
				if buffer[position] != rune('m') {
					goto l632
				}
				position++
				if buffer[position] != rune('p') {
					goto l632
				}
				position++
				if buffer[position] != rune('l') {
					goto l632
				}
				position++
				if buffer[position] != rune('e') {
					goto l632
				}
				position++
				if buffer[position] != rune('x') {
					goto l632
				}
				position++
				{
					position634, tokenIndex634, depth634 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l634
					}
					goto l632
				l634:
					position, tokenIndex, depth = position634, tokenIndex634, depth634
				}
				if !_rules[ruleSpacing]() {
					goto l632
				}
				depth--
				add(ruleCOMPLEX, position633)
			}
			return true
		l632:
			position, tokenIndex, depth = position632, tokenIndex632, depth632
			return false
		},
		/* 115 STDCALL <- <('_' 's' 't' 'd' 'c' 'a' 'l' 'l' !IdChar Spacing)> */
		func() bool {
			position635, tokenIndex635, depth635 := position, tokenIndex, depth
			{
				position636 := position
				depth++
				if buffer[position] != rune('_') {
					goto l635
				}
				position++
				if buffer[position] != rune('s') {
					goto l635
				}
				position++
				if buffer[position] != rune('t') {
					goto l635
				}
				position++
				if buffer[position] != rune('d') {
					goto l635
				}
				position++
				if buffer[position] != rune('c') {
					goto l635
				}
				position++
				if buffer[position] != rune('a') {
					goto l635
				}
				position++
				if buffer[position] != rune('l') {
					goto l635
				}
				position++
				if buffer[position] != rune('l') {
					goto l635
				}
				position++
				{
					position637, tokenIndex637, depth637 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l637
					}
					goto l635
				l637:
					position, tokenIndex, depth = position637, tokenIndex637, depth637
				}
				if !_rules[ruleSpacing]() {
					goto l635
				}
				depth--
				add(ruleSTDCALL, position636)
			}
			return true
		l635:
			position, tokenIndex, depth = position635, tokenIndex635, depth635
			return false
		},
		/* 116 DECLSPEC <- <('_' '_' 'd' 'e' 'c' 'l' 's' 'p' 'e' 'c' !IdChar Spacing)> */
		func() bool {
			position638, tokenIndex638, depth638 := position, tokenIndex, depth
			{
				position639 := position
				depth++
				if buffer[position] != rune('_') {
					goto l638
				}
				position++
				if buffer[position] != rune('_') {
					goto l638
				}
				position++
				if buffer[position] != rune('d') {
					goto l638
				}
				position++
				if buffer[position] != rune('e') {
					goto l638
				}
				position++
				if buffer[position] != rune('c') {
					goto l638
				}
				position++
				if buffer[position] != rune('l') {
					goto l638
				}
				position++
				if buffer[position] != rune('s') {
					goto l638
				}
				position++
				if buffer[position] != rune('p') {
					goto l638
				}
				position++
				if buffer[position] != rune('e') {
					goto l638
				}
				position++
				if buffer[position] != rune('c') {
					goto l638
				}
				position++
				{
					position640, tokenIndex640, depth640 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l640
					}
					goto l638
				l640:
					position, tokenIndex, depth = position640, tokenIndex640, depth640
				}
				if !_rules[ruleSpacing]() {
					goto l638
				}
				depth--
				add(ruleDECLSPEC, position639)
			}
			return true
		l638:
			position, tokenIndex, depth = position638, tokenIndex638, depth638
			return false
		},
		/* 117 ATTRIBUTE <- <('_' '_' 'a' 't' 't' 'r' 'i' 'b' 'u' 't' 'e' '_' '_' !IdChar Spacing)> */
		func() bool {
			position641, tokenIndex641, depth641 := position, tokenIndex, depth
			{
				position642 := position
				depth++
				if buffer[position] != rune('_') {
					goto l641
				}
				position++
				if buffer[position] != rune('_') {
					goto l641
				}
				position++
				if buffer[position] != rune('a') {
					goto l641
				}
				position++
				if buffer[position] != rune('t') {
					goto l641
				}
				position++
				if buffer[position] != rune('t') {
					goto l641
				}
				position++
				if buffer[position] != rune('r') {
					goto l641
				}
				position++
				if buffer[position] != rune('i') {
					goto l641
				}
				position++
				if buffer[position] != rune('b') {
					goto l641
				}
				position++
				if buffer[position] != rune('u') {
					goto l641
				}
				position++
				if buffer[position] != rune('t') {
					goto l641
				}
				position++
				if buffer[position] != rune('e') {
					goto l641
				}
				position++
				if buffer[position] != rune('_') {
					goto l641
				}
				position++
				if buffer[position] != rune('_') {
					goto l641
				}
				position++
				{
					position643, tokenIndex643, depth643 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l643
					}
					goto l641
				l643:
					position, tokenIndex, depth = position643, tokenIndex643, depth643
				}
				if !_rules[ruleSpacing]() {
					goto l641
				}
				depth--
				add(ruleATTRIBUTE, position642)
			}
			return true
		l641:
			position, tokenIndex, depth = position641, tokenIndex641, depth641
			return false
		},
		/* 118 Keyword <- <((('a' 'u' 't' 'o') / ('b' 'r' 'e' 'a' 'k') / ('c' 'a' 's' 'e') / ('c' 'h' 'a' 'r') / ('c' 'o' 'n' 's' 't') / ('c' 'o' 'n' 't' 'i' 'n' 'u' 'e') / ('d' 'e' 'f' 'a' 'u' 'l' 't') / ('d' 'o' 'u' 'b' 'l' 'e') / ('d' 'o') / ('e' 'l' 's' 'e') / ('e' 'n' 'u' 'm') / ('e' 'x' 't' 'e' 'r' 'n') / ('f' 'l' 'o' 'a' 't') / ('f' 'o' 'r') / ('g' 'o' 't' 'o') / ('i' 'f') / ('i' 'n' 't') / ('i' 'n' 'l' 'i' 'n' 'e') / ('l' 'o' 'n' 'g') / ('r' 'e' 'g' 'i' 's' 't' 'e' 'r') / ('r' 'e' 's' 't' 'r' 'i' 'c' 't') / ('r' 'e' 't' 'u' 'r' 'n') / ('s' 'h' 'o' 'r' 't') / ('s' 'i' 'g' 'n' 'e' 'd') / ('s' 'i' 'z' 'e' 'o' 'f') / ('s' 't' 'a' 't' 'i' 'c') / ('s' 't' 'r' 'u' 'c' 't') / ('s' 'w' 'i' 't' 'c' 'h') / ('t' 'y' 'p' 'e' 'd' 'e' 'f') / ('u' 'n' 'i' 'o' 'n') / ('u' 'n' 's' 'i' 'g' 'n' 'e' 'd') / ('v' 'o' 'i' 'd') / ('v' 'o' 'l' 'a' 't' 'i' 'l' 'e') / ('w' 'h' 'i' 'l' 'e') / ('_' 'B' 'o' 'o' 'l') / ('_' 'C' 'o' 'm' 'p' 'l' 'e' 'x') / ('_' 'I' 'm' 'a' 'g' 'i' 'n' 'a' 'r' 'y') / ('_' 's' 't' 'd' 'c' 'a' 'l' 'l') / ('_' '_' 'd' 'e' 'c' 'l' 's' 'p' 'e' 'c') / ('_' '_' 'a' 't' 't' 'r' 'i' 'b' 'u' 't' 'e' '_' '_')) !IdChar)> */
		func() bool {
			position644, tokenIndex644, depth644 := position, tokenIndex, depth
			{
				position645 := position
				depth++
				{
					position646, tokenIndex646, depth646 := position, tokenIndex, depth
					if buffer[position] != rune('a') {
						goto l647
					}
					position++
					if buffer[position] != rune('u') {
						goto l647
					}
					position++
					if buffer[position] != rune('t') {
						goto l647
					}
					position++
					if buffer[position] != rune('o') {
						goto l647
					}
					position++
					goto l646
				l647:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('b') {
						goto l648
					}
					position++
					if buffer[position] != rune('r') {
						goto l648
					}
					position++
					if buffer[position] != rune('e') {
						goto l648
					}
					position++
					if buffer[position] != rune('a') {
						goto l648
					}
					position++
					if buffer[position] != rune('k') {
						goto l648
					}
					position++
					goto l646
				l648:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('c') {
						goto l649
					}
					position++
					if buffer[position] != rune('a') {
						goto l649
					}
					position++
					if buffer[position] != rune('s') {
						goto l649
					}
					position++
					if buffer[position] != rune('e') {
						goto l649
					}
					position++
					goto l646
				l649:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('c') {
						goto l650
					}
					position++
					if buffer[position] != rune('h') {
						goto l650
					}
					position++
					if buffer[position] != rune('a') {
						goto l650
					}
					position++
					if buffer[position] != rune('r') {
						goto l650
					}
					position++
					goto l646
				l650:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('c') {
						goto l651
					}
					position++
					if buffer[position] != rune('o') {
						goto l651
					}
					position++
					if buffer[position] != rune('n') {
						goto l651
					}
					position++
					if buffer[position] != rune('s') {
						goto l651
					}
					position++
					if buffer[position] != rune('t') {
						goto l651
					}
					position++
					goto l646
				l651:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('c') {
						goto l652
					}
					position++
					if buffer[position] != rune('o') {
						goto l652
					}
					position++
					if buffer[position] != rune('n') {
						goto l652
					}
					position++
					if buffer[position] != rune('t') {
						goto l652
					}
					position++
					if buffer[position] != rune('i') {
						goto l652
					}
					position++
					if buffer[position] != rune('n') {
						goto l652
					}
					position++
					if buffer[position] != rune('u') {
						goto l652
					}
					position++
					if buffer[position] != rune('e') {
						goto l652
					}
					position++
					goto l646
				l652:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('d') {
						goto l653
					}
					position++
					if buffer[position] != rune('e') {
						goto l653
					}
					position++
					if buffer[position] != rune('f') {
						goto l653
					}
					position++
					if buffer[position] != rune('a') {
						goto l653
					}
					position++
					if buffer[position] != rune('u') {
						goto l653
					}
					position++
					if buffer[position] != rune('l') {
						goto l653
					}
					position++
					if buffer[position] != rune('t') {
						goto l653
					}
					position++
					goto l646
				l653:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('d') {
						goto l654
					}
					position++
					if buffer[position] != rune('o') {
						goto l654
					}
					position++
					if buffer[position] != rune('u') {
						goto l654
					}
					position++
					if buffer[position] != rune('b') {
						goto l654
					}
					position++
					if buffer[position] != rune('l') {
						goto l654
					}
					position++
					if buffer[position] != rune('e') {
						goto l654
					}
					position++
					goto l646
				l654:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('d') {
						goto l655
					}
					position++
					if buffer[position] != rune('o') {
						goto l655
					}
					position++
					goto l646
				l655:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('e') {
						goto l656
					}
					position++
					if buffer[position] != rune('l') {
						goto l656
					}
					position++
					if buffer[position] != rune('s') {
						goto l656
					}
					position++
					if buffer[position] != rune('e') {
						goto l656
					}
					position++
					goto l646
				l656:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('e') {
						goto l657
					}
					position++
					if buffer[position] != rune('n') {
						goto l657
					}
					position++
					if buffer[position] != rune('u') {
						goto l657
					}
					position++
					if buffer[position] != rune('m') {
						goto l657
					}
					position++
					goto l646
				l657:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('e') {
						goto l658
					}
					position++
					if buffer[position] != rune('x') {
						goto l658
					}
					position++
					if buffer[position] != rune('t') {
						goto l658
					}
					position++
					if buffer[position] != rune('e') {
						goto l658
					}
					position++
					if buffer[position] != rune('r') {
						goto l658
					}
					position++
					if buffer[position] != rune('n') {
						goto l658
					}
					position++
					goto l646
				l658:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('f') {
						goto l659
					}
					position++
					if buffer[position] != rune('l') {
						goto l659
					}
					position++
					if buffer[position] != rune('o') {
						goto l659
					}
					position++
					if buffer[position] != rune('a') {
						goto l659
					}
					position++
					if buffer[position] != rune('t') {
						goto l659
					}
					position++
					goto l646
				l659:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('f') {
						goto l660
					}
					position++
					if buffer[position] != rune('o') {
						goto l660
					}
					position++
					if buffer[position] != rune('r') {
						goto l660
					}
					position++
					goto l646
				l660:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('g') {
						goto l661
					}
					position++
					if buffer[position] != rune('o') {
						goto l661
					}
					position++
					if buffer[position] != rune('t') {
						goto l661
					}
					position++
					if buffer[position] != rune('o') {
						goto l661
					}
					position++
					goto l646
				l661:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('i') {
						goto l662
					}
					position++
					if buffer[position] != rune('f') {
						goto l662
					}
					position++
					goto l646
				l662:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('i') {
						goto l663
					}
					position++
					if buffer[position] != rune('n') {
						goto l663
					}
					position++
					if buffer[position] != rune('t') {
						goto l663
					}
					position++
					goto l646
				l663:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('i') {
						goto l664
					}
					position++
					if buffer[position] != rune('n') {
						goto l664
					}
					position++
					if buffer[position] != rune('l') {
						goto l664
					}
					position++
					if buffer[position] != rune('i') {
						goto l664
					}
					position++
					if buffer[position] != rune('n') {
						goto l664
					}
					position++
					if buffer[position] != rune('e') {
						goto l664
					}
					position++
					goto l646
				l664:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('l') {
						goto l665
					}
					position++
					if buffer[position] != rune('o') {
						goto l665
					}
					position++
					if buffer[position] != rune('n') {
						goto l665
					}
					position++
					if buffer[position] != rune('g') {
						goto l665
					}
					position++
					goto l646
				l665:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('r') {
						goto l666
					}
					position++
					if buffer[position] != rune('e') {
						goto l666
					}
					position++
					if buffer[position] != rune('g') {
						goto l666
					}
					position++
					if buffer[position] != rune('i') {
						goto l666
					}
					position++
					if buffer[position] != rune('s') {
						goto l666
					}
					position++
					if buffer[position] != rune('t') {
						goto l666
					}
					position++
					if buffer[position] != rune('e') {
						goto l666
					}
					position++
					if buffer[position] != rune('r') {
						goto l666
					}
					position++
					goto l646
				l666:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('r') {
						goto l667
					}
					position++
					if buffer[position] != rune('e') {
						goto l667
					}
					position++
					if buffer[position] != rune('s') {
						goto l667
					}
					position++
					if buffer[position] != rune('t') {
						goto l667
					}
					position++
					if buffer[position] != rune('r') {
						goto l667
					}
					position++
					if buffer[position] != rune('i') {
						goto l667
					}
					position++
					if buffer[position] != rune('c') {
						goto l667
					}
					position++
					if buffer[position] != rune('t') {
						goto l667
					}
					position++
					goto l646
				l667:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('r') {
						goto l668
					}
					position++
					if buffer[position] != rune('e') {
						goto l668
					}
					position++
					if buffer[position] != rune('t') {
						goto l668
					}
					position++
					if buffer[position] != rune('u') {
						goto l668
					}
					position++
					if buffer[position] != rune('r') {
						goto l668
					}
					position++
					if buffer[position] != rune('n') {
						goto l668
					}
					position++
					goto l646
				l668:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('s') {
						goto l669
					}
					position++
					if buffer[position] != rune('h') {
						goto l669
					}
					position++
					if buffer[position] != rune('o') {
						goto l669
					}
					position++
					if buffer[position] != rune('r') {
						goto l669
					}
					position++
					if buffer[position] != rune('t') {
						goto l669
					}
					position++
					goto l646
				l669:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('s') {
						goto l670
					}
					position++
					if buffer[position] != rune('i') {
						goto l670
					}
					position++
					if buffer[position] != rune('g') {
						goto l670
					}
					position++
					if buffer[position] != rune('n') {
						goto l670
					}
					position++
					if buffer[position] != rune('e') {
						goto l670
					}
					position++
					if buffer[position] != rune('d') {
						goto l670
					}
					position++
					goto l646
				l670:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('s') {
						goto l671
					}
					position++
					if buffer[position] != rune('i') {
						goto l671
					}
					position++
					if buffer[position] != rune('z') {
						goto l671
					}
					position++
					if buffer[position] != rune('e') {
						goto l671
					}
					position++
					if buffer[position] != rune('o') {
						goto l671
					}
					position++
					if buffer[position] != rune('f') {
						goto l671
					}
					position++
					goto l646
				l671:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('s') {
						goto l672
					}
					position++
					if buffer[position] != rune('t') {
						goto l672
					}
					position++
					if buffer[position] != rune('a') {
						goto l672
					}
					position++
					if buffer[position] != rune('t') {
						goto l672
					}
					position++
					if buffer[position] != rune('i') {
						goto l672
					}
					position++
					if buffer[position] != rune('c') {
						goto l672
					}
					position++
					goto l646
				l672:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('s') {
						goto l673
					}
					position++
					if buffer[position] != rune('t') {
						goto l673
					}
					position++
					if buffer[position] != rune('r') {
						goto l673
					}
					position++
					if buffer[position] != rune('u') {
						goto l673
					}
					position++
					if buffer[position] != rune('c') {
						goto l673
					}
					position++
					if buffer[position] != rune('t') {
						goto l673
					}
					position++
					goto l646
				l673:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('s') {
						goto l674
					}
					position++
					if buffer[position] != rune('w') {
						goto l674
					}
					position++
					if buffer[position] != rune('i') {
						goto l674
					}
					position++
					if buffer[position] != rune('t') {
						goto l674
					}
					position++
					if buffer[position] != rune('c') {
						goto l674
					}
					position++
					if buffer[position] != rune('h') {
						goto l674
					}
					position++
					goto l646
				l674:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('t') {
						goto l675
					}
					position++
					if buffer[position] != rune('y') {
						goto l675
					}
					position++
					if buffer[position] != rune('p') {
						goto l675
					}
					position++
					if buffer[position] != rune('e') {
						goto l675
					}
					position++
					if buffer[position] != rune('d') {
						goto l675
					}
					position++
					if buffer[position] != rune('e') {
						goto l675
					}
					position++
					if buffer[position] != rune('f') {
						goto l675
					}
					position++
					goto l646
				l675:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('u') {
						goto l676
					}
					position++
					if buffer[position] != rune('n') {
						goto l676
					}
					position++
					if buffer[position] != rune('i') {
						goto l676
					}
					position++
					if buffer[position] != rune('o') {
						goto l676
					}
					position++
					if buffer[position] != rune('n') {
						goto l676
					}
					position++
					goto l646
				l676:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('u') {
						goto l677
					}
					position++
					if buffer[position] != rune('n') {
						goto l677
					}
					position++
					if buffer[position] != rune('s') {
						goto l677
					}
					position++
					if buffer[position] != rune('i') {
						goto l677
					}
					position++
					if buffer[position] != rune('g') {
						goto l677
					}
					position++
					if buffer[position] != rune('n') {
						goto l677
					}
					position++
					if buffer[position] != rune('e') {
						goto l677
					}
					position++
					if buffer[position] != rune('d') {
						goto l677
					}
					position++
					goto l646
				l677:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('v') {
						goto l678
					}
					position++
					if buffer[position] != rune('o') {
						goto l678
					}
					position++
					if buffer[position] != rune('i') {
						goto l678
					}
					position++
					if buffer[position] != rune('d') {
						goto l678
					}
					position++
					goto l646
				l678:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('v') {
						goto l679
					}
					position++
					if buffer[position] != rune('o') {
						goto l679
					}
					position++
					if buffer[position] != rune('l') {
						goto l679
					}
					position++
					if buffer[position] != rune('a') {
						goto l679
					}
					position++
					if buffer[position] != rune('t') {
						goto l679
					}
					position++
					if buffer[position] != rune('i') {
						goto l679
					}
					position++
					if buffer[position] != rune('l') {
						goto l679
					}
					position++
					if buffer[position] != rune('e') {
						goto l679
					}
					position++
					goto l646
				l679:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('w') {
						goto l680
					}
					position++
					if buffer[position] != rune('h') {
						goto l680
					}
					position++
					if buffer[position] != rune('i') {
						goto l680
					}
					position++
					if buffer[position] != rune('l') {
						goto l680
					}
					position++
					if buffer[position] != rune('e') {
						goto l680
					}
					position++
					goto l646
				l680:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('_') {
						goto l681
					}
					position++
					if buffer[position] != rune('B') {
						goto l681
					}
					position++
					if buffer[position] != rune('o') {
						goto l681
					}
					position++
					if buffer[position] != rune('o') {
						goto l681
					}
					position++
					if buffer[position] != rune('l') {
						goto l681
					}
					position++
					goto l646
				l681:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('_') {
						goto l682
					}
					position++
					if buffer[position] != rune('C') {
						goto l682
					}
					position++
					if buffer[position] != rune('o') {
						goto l682
					}
					position++
					if buffer[position] != rune('m') {
						goto l682
					}
					position++
					if buffer[position] != rune('p') {
						goto l682
					}
					position++
					if buffer[position] != rune('l') {
						goto l682
					}
					position++
					if buffer[position] != rune('e') {
						goto l682
					}
					position++
					if buffer[position] != rune('x') {
						goto l682
					}
					position++
					goto l646
				l682:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('_') {
						goto l683
					}
					position++
					if buffer[position] != rune('I') {
						goto l683
					}
					position++
					if buffer[position] != rune('m') {
						goto l683
					}
					position++
					if buffer[position] != rune('a') {
						goto l683
					}
					position++
					if buffer[position] != rune('g') {
						goto l683
					}
					position++
					if buffer[position] != rune('i') {
						goto l683
					}
					position++
					if buffer[position] != rune('n') {
						goto l683
					}
					position++
					if buffer[position] != rune('a') {
						goto l683
					}
					position++
					if buffer[position] != rune('r') {
						goto l683
					}
					position++
					if buffer[position] != rune('y') {
						goto l683
					}
					position++
					goto l646
				l683:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('_') {
						goto l684
					}
					position++
					if buffer[position] != rune('s') {
						goto l684
					}
					position++
					if buffer[position] != rune('t') {
						goto l684
					}
					position++
					if buffer[position] != rune('d') {
						goto l684
					}
					position++
					if buffer[position] != rune('c') {
						goto l684
					}
					position++
					if buffer[position] != rune('a') {
						goto l684
					}
					position++
					if buffer[position] != rune('l') {
						goto l684
					}
					position++
					if buffer[position] != rune('l') {
						goto l684
					}
					position++
					goto l646
				l684:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('_') {
						goto l685
					}
					position++
					if buffer[position] != rune('_') {
						goto l685
					}
					position++
					if buffer[position] != rune('d') {
						goto l685
					}
					position++
					if buffer[position] != rune('e') {
						goto l685
					}
					position++
					if buffer[position] != rune('c') {
						goto l685
					}
					position++
					if buffer[position] != rune('l') {
						goto l685
					}
					position++
					if buffer[position] != rune('s') {
						goto l685
					}
					position++
					if buffer[position] != rune('p') {
						goto l685
					}
					position++
					if buffer[position] != rune('e') {
						goto l685
					}
					position++
					if buffer[position] != rune('c') {
						goto l685
					}
					position++
					goto l646
				l685:
					position, tokenIndex, depth = position646, tokenIndex646, depth646
					if buffer[position] != rune('_') {
						goto l644
					}
					position++
					if buffer[position] != rune('_') {
						goto l644
					}
					position++
					if buffer[position] != rune('a') {
						goto l644
					}
					position++
					if buffer[position] != rune('t') {
						goto l644
					}
					position++
					if buffer[position] != rune('t') {
						goto l644
					}
					position++
					if buffer[position] != rune('r') {
						goto l644
					}
					position++
					if buffer[position] != rune('i') {
						goto l644
					}
					position++
					if buffer[position] != rune('b') {
						goto l644
					}
					position++
					if buffer[position] != rune('u') {
						goto l644
					}
					position++
					if buffer[position] != rune('t') {
						goto l644
					}
					position++
					if buffer[position] != rune('e') {
						goto l644
					}
					position++
					if buffer[position] != rune('_') {
						goto l644
					}
					position++
					if buffer[position] != rune('_') {
						goto l644
					}
					position++
				}
			l646:
				{
					position686, tokenIndex686, depth686 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l686
					}
					goto l644
				l686:
					position, tokenIndex, depth = position686, tokenIndex686, depth686
				}
				depth--
				add(ruleKeyword, position645)
			}
			return true
		l644:
			position, tokenIndex, depth = position644, tokenIndex644, depth644
			return false
		},
		/* 119 Identifier <- <(!Keyword IdNondigit IdChar* Spacing)> */
		func() bool {
			position687, tokenIndex687, depth687 := position, tokenIndex, depth
			{
				position688 := position
				depth++
				{
					position689, tokenIndex689, depth689 := position, tokenIndex, depth
					if !_rules[ruleKeyword]() {
						goto l689
					}
					goto l687
				l689:
					position, tokenIndex, depth = position689, tokenIndex689, depth689
				}
				if !_rules[ruleIdNondigit]() {
					goto l687
				}
			l690:
				{
					position691, tokenIndex691, depth691 := position, tokenIndex, depth
					if !_rules[ruleIdChar]() {
						goto l691
					}
					goto l690
				l691:
					position, tokenIndex, depth = position691, tokenIndex691, depth691
				}
				if !_rules[ruleSpacing]() {
					goto l687
				}
				depth--
				add(ruleIdentifier, position688)
			}
			return true
		l687:
			position, tokenIndex, depth = position687, tokenIndex687, depth687
			return false
		},
		/* 120 IdNondigit <- <([a-z] / [A-Z] / '_' / UniversalCharacter)> */
		func() bool {
			position692, tokenIndex692, depth692 := position, tokenIndex, depth
			{
				position693 := position
				depth++
				{
					position694, tokenIndex694, depth694 := position, tokenIndex, depth
					if c := buffer[position]; c < rune('a') || c > rune('z') {
						goto l695
					}
					position++
					goto l694
				l695:
					position, tokenIndex, depth = position694, tokenIndex694, depth694
					if c := buffer[position]; c < rune('A') || c > rune('Z') {
						goto l696
					}
					position++
					goto l694
				l696:
					position, tokenIndex, depth = position694, tokenIndex694, depth694
					if buffer[position] != rune('_') {
						goto l697
					}
					position++
					goto l694
				l697:
					position, tokenIndex, depth = position694, tokenIndex694, depth694
					if !_rules[ruleUniversalCharacter]() {
						goto l692
					}
				}
			l694:
				depth--
				add(ruleIdNondigit, position693)
			}
			return true
		l692:
			position, tokenIndex, depth = position692, tokenIndex692, depth692
			return false
		},
		/* 121 IdChar <- <([a-z] / [A-Z] / [0-9] / '_' / UniversalCharacter)> */
		func() bool {
			position698, tokenIndex698, depth698 := position, tokenIndex, depth
			{
				position699 := position
				depth++
				{
					position700, tokenIndex700, depth700 := position, tokenIndex, depth
					if c := buffer[position]; c < rune('a') || c > rune('z') {
						goto l701
					}
					position++
					goto l700
				l701:
					position, tokenIndex, depth = position700, tokenIndex700, depth700
					if c := buffer[position]; c < rune('A') || c > rune('Z') {
						goto l702
					}
					position++
					goto l700
				l702:
					position, tokenIndex, depth = position700, tokenIndex700, depth700
					if c := buffer[position]; c < rune('0') || c > rune('9') {
						goto l703
					}
					position++
					goto l700
				l703:
					position, tokenIndex, depth = position700, tokenIndex700, depth700
					if buffer[position] != rune('_') {
						goto l704
					}
					position++
					goto l700
				l704:
					position, tokenIndex, depth = position700, tokenIndex700, depth700
					if !_rules[ruleUniversalCharacter]() {
						goto l698
					}
				}
			l700:
				depth--
				add(ruleIdChar, position699)
			}
			return true
		l698:
			position, tokenIndex, depth = position698, tokenIndex698, depth698
			return false
		},
		/* 122 UniversalCharacter <- <(('\\' 'u' HexQuad) / ('\\' 'U' HexQuad HexQuad))> */
		func() bool {
			position705, tokenIndex705, depth705 := position, tokenIndex, depth
			{
				position706 := position
				depth++
				{
					position707, tokenIndex707, depth707 := position, tokenIndex, depth
					if buffer[position] != rune('\\') {
						goto l708
					}
					position++
					if buffer[position] != rune('u') {
						goto l708
					}
					position++
					if !_rules[ruleHexQuad]() {
						goto l708
					}
					goto l707
				l708:
					position, tokenIndex, depth = position707, tokenIndex707, depth707
					if buffer[position] != rune('\\') {
						goto l705
					}
					position++
					if buffer[position] != rune('U') {
						goto l705
					}
					position++
					if !_rules[ruleHexQuad]() {
						goto l705
					}
					if !_rules[ruleHexQuad]() {
						goto l705
					}
				}
			l707:
				depth--
				add(ruleUniversalCharacter, position706)
			}
			return true
		l705:
			position, tokenIndex, depth = position705, tokenIndex705, depth705
			return false
		},
		/* 123 HexQuad <- <(HexDigit HexDigit HexDigit HexDigit)> */
		func() bool {
			position709, tokenIndex709, depth709 := position, tokenIndex, depth
			{
				position710 := position
				depth++
				if !_rules[ruleHexDigit]() {
					goto l709
				}
				if !_rules[ruleHexDigit]() {
					goto l709
				}
				if !_rules[ruleHexDigit]() {
					goto l709
				}
				if !_rules[ruleHexDigit]() {
					goto l709
				}
				depth--
				add(ruleHexQuad, position710)
			}
			return true
		l709:
			position, tokenIndex, depth = position709, tokenIndex709, depth709
			return false
		},
		/* 124 Constant <- <(FloatConstant / IntegerConstant / EnumerationConstant / CharacterConstant)> */
		func() bool {
			position711, tokenIndex711, depth711 := position, tokenIndex, depth
			{
				position712 := position
				depth++
				{
					position713, tokenIndex713, depth713 := position, tokenIndex, depth
					if !_rules[ruleFloatConstant]() {
						goto l714
					}
					goto l713
				l714:
					position, tokenIndex, depth = position713, tokenIndex713, depth713
					if !_rules[ruleIntegerConstant]() {
						goto l715
					}
					goto l713
				l715:
					position, tokenIndex, depth = position713, tokenIndex713, depth713
					if !_rules[ruleEnumerationConstant]() {
						goto l716
					}
					goto l713
				l716:
					position, tokenIndex, depth = position713, tokenIndex713, depth713
					if !_rules[ruleCharacterConstant]() {
						goto l711
					}
				}
			l713:
				depth--
				add(ruleConstant, position712)
			}
			return true
		l711:
			position, tokenIndex, depth = position711, tokenIndex711, depth711
			return false
		},
		/* 125 IntegerConstant <- <((DecimalConstant / HexConstant / OctalConstant) IntegerSuffix? Spacing)> */
		func() bool {
			position717, tokenIndex717, depth717 := position, tokenIndex, depth
			{
				position718 := position
				depth++
				{
					position719, tokenIndex719, depth719 := position, tokenIndex, depth
					if !_rules[ruleDecimalConstant]() {
						goto l720
					}
					goto l719
				l720:
					position, tokenIndex, depth = position719, tokenIndex719, depth719
					if !_rules[ruleHexConstant]() {
						goto l721
					}
					goto l719
				l721:
					position, tokenIndex, depth = position719, tokenIndex719, depth719
					if !_rules[ruleOctalConstant]() {
						goto l717
					}
				}
			l719:
				{
					position722, tokenIndex722, depth722 := position, tokenIndex, depth
					if !_rules[ruleIntegerSuffix]() {
						goto l722
					}
					goto l723
				l722:
					position, tokenIndex, depth = position722, tokenIndex722, depth722
				}
			l723:
				if !_rules[ruleSpacing]() {
					goto l717
				}
				depth--
				add(ruleIntegerConstant, position718)
			}
			return true
		l717:
			position, tokenIndex, depth = position717, tokenIndex717, depth717
			return false
		},
		/* 126 DecimalConstant <- <([1-9] [0-9]*)> */
		func() bool {
			position724, tokenIndex724, depth724 := position, tokenIndex, depth
			{
				position725 := position
				depth++
				if c := buffer[position]; c < rune('1') || c > rune('9') {
					goto l724
				}
				position++
			l726:
				{
					position727, tokenIndex727, depth727 := position, tokenIndex, depth
					if c := buffer[position]; c < rune('0') || c > rune('9') {
						goto l727
					}
					position++
					goto l726
				l727:
					position, tokenIndex, depth = position727, tokenIndex727, depth727
				}
				depth--
				add(ruleDecimalConstant, position725)
			}
			return true
		l724:
			position, tokenIndex, depth = position724, tokenIndex724, depth724
			return false
		},
		/* 127 OctalConstant <- <('0' [0-7]*)> */
		func() bool {
			position728, tokenIndex728, depth728 := position, tokenIndex, depth
			{
				position729 := position
				depth++
				if buffer[position] != rune('0') {
					goto l728
				}
				position++
			l730:
				{
					position731, tokenIndex731, depth731 := position, tokenIndex, depth
					if c := buffer[position]; c < rune('0') || c > rune('7') {
						goto l731
					}
					position++
					goto l730
				l731:
					position, tokenIndex, depth = position731, tokenIndex731, depth731
				}
				depth--
				add(ruleOctalConstant, position729)
			}
			return true
		l728:
			position, tokenIndex, depth = position728, tokenIndex728, depth728
			return false
		},
		/* 128 HexConstant <- <(HexPrefix HexDigit+)> */
		func() bool {
			position732, tokenIndex732, depth732 := position, tokenIndex, depth
			{
				position733 := position
				depth++
				if !_rules[ruleHexPrefix]() {
					goto l732
				}
				if !_rules[ruleHexDigit]() {
					goto l732
				}
			l734:
				{
					position735, tokenIndex735, depth735 := position, tokenIndex, depth
					if !_rules[ruleHexDigit]() {
						goto l735
					}
					goto l734
				l735:
					position, tokenIndex, depth = position735, tokenIndex735, depth735
				}
				depth--
				add(ruleHexConstant, position733)
			}
			return true
		l732:
			position, tokenIndex, depth = position732, tokenIndex732, depth732
			return false
		},
		/* 129 HexPrefix <- <(('0' 'x') / ('0' 'X'))> */
		func() bool {
			position736, tokenIndex736, depth736 := position, tokenIndex, depth
			{
				position737 := position
				depth++
				{
					position738, tokenIndex738, depth738 := position, tokenIndex, depth
					if buffer[position] != rune('0') {
						goto l739
					}
					position++
					if buffer[position] != rune('x') {
						goto l739
					}
					position++
					goto l738
				l739:
					position, tokenIndex, depth = position738, tokenIndex738, depth738
					if buffer[position] != rune('0') {
						goto l736
					}
					position++
					if buffer[position] != rune('X') {
						goto l736
					}
					position++
				}
			l738:
				depth--
				add(ruleHexPrefix, position737)
			}
			return true
		l736:
			position, tokenIndex, depth = position736, tokenIndex736, depth736
			return false
		},
		/* 130 HexDigit <- <([a-f] / [A-F] / [0-9])> */
		func() bool {
			position740, tokenIndex740, depth740 := position, tokenIndex, depth
			{
				position741 := position
				depth++
				{
					position742, tokenIndex742, depth742 := position, tokenIndex, depth
					if c := buffer[position]; c < rune('a') || c > rune('f') {
						goto l743
					}
					position++
					goto l742
				l743:
					position, tokenIndex, depth = position742, tokenIndex742, depth742
					if c := buffer[position]; c < rune('A') || c > rune('F') {
						goto l744
					}
					position++
					goto l742
				l744:
					position, tokenIndex, depth = position742, tokenIndex742, depth742
					if c := buffer[position]; c < rune('0') || c > rune('9') {
						goto l740
					}
					position++
				}
			l742:
				depth--
				add(ruleHexDigit, position741)
			}
			return true
		l740:
			position, tokenIndex, depth = position740, tokenIndex740, depth740
			return false
		},
		/* 131 IntegerSuffix <- <((('u' / 'U') Lsuffix?) / (Lsuffix ('u' / 'U')?))> */
		func() bool {
			position745, tokenIndex745, depth745 := position, tokenIndex, depth
			{
				position746 := position
				depth++
				{
					position747, tokenIndex747, depth747 := position, tokenIndex, depth
					{
						position749, tokenIndex749, depth749 := position, tokenIndex, depth
						if buffer[position] != rune('u') {
							goto l750
						}
						position++
						goto l749
					l750:
						position, tokenIndex, depth = position749, tokenIndex749, depth749
						if buffer[position] != rune('U') {
							goto l748
						}
						position++
					}
				l749:
					{
						position751, tokenIndex751, depth751 := position, tokenIndex, depth
						if !_rules[ruleLsuffix]() {
							goto l751
						}
						goto l752
					l751:
						position, tokenIndex, depth = position751, tokenIndex751, depth751
					}
				l752:
					goto l747
				l748:
					position, tokenIndex, depth = position747, tokenIndex747, depth747
					if !_rules[ruleLsuffix]() {
						goto l745
					}
					{
						position753, tokenIndex753, depth753 := position, tokenIndex, depth
						{
							position755, tokenIndex755, depth755 := position, tokenIndex, depth
							if buffer[position] != rune('u') {
								goto l756
							}
							position++
							goto l755
						l756:
							position, tokenIndex, depth = position755, tokenIndex755, depth755
							if buffer[position] != rune('U') {
								goto l753
							}
							position++
						}
					l755:
						goto l754
					l753:
						position, tokenIndex, depth = position753, tokenIndex753, depth753
					}
				l754:
				}
			l747:
				depth--
				add(ruleIntegerSuffix, position746)
			}
			return true
		l745:
			position, tokenIndex, depth = position745, tokenIndex745, depth745
			return false
		},
		/* 132 Lsuffix <- <(('l' 'l') / ('L' 'L') / ('l' / 'L'))> */
		func() bool {
			position757, tokenIndex757, depth757 := position, tokenIndex, depth
			{
				position758 := position
				depth++
				{
					position759, tokenIndex759, depth759 := position, tokenIndex, depth
					if buffer[position] != rune('l') {
						goto l760
					}
					position++
					if buffer[position] != rune('l') {
						goto l760
					}
					position++
					goto l759
				l760:
					position, tokenIndex, depth = position759, tokenIndex759, depth759
					if buffer[position] != rune('L') {
						goto l761
					}
					position++
					if buffer[position] != rune('L') {
						goto l761
					}
					position++
					goto l759
				l761:
					position, tokenIndex, depth = position759, tokenIndex759, depth759
					{
						position762, tokenIndex762, depth762 := position, tokenIndex, depth
						if buffer[position] != rune('l') {
							goto l763
						}
						position++
						goto l762
					l763:
						position, tokenIndex, depth = position762, tokenIndex762, depth762
						if buffer[position] != rune('L') {
							goto l757
						}
						position++
					}
				l762:
				}
			l759:
				depth--
				add(ruleLsuffix, position758)
			}
			return true
		l757:
			position, tokenIndex, depth = position757, tokenIndex757, depth757
			return false
		},
		/* 133 FloatConstant <- <((DecimalFloatConstant / HexFloatConstant) FloatSuffix? Spacing)> */
		func() bool {
			position764, tokenIndex764, depth764 := position, tokenIndex, depth
			{
				position765 := position
				depth++
				{
					position766, tokenIndex766, depth766 := position, tokenIndex, depth
					if !_rules[ruleDecimalFloatConstant]() {
						goto l767
					}
					goto l766
				l767:
					position, tokenIndex, depth = position766, tokenIndex766, depth766
					if !_rules[ruleHexFloatConstant]() {
						goto l764
					}
				}
			l766:
				{
					position768, tokenIndex768, depth768 := position, tokenIndex, depth
					if !_rules[ruleFloatSuffix]() {
						goto l768
					}
					goto l769
				l768:
					position, tokenIndex, depth = position768, tokenIndex768, depth768
				}
			l769:
				if !_rules[ruleSpacing]() {
					goto l764
				}
				depth--
				add(ruleFloatConstant, position765)
			}
			return true
		l764:
			position, tokenIndex, depth = position764, tokenIndex764, depth764
			return false
		},
		/* 134 DecimalFloatConstant <- <((Fraction Exponent?) / ([0-9]+ Exponent))> */
		func() bool {
			position770, tokenIndex770, depth770 := position, tokenIndex, depth
			{
				position771 := position
				depth++
				{
					position772, tokenIndex772, depth772 := position, tokenIndex, depth
					if !_rules[ruleFraction]() {
						goto l773
					}
					{
						position774, tokenIndex774, depth774 := position, tokenIndex, depth
						if !_rules[ruleExponent]() {
							goto l774
						}
						goto l775
					l774:
						position, tokenIndex, depth = position774, tokenIndex774, depth774
					}
				l775:
					goto l772
				l773:
					position, tokenIndex, depth = position772, tokenIndex772, depth772
					if c := buffer[position]; c < rune('0') || c > rune('9') {
						goto l770
					}
					position++
				l776:
					{
						position777, tokenIndex777, depth777 := position, tokenIndex, depth
						if c := buffer[position]; c < rune('0') || c > rune('9') {
							goto l777
						}
						position++
						goto l776
					l777:
						position, tokenIndex, depth = position777, tokenIndex777, depth777
					}
					if !_rules[ruleExponent]() {
						goto l770
					}
				}
			l772:
				depth--
				add(ruleDecimalFloatConstant, position771)
			}
			return true
		l770:
			position, tokenIndex, depth = position770, tokenIndex770, depth770
			return false
		},
		/* 135 HexFloatConstant <- <((HexPrefix HexFraction BinaryExponent?) / (HexPrefix HexDigit+ BinaryExponent))> */
		func() bool {
			position778, tokenIndex778, depth778 := position, tokenIndex, depth
			{
				position779 := position
				depth++
				{
					position780, tokenIndex780, depth780 := position, tokenIndex, depth
					if !_rules[ruleHexPrefix]() {
						goto l781
					}
					if !_rules[ruleHexFraction]() {
						goto l781
					}
					{
						position782, tokenIndex782, depth782 := position, tokenIndex, depth
						if !_rules[ruleBinaryExponent]() {
							goto l782
						}
						goto l783
					l782:
						position, tokenIndex, depth = position782, tokenIndex782, depth782
					}
				l783:
					goto l780
				l781:
					position, tokenIndex, depth = position780, tokenIndex780, depth780
					if !_rules[ruleHexPrefix]() {
						goto l778
					}
					if !_rules[ruleHexDigit]() {
						goto l778
					}
				l784:
					{
						position785, tokenIndex785, depth785 := position, tokenIndex, depth
						if !_rules[ruleHexDigit]() {
							goto l785
						}
						goto l784
					l785:
						position, tokenIndex, depth = position785, tokenIndex785, depth785
					}
					if !_rules[ruleBinaryExponent]() {
						goto l778
					}
				}
			l780:
				depth--
				add(ruleHexFloatConstant, position779)
			}
			return true
		l778:
			position, tokenIndex, depth = position778, tokenIndex778, depth778
			return false
		},
		/* 136 Fraction <- <(([0-9]* '.' [0-9]+) / ([0-9]+ '.'))> */
		func() bool {
			position786, tokenIndex786, depth786 := position, tokenIndex, depth
			{
				position787 := position
				depth++
				{
					position788, tokenIndex788, depth788 := position, tokenIndex, depth
				l790:
					{
						position791, tokenIndex791, depth791 := position, tokenIndex, depth
						if c := buffer[position]; c < rune('0') || c > rune('9') {
							goto l791
						}
						position++
						goto l790
					l791:
						position, tokenIndex, depth = position791, tokenIndex791, depth791
					}
					if buffer[position] != rune('.') {
						goto l789
					}
					position++
					if c := buffer[position]; c < rune('0') || c > rune('9') {
						goto l789
					}
					position++
				l792:
					{
						position793, tokenIndex793, depth793 := position, tokenIndex, depth
						if c := buffer[position]; c < rune('0') || c > rune('9') {
							goto l793
						}
						position++
						goto l792
					l793:
						position, tokenIndex, depth = position793, tokenIndex793, depth793
					}
					goto l788
				l789:
					position, tokenIndex, depth = position788, tokenIndex788, depth788
					if c := buffer[position]; c < rune('0') || c > rune('9') {
						goto l786
					}
					position++
				l794:
					{
						position795, tokenIndex795, depth795 := position, tokenIndex, depth
						if c := buffer[position]; c < rune('0') || c > rune('9') {
							goto l795
						}
						position++
						goto l794
					l795:
						position, tokenIndex, depth = position795, tokenIndex795, depth795
					}
					if buffer[position] != rune('.') {
						goto l786
					}
					position++
				}
			l788:
				depth--
				add(ruleFraction, position787)
			}
			return true
		l786:
			position, tokenIndex, depth = position786, tokenIndex786, depth786
			return false
		},
		/* 137 HexFraction <- <((HexDigit* '.' HexDigit+) / (HexDigit+ '.'))> */
		func() bool {
			position796, tokenIndex796, depth796 := position, tokenIndex, depth
			{
				position797 := position
				depth++
				{
					position798, tokenIndex798, depth798 := position, tokenIndex, depth
				l800:
					{
						position801, tokenIndex801, depth801 := position, tokenIndex, depth
						if !_rules[ruleHexDigit]() {
							goto l801
						}
						goto l800
					l801:
						position, tokenIndex, depth = position801, tokenIndex801, depth801
					}
					if buffer[position] != rune('.') {
						goto l799
					}
					position++
					if !_rules[ruleHexDigit]() {
						goto l799
					}
				l802:
					{
						position803, tokenIndex803, depth803 := position, tokenIndex, depth
						if !_rules[ruleHexDigit]() {
							goto l803
						}
						goto l802
					l803:
						position, tokenIndex, depth = position803, tokenIndex803, depth803
					}
					goto l798
				l799:
					position, tokenIndex, depth = position798, tokenIndex798, depth798
					if !_rules[ruleHexDigit]() {
						goto l796
					}
				l804:
					{
						position805, tokenIndex805, depth805 := position, tokenIndex, depth
						if !_rules[ruleHexDigit]() {
							goto l805
						}
						goto l804
					l805:
						position, tokenIndex, depth = position805, tokenIndex805, depth805
					}
					if buffer[position] != rune('.') {
						goto l796
					}
					position++
				}
			l798:
				depth--
				add(ruleHexFraction, position797)
			}
			return true
		l796:
			position, tokenIndex, depth = position796, tokenIndex796, depth796
			return false
		},
		/* 138 Exponent <- <(('e' / 'E') ('+' / '-')? [0-9]+)> */
		func() bool {
			position806, tokenIndex806, depth806 := position, tokenIndex, depth
			{
				position807 := position
				depth++
				{
					position808, tokenIndex808, depth808 := position, tokenIndex, depth
					if buffer[position] != rune('e') {
						goto l809
					}
					position++
					goto l808
				l809:
					position, tokenIndex, depth = position808, tokenIndex808, depth808
					if buffer[position] != rune('E') {
						goto l806
					}
					position++
				}
			l808:
				{
					position810, tokenIndex810, depth810 := position, tokenIndex, depth
					{
						position812, tokenIndex812, depth812 := position, tokenIndex, depth
						if buffer[position] != rune('+') {
							goto l813
						}
						position++
						goto l812
					l813:
						position, tokenIndex, depth = position812, tokenIndex812, depth812
						if buffer[position] != rune('-') {
							goto l810
						}
						position++
					}
				l812:
					goto l811
				l810:
					position, tokenIndex, depth = position810, tokenIndex810, depth810
				}
			l811:
				if c := buffer[position]; c < rune('0') || c > rune('9') {
					goto l806
				}
				position++
			l814:
				{
					position815, tokenIndex815, depth815 := position, tokenIndex, depth
					if c := buffer[position]; c < rune('0') || c > rune('9') {
						goto l815
					}
					position++
					goto l814
				l815:
					position, tokenIndex, depth = position815, tokenIndex815, depth815
				}
				depth--
				add(ruleExponent, position807)
			}
			return true
		l806:
			position, tokenIndex, depth = position806, tokenIndex806, depth806
			return false
		},
		/* 139 BinaryExponent <- <(('p' / 'P') ('+' / '-')? [0-9]+)> */
		func() bool {
			position816, tokenIndex816, depth816 := position, tokenIndex, depth
			{
				position817 := position
				depth++
				{
					position818, tokenIndex818, depth818 := position, tokenIndex, depth
					if buffer[position] != rune('p') {
						goto l819
					}
					position++
					goto l818
				l819:
					position, tokenIndex, depth = position818, tokenIndex818, depth818
					if buffer[position] != rune('P') {
						goto l816
					}
					position++
				}
			l818:
				{
					position820, tokenIndex820, depth820 := position, tokenIndex, depth
					{
						position822, tokenIndex822, depth822 := position, tokenIndex, depth
						if buffer[position] != rune('+') {
							goto l823
						}
						position++
						goto l822
					l823:
						position, tokenIndex, depth = position822, tokenIndex822, depth822
						if buffer[position] != rune('-') {
							goto l820
						}
						position++
					}
				l822:
					goto l821
				l820:
					position, tokenIndex, depth = position820, tokenIndex820, depth820
				}
			l821:
				if c := buffer[position]; c < rune('0') || c > rune('9') {
					goto l816
				}
				position++
			l824:
				{
					position825, tokenIndex825, depth825 := position, tokenIndex, depth
					if c := buffer[position]; c < rune('0') || c > rune('9') {
						goto l825
					}
					position++
					goto l824
				l825:
					position, tokenIndex, depth = position825, tokenIndex825, depth825
				}
				depth--
				add(ruleBinaryExponent, position817)
			}
			return true
		l816:
			position, tokenIndex, depth = position816, tokenIndex816, depth816
			return false
		},
		/* 140 FloatSuffix <- <('f' / 'l' / 'F' / 'L')> */
		func() bool {
			position826, tokenIndex826, depth826 := position, tokenIndex, depth
			{
				position827 := position
				depth++
				{
					position828, tokenIndex828, depth828 := position, tokenIndex, depth
					if buffer[position] != rune('f') {
						goto l829
					}
					position++
					goto l828
				l829:
					position, tokenIndex, depth = position828, tokenIndex828, depth828
					if buffer[position] != rune('l') {
						goto l830
					}
					position++
					goto l828
				l830:
					position, tokenIndex, depth = position828, tokenIndex828, depth828
					if buffer[position] != rune('F') {
						goto l831
					}
					position++
					goto l828
				l831:
					position, tokenIndex, depth = position828, tokenIndex828, depth828
					if buffer[position] != rune('L') {
						goto l826
					}
					position++
				}
			l828:
				depth--
				add(ruleFloatSuffix, position827)
			}
			return true
		l826:
			position, tokenIndex, depth = position826, tokenIndex826, depth826
			return false
		},
		/* 141 EnumerationConstant <- <Identifier> */
		func() bool {
			position832, tokenIndex832, depth832 := position, tokenIndex, depth
			{
				position833 := position
				depth++
				if !_rules[ruleIdentifier]() {
					goto l832
				}
				depth--
				add(ruleEnumerationConstant, position833)
			}
			return true
		l832:
			position, tokenIndex, depth = position832, tokenIndex832, depth832
			return false
		},
		/* 142 CharacterConstant <- <('L'? '\'' Char* '\'' Spacing)> */
		func() bool {
			position834, tokenIndex834, depth834 := position, tokenIndex, depth
			{
				position835 := position
				depth++
				{
					position836, tokenIndex836, depth836 := position, tokenIndex, depth
					if buffer[position] != rune('L') {
						goto l836
					}
					position++
					goto l837
				l836:
					position, tokenIndex, depth = position836, tokenIndex836, depth836
				}
			l837:
				if buffer[position] != rune('\'') {
					goto l834
				}
				position++
			l838:
				{
					position839, tokenIndex839, depth839 := position, tokenIndex, depth
					if !_rules[ruleChar]() {
						goto l839
					}
					goto l838
				l839:
					position, tokenIndex, depth = position839, tokenIndex839, depth839
				}
				if buffer[position] != rune('\'') {
					goto l834
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l834
				}
				depth--
				add(ruleCharacterConstant, position835)
			}
			return true
		l834:
			position, tokenIndex, depth = position834, tokenIndex834, depth834
			return false
		},
		/* 143 Char <- <(Escape / (!('\'' / '\n' / '\\') .))> */
		func() bool {
			position840, tokenIndex840, depth840 := position, tokenIndex, depth
			{
				position841 := position
				depth++
				{
					position842, tokenIndex842, depth842 := position, tokenIndex, depth
					if !_rules[ruleEscape]() {
						goto l843
					}
					goto l842
				l843:
					position, tokenIndex, depth = position842, tokenIndex842, depth842
					{
						position844, tokenIndex844, depth844 := position, tokenIndex, depth
						{
							position845, tokenIndex845, depth845 := position, tokenIndex, depth
							if buffer[position] != rune('\'') {
								goto l846
							}
							position++
							goto l845
						l846:
							position, tokenIndex, depth = position845, tokenIndex845, depth845
							if buffer[position] != rune('\n') {
								goto l847
							}
							position++
							goto l845
						l847:
							position, tokenIndex, depth = position845, tokenIndex845, depth845
							if buffer[position] != rune('\\') {
								goto l844
							}
							position++
						}
					l845:
						goto l840
					l844:
						position, tokenIndex, depth = position844, tokenIndex844, depth844
					}
					if !matchDot() {
						goto l840
					}
				}
			l842:
				depth--
				add(ruleChar, position841)
			}
			return true
		l840:
			position, tokenIndex, depth = position840, tokenIndex840, depth840
			return false
		},
		/* 144 Escape <- <(SimpleEscape / OctalEscape / HexEscape / UniversalCharacter)> */
		func() bool {
			position848, tokenIndex848, depth848 := position, tokenIndex, depth
			{
				position849 := position
				depth++
				{
					position850, tokenIndex850, depth850 := position, tokenIndex, depth
					if !_rules[ruleSimpleEscape]() {
						goto l851
					}
					goto l850
				l851:
					position, tokenIndex, depth = position850, tokenIndex850, depth850
					if !_rules[ruleOctalEscape]() {
						goto l852
					}
					goto l850
				l852:
					position, tokenIndex, depth = position850, tokenIndex850, depth850
					if !_rules[ruleHexEscape]() {
						goto l853
					}
					goto l850
				l853:
					position, tokenIndex, depth = position850, tokenIndex850, depth850
					if !_rules[ruleUniversalCharacter]() {
						goto l848
					}
				}
			l850:
				depth--
				add(ruleEscape, position849)
			}
			return true
		l848:
			position, tokenIndex, depth = position848, tokenIndex848, depth848
			return false
		},
		/* 145 SimpleEscape <- <('\\' ('\'' / '"' / '?' / '\\' / '%' / 'a' / 'b' / 'f' / 'n' / 'r' / 't' / 'v'))> */
		func() bool {
			position854, tokenIndex854, depth854 := position, tokenIndex, depth
			{
				position855 := position
				depth++
				if buffer[position] != rune('\\') {
					goto l854
				}
				position++
				{
					position856, tokenIndex856, depth856 := position, tokenIndex, depth
					if buffer[position] != rune('\'') {
						goto l857
					}
					position++
					goto l856
				l857:
					position, tokenIndex, depth = position856, tokenIndex856, depth856
					if buffer[position] != rune('"') {
						goto l858
					}
					position++
					goto l856
				l858:
					position, tokenIndex, depth = position856, tokenIndex856, depth856
					if buffer[position] != rune('?') {
						goto l859
					}
					position++
					goto l856
				l859:
					position, tokenIndex, depth = position856, tokenIndex856, depth856
					if buffer[position] != rune('\\') {
						goto l860
					}
					position++
					goto l856
				l860:
					position, tokenIndex, depth = position856, tokenIndex856, depth856
					if buffer[position] != rune('%') {
						goto l861
					}
					position++
					goto l856
				l861:
					position, tokenIndex, depth = position856, tokenIndex856, depth856
					if buffer[position] != rune('a') {
						goto l862
					}
					position++
					goto l856
				l862:
					position, tokenIndex, depth = position856, tokenIndex856, depth856
					if buffer[position] != rune('b') {
						goto l863
					}
					position++
					goto l856
				l863:
					position, tokenIndex, depth = position856, tokenIndex856, depth856
					if buffer[position] != rune('f') {
						goto l864
					}
					position++
					goto l856
				l864:
					position, tokenIndex, depth = position856, tokenIndex856, depth856
					if buffer[position] != rune('n') {
						goto l865
					}
					position++
					goto l856
				l865:
					position, tokenIndex, depth = position856, tokenIndex856, depth856
					if buffer[position] != rune('r') {
						goto l866
					}
					position++
					goto l856
				l866:
					position, tokenIndex, depth = position856, tokenIndex856, depth856
					if buffer[position] != rune('t') {
						goto l867
					}
					position++
					goto l856
				l867:
					position, tokenIndex, depth = position856, tokenIndex856, depth856
					if buffer[position] != rune('v') {
						goto l854
					}
					position++
				}
			l856:
				depth--
				add(ruleSimpleEscape, position855)
			}
			return true
		l854:
			position, tokenIndex, depth = position854, tokenIndex854, depth854
			return false
		},
		/* 146 OctalEscape <- <('\\' [0-7] [0-7]? [0-7]?)> */
		func() bool {
			position868, tokenIndex868, depth868 := position, tokenIndex, depth
			{
				position869 := position
				depth++
				if buffer[position] != rune('\\') {
					goto l868
				}
				position++
				if c := buffer[position]; c < rune('0') || c > rune('7') {
					goto l868
				}
				position++
				{
					position870, tokenIndex870, depth870 := position, tokenIndex, depth
					if c := buffer[position]; c < rune('0') || c > rune('7') {
						goto l870
					}
					position++
					goto l871
				l870:
					position, tokenIndex, depth = position870, tokenIndex870, depth870
				}
			l871:
				{
					position872, tokenIndex872, depth872 := position, tokenIndex, depth
					if c := buffer[position]; c < rune('0') || c > rune('7') {
						goto l872
					}
					position++
					goto l873
				l872:
					position, tokenIndex, depth = position872, tokenIndex872, depth872
				}
			l873:
				depth--
				add(ruleOctalEscape, position869)
			}
			return true
		l868:
			position, tokenIndex, depth = position868, tokenIndex868, depth868
			return false
		},
		/* 147 HexEscape <- <('\\' 'x' HexDigit+)> */
		func() bool {
			position874, tokenIndex874, depth874 := position, tokenIndex, depth
			{
				position875 := position
				depth++
				if buffer[position] != rune('\\') {
					goto l874
				}
				position++
				if buffer[position] != rune('x') {
					goto l874
				}
				position++
				if !_rules[ruleHexDigit]() {
					goto l874
				}
			l876:
				{
					position877, tokenIndex877, depth877 := position, tokenIndex, depth
					if !_rules[ruleHexDigit]() {
						goto l877
					}
					goto l876
				l877:
					position, tokenIndex, depth = position877, tokenIndex877, depth877
				}
				depth--
				add(ruleHexEscape, position875)
			}
			return true
		l874:
			position, tokenIndex, depth = position874, tokenIndex874, depth874
			return false
		},
		/* 148 StringLiteral <- <('L'? ('"' StringChar* '"' Spacing)+)> */
		func() bool {
			position878, tokenIndex878, depth878 := position, tokenIndex, depth
			{
				position879 := position
				depth++
				{
					position880, tokenIndex880, depth880 := position, tokenIndex, depth
					if buffer[position] != rune('L') {
						goto l880
					}
					position++
					goto l881
				l880:
					position, tokenIndex, depth = position880, tokenIndex880, depth880
				}
			l881:
				if buffer[position] != rune('"') {
					goto l878
				}
				position++
			l884:
				{
					position885, tokenIndex885, depth885 := position, tokenIndex, depth
					if !_rules[ruleStringChar]() {
						goto l885
					}
					goto l884
				l885:
					position, tokenIndex, depth = position885, tokenIndex885, depth885
				}
				if buffer[position] != rune('"') {
					goto l878
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l878
				}
			l882:
				{
					position883, tokenIndex883, depth883 := position, tokenIndex, depth
					if buffer[position] != rune('"') {
						goto l883
					}
					position++
				l886:
					{
						position887, tokenIndex887, depth887 := position, tokenIndex, depth
						if !_rules[ruleStringChar]() {
							goto l887
						}
						goto l886
					l887:
						position, tokenIndex, depth = position887, tokenIndex887, depth887
					}
					if buffer[position] != rune('"') {
						goto l883
					}
					position++
					if !_rules[ruleSpacing]() {
						goto l883
					}
					goto l882
				l883:
					position, tokenIndex, depth = position883, tokenIndex883, depth883
				}
				depth--
				add(ruleStringLiteral, position879)
			}
			return true
		l878:
			position, tokenIndex, depth = position878, tokenIndex878, depth878
			return false
		},
		/* 149 StringChar <- <(Escape / (!('"' / '\n' / '\\') .))> */
		func() bool {
			position888, tokenIndex888, depth888 := position, tokenIndex, depth
			{
				position889 := position
				depth++
				{
					position890, tokenIndex890, depth890 := position, tokenIndex, depth
					if !_rules[ruleEscape]() {
						goto l891
					}
					goto l890
				l891:
					position, tokenIndex, depth = position890, tokenIndex890, depth890
					{
						position892, tokenIndex892, depth892 := position, tokenIndex, depth
						{
							position893, tokenIndex893, depth893 := position, tokenIndex, depth
							if buffer[position] != rune('"') {
								goto l894
							}
							position++
							goto l893
						l894:
							position, tokenIndex, depth = position893, tokenIndex893, depth893
							if buffer[position] != rune('\n') {
								goto l895
							}
							position++
							goto l893
						l895:
							position, tokenIndex, depth = position893, tokenIndex893, depth893
							if buffer[position] != rune('\\') {
								goto l892
							}
							position++
						}
					l893:
						goto l888
					l892:
						position, tokenIndex, depth = position892, tokenIndex892, depth892
					}
					if !matchDot() {
						goto l888
					}
				}
			l890:
				depth--
				add(ruleStringChar, position889)
			}
			return true
		l888:
			position, tokenIndex, depth = position888, tokenIndex888, depth888
			return false
		},
		/* 150 LBRK <- <('[' Spacing)> */
		func() bool {
			position896, tokenIndex896, depth896 := position, tokenIndex, depth
			{
				position897 := position
				depth++
				if buffer[position] != rune('[') {
					goto l896
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l896
				}
				depth--
				add(ruleLBRK, position897)
			}
			return true
		l896:
			position, tokenIndex, depth = position896, tokenIndex896, depth896
			return false
		},
		/* 151 RBRK <- <(']' Spacing)> */
		func() bool {
			position898, tokenIndex898, depth898 := position, tokenIndex, depth
			{
				position899 := position
				depth++
				if buffer[position] != rune(']') {
					goto l898
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l898
				}
				depth--
				add(ruleRBRK, position899)
			}
			return true
		l898:
			position, tokenIndex, depth = position898, tokenIndex898, depth898
			return false
		},
		/* 152 LPAR <- <('(' Spacing)> */
		func() bool {
			position900, tokenIndex900, depth900 := position, tokenIndex, depth
			{
				position901 := position
				depth++
				if buffer[position] != rune('(') {
					goto l900
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l900
				}
				depth--
				add(ruleLPAR, position901)
			}
			return true
		l900:
			position, tokenIndex, depth = position900, tokenIndex900, depth900
			return false
		},
		/* 153 RPAR <- <(')' Spacing)> */
		func() bool {
			position902, tokenIndex902, depth902 := position, tokenIndex, depth
			{
				position903 := position
				depth++
				if buffer[position] != rune(')') {
					goto l902
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l902
				}
				depth--
				add(ruleRPAR, position903)
			}
			return true
		l902:
			position, tokenIndex, depth = position902, tokenIndex902, depth902
			return false
		},
		/* 154 LWING <- <('{' Spacing)> */
		func() bool {
			position904, tokenIndex904, depth904 := position, tokenIndex, depth
			{
				position905 := position
				depth++
				if buffer[position] != rune('{') {
					goto l904
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l904
				}
				depth--
				add(ruleLWING, position905)
			}
			return true
		l904:
			position, tokenIndex, depth = position904, tokenIndex904, depth904
			return false
		},
		/* 155 RWING <- <('}' Spacing)> */
		func() bool {
			position906, tokenIndex906, depth906 := position, tokenIndex, depth
			{
				position907 := position
				depth++
				if buffer[position] != rune('}') {
					goto l906
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l906
				}
				depth--
				add(ruleRWING, position907)
			}
			return true
		l906:
			position, tokenIndex, depth = position906, tokenIndex906, depth906
			return false
		},
		/* 156 DOT <- <('.' Spacing)> */
		func() bool {
			position908, tokenIndex908, depth908 := position, tokenIndex, depth
			{
				position909 := position
				depth++
				if buffer[position] != rune('.') {
					goto l908
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l908
				}
				depth--
				add(ruleDOT, position909)
			}
			return true
		l908:
			position, tokenIndex, depth = position908, tokenIndex908, depth908
			return false
		},
		/* 157 PTR <- <('-' '>' Spacing)> */
		func() bool {
			position910, tokenIndex910, depth910 := position, tokenIndex, depth
			{
				position911 := position
				depth++
				if buffer[position] != rune('-') {
					goto l910
				}
				position++
				if buffer[position] != rune('>') {
					goto l910
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l910
				}
				depth--
				add(rulePTR, position911)
			}
			return true
		l910:
			position, tokenIndex, depth = position910, tokenIndex910, depth910
			return false
		},
		/* 158 INC <- <('+' '+' Spacing)> */
		func() bool {
			position912, tokenIndex912, depth912 := position, tokenIndex, depth
			{
				position913 := position
				depth++
				if buffer[position] != rune('+') {
					goto l912
				}
				position++
				if buffer[position] != rune('+') {
					goto l912
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l912
				}
				depth--
				add(ruleINC, position913)
			}
			return true
		l912:
			position, tokenIndex, depth = position912, tokenIndex912, depth912
			return false
		},
		/* 159 DEC <- <('-' '-' Spacing)> */
		func() bool {
			position914, tokenIndex914, depth914 := position, tokenIndex, depth
			{
				position915 := position
				depth++
				if buffer[position] != rune('-') {
					goto l914
				}
				position++
				if buffer[position] != rune('-') {
					goto l914
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l914
				}
				depth--
				add(ruleDEC, position915)
			}
			return true
		l914:
			position, tokenIndex, depth = position914, tokenIndex914, depth914
			return false
		},
		/* 160 AND <- <('&' !'&' Spacing)> */
		func() bool {
			position916, tokenIndex916, depth916 := position, tokenIndex, depth
			{
				position917 := position
				depth++
				if buffer[position] != rune('&') {
					goto l916
				}
				position++
				{
					position918, tokenIndex918, depth918 := position, tokenIndex, depth
					if buffer[position] != rune('&') {
						goto l918
					}
					position++
					goto l916
				l918:
					position, tokenIndex, depth = position918, tokenIndex918, depth918
				}
				if !_rules[ruleSpacing]() {
					goto l916
				}
				depth--
				add(ruleAND, position917)
			}
			return true
		l916:
			position, tokenIndex, depth = position916, tokenIndex916, depth916
			return false
		},
		/* 161 STAR <- <('*' !'=' Spacing)> */
		func() bool {
			position919, tokenIndex919, depth919 := position, tokenIndex, depth
			{
				position920 := position
				depth++
				if buffer[position] != rune('*') {
					goto l919
				}
				position++
				{
					position921, tokenIndex921, depth921 := position, tokenIndex, depth
					if buffer[position] != rune('=') {
						goto l921
					}
					position++
					goto l919
				l921:
					position, tokenIndex, depth = position921, tokenIndex921, depth921
				}
				if !_rules[ruleSpacing]() {
					goto l919
				}
				depth--
				add(ruleSTAR, position920)
			}
			return true
		l919:
			position, tokenIndex, depth = position919, tokenIndex919, depth919
			return false
		},
		/* 162 PLUS <- <('+' !('+' / '=') Spacing)> */
		func() bool {
			position922, tokenIndex922, depth922 := position, tokenIndex, depth
			{
				position923 := position
				depth++
				if buffer[position] != rune('+') {
					goto l922
				}
				position++
				{
					position924, tokenIndex924, depth924 := position, tokenIndex, depth
					{
						position925, tokenIndex925, depth925 := position, tokenIndex, depth
						if buffer[position] != rune('+') {
							goto l926
						}
						position++
						goto l925
					l926:
						position, tokenIndex, depth = position925, tokenIndex925, depth925
						if buffer[position] != rune('=') {
							goto l924
						}
						position++
					}
				l925:
					goto l922
				l924:
					position, tokenIndex, depth = position924, tokenIndex924, depth924
				}
				if !_rules[ruleSpacing]() {
					goto l922
				}
				depth--
				add(rulePLUS, position923)
			}
			return true
		l922:
			position, tokenIndex, depth = position922, tokenIndex922, depth922
			return false
		},
		/* 163 MINUS <- <('-' !('-' / '=' / '>') Spacing)> */
		func() bool {
			position927, tokenIndex927, depth927 := position, tokenIndex, depth
			{
				position928 := position
				depth++
				if buffer[position] != rune('-') {
					goto l927
				}
				position++
				{
					position929, tokenIndex929, depth929 := position, tokenIndex, depth
					{
						position930, tokenIndex930, depth930 := position, tokenIndex, depth
						if buffer[position] != rune('-') {
							goto l931
						}
						position++
						goto l930
					l931:
						position, tokenIndex, depth = position930, tokenIndex930, depth930
						if buffer[position] != rune('=') {
							goto l932
						}
						position++
						goto l930
					l932:
						position, tokenIndex, depth = position930, tokenIndex930, depth930
						if buffer[position] != rune('>') {
							goto l929
						}
						position++
					}
				l930:
					goto l927
				l929:
					position, tokenIndex, depth = position929, tokenIndex929, depth929
				}
				if !_rules[ruleSpacing]() {
					goto l927
				}
				depth--
				add(ruleMINUS, position928)
			}
			return true
		l927:
			position, tokenIndex, depth = position927, tokenIndex927, depth927
			return false
		},
		/* 164 TILDA <- <('~' Spacing)> */
		func() bool {
			position933, tokenIndex933, depth933 := position, tokenIndex, depth
			{
				position934 := position
				depth++
				if buffer[position] != rune('~') {
					goto l933
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l933
				}
				depth--
				add(ruleTILDA, position934)
			}
			return true
		l933:
			position, tokenIndex, depth = position933, tokenIndex933, depth933
			return false
		},
		/* 165 BANG <- <('!' !'=' Spacing)> */
		func() bool {
			position935, tokenIndex935, depth935 := position, tokenIndex, depth
			{
				position936 := position
				depth++
				if buffer[position] != rune('!') {
					goto l935
				}
				position++
				{
					position937, tokenIndex937, depth937 := position, tokenIndex, depth
					if buffer[position] != rune('=') {
						goto l937
					}
					position++
					goto l935
				l937:
					position, tokenIndex, depth = position937, tokenIndex937, depth937
				}
				if !_rules[ruleSpacing]() {
					goto l935
				}
				depth--
				add(ruleBANG, position936)
			}
			return true
		l935:
			position, tokenIndex, depth = position935, tokenIndex935, depth935
			return false
		},
		/* 166 DIV <- <('/' !'=' Spacing)> */
		func() bool {
			position938, tokenIndex938, depth938 := position, tokenIndex, depth
			{
				position939 := position
				depth++
				if buffer[position] != rune('/') {
					goto l938
				}
				position++
				{
					position940, tokenIndex940, depth940 := position, tokenIndex, depth
					if buffer[position] != rune('=') {
						goto l940
					}
					position++
					goto l938
				l940:
					position, tokenIndex, depth = position940, tokenIndex940, depth940
				}
				if !_rules[ruleSpacing]() {
					goto l938
				}
				depth--
				add(ruleDIV, position939)
			}
			return true
		l938:
			position, tokenIndex, depth = position938, tokenIndex938, depth938
			return false
		},
		/* 167 MOD <- <('%' !('=' / '>') Spacing)> */
		func() bool {
			position941, tokenIndex941, depth941 := position, tokenIndex, depth
			{
				position942 := position
				depth++
				if buffer[position] != rune('%') {
					goto l941
				}
				position++
				{
					position943, tokenIndex943, depth943 := position, tokenIndex, depth
					{
						position944, tokenIndex944, depth944 := position, tokenIndex, depth
						if buffer[position] != rune('=') {
							goto l945
						}
						position++
						goto l944
					l945:
						position, tokenIndex, depth = position944, tokenIndex944, depth944
						if buffer[position] != rune('>') {
							goto l943
						}
						position++
					}
				l944:
					goto l941
				l943:
					position, tokenIndex, depth = position943, tokenIndex943, depth943
				}
				if !_rules[ruleSpacing]() {
					goto l941
				}
				depth--
				add(ruleMOD, position942)
			}
			return true
		l941:
			position, tokenIndex, depth = position941, tokenIndex941, depth941
			return false
		},
		/* 168 LEFT <- <('<' '<' !'=' Spacing)> */
		func() bool {
			position946, tokenIndex946, depth946 := position, tokenIndex, depth
			{
				position947 := position
				depth++
				if buffer[position] != rune('<') {
					goto l946
				}
				position++
				if buffer[position] != rune('<') {
					goto l946
				}
				position++
				{
					position948, tokenIndex948, depth948 := position, tokenIndex, depth
					if buffer[position] != rune('=') {
						goto l948
					}
					position++
					goto l946
				l948:
					position, tokenIndex, depth = position948, tokenIndex948, depth948
				}
				if !_rules[ruleSpacing]() {
					goto l946
				}
				depth--
				add(ruleLEFT, position947)
			}
			return true
		l946:
			position, tokenIndex, depth = position946, tokenIndex946, depth946
			return false
		},
		/* 169 RIGHT <- <('>' '>' !'=' Spacing)> */
		func() bool {
			position949, tokenIndex949, depth949 := position, tokenIndex, depth
			{
				position950 := position
				depth++
				if buffer[position] != rune('>') {
					goto l949
				}
				position++
				if buffer[position] != rune('>') {
					goto l949
				}
				position++
				{
					position951, tokenIndex951, depth951 := position, tokenIndex, depth
					if buffer[position] != rune('=') {
						goto l951
					}
					position++
					goto l949
				l951:
					position, tokenIndex, depth = position951, tokenIndex951, depth951
				}
				if !_rules[ruleSpacing]() {
					goto l949
				}
				depth--
				add(ruleRIGHT, position950)
			}
			return true
		l949:
			position, tokenIndex, depth = position949, tokenIndex949, depth949
			return false
		},
		/* 170 LT <- <('<' !'=' Spacing)> */
		func() bool {
			position952, tokenIndex952, depth952 := position, tokenIndex, depth
			{
				position953 := position
				depth++
				if buffer[position] != rune('<') {
					goto l952
				}
				position++
				{
					position954, tokenIndex954, depth954 := position, tokenIndex, depth
					if buffer[position] != rune('=') {
						goto l954
					}
					position++
					goto l952
				l954:
					position, tokenIndex, depth = position954, tokenIndex954, depth954
				}
				if !_rules[ruleSpacing]() {
					goto l952
				}
				depth--
				add(ruleLT, position953)
			}
			return true
		l952:
			position, tokenIndex, depth = position952, tokenIndex952, depth952
			return false
		},
		/* 171 GT <- <('>' !'=' Spacing)> */
		func() bool {
			position955, tokenIndex955, depth955 := position, tokenIndex, depth
			{
				position956 := position
				depth++
				if buffer[position] != rune('>') {
					goto l955
				}
				position++
				{
					position957, tokenIndex957, depth957 := position, tokenIndex, depth
					if buffer[position] != rune('=') {
						goto l957
					}
					position++
					goto l955
				l957:
					position, tokenIndex, depth = position957, tokenIndex957, depth957
				}
				if !_rules[ruleSpacing]() {
					goto l955
				}
				depth--
				add(ruleGT, position956)
			}
			return true
		l955:
			position, tokenIndex, depth = position955, tokenIndex955, depth955
			return false
		},
		/* 172 LE <- <('<' '=' Spacing)> */
		func() bool {
			position958, tokenIndex958, depth958 := position, tokenIndex, depth
			{
				position959 := position
				depth++
				if buffer[position] != rune('<') {
					goto l958
				}
				position++
				if buffer[position] != rune('=') {
					goto l958
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l958
				}
				depth--
				add(ruleLE, position959)
			}
			return true
		l958:
			position, tokenIndex, depth = position958, tokenIndex958, depth958
			return false
		},
		/* 173 GE <- <('>' '=' Spacing)> */
		func() bool {
			position960, tokenIndex960, depth960 := position, tokenIndex, depth
			{
				position961 := position
				depth++
				if buffer[position] != rune('>') {
					goto l960
				}
				position++
				if buffer[position] != rune('=') {
					goto l960
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l960
				}
				depth--
				add(ruleGE, position961)
			}
			return true
		l960:
			position, tokenIndex, depth = position960, tokenIndex960, depth960
			return false
		},
		/* 174 EQUEQU <- <('=' '=' Spacing)> */
		func() bool {
			position962, tokenIndex962, depth962 := position, tokenIndex, depth
			{
				position963 := position
				depth++
				if buffer[position] != rune('=') {
					goto l962
				}
				position++
				if buffer[position] != rune('=') {
					goto l962
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l962
				}
				depth--
				add(ruleEQUEQU, position963)
			}
			return true
		l962:
			position, tokenIndex, depth = position962, tokenIndex962, depth962
			return false
		},
		/* 175 BANGEQU <- <('!' '=' Spacing)> */
		func() bool {
			position964, tokenIndex964, depth964 := position, tokenIndex, depth
			{
				position965 := position
				depth++
				if buffer[position] != rune('!') {
					goto l964
				}
				position++
				if buffer[position] != rune('=') {
					goto l964
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l964
				}
				depth--
				add(ruleBANGEQU, position965)
			}
			return true
		l964:
			position, tokenIndex, depth = position964, tokenIndex964, depth964
			return false
		},
		/* 176 HAT <- <('^' !'=' Spacing)> */
		func() bool {
			position966, tokenIndex966, depth966 := position, tokenIndex, depth
			{
				position967 := position
				depth++
				if buffer[position] != rune('^') {
					goto l966
				}
				position++
				{
					position968, tokenIndex968, depth968 := position, tokenIndex, depth
					if buffer[position] != rune('=') {
						goto l968
					}
					position++
					goto l966
				l968:
					position, tokenIndex, depth = position968, tokenIndex968, depth968
				}
				if !_rules[ruleSpacing]() {
					goto l966
				}
				depth--
				add(ruleHAT, position967)
			}
			return true
		l966:
			position, tokenIndex, depth = position966, tokenIndex966, depth966
			return false
		},
		/* 177 OR <- <('|' !'=' Spacing)> */
		func() bool {
			position969, tokenIndex969, depth969 := position, tokenIndex, depth
			{
				position970 := position
				depth++
				if buffer[position] != rune('|') {
					goto l969
				}
				position++
				{
					position971, tokenIndex971, depth971 := position, tokenIndex, depth
					if buffer[position] != rune('=') {
						goto l971
					}
					position++
					goto l969
				l971:
					position, tokenIndex, depth = position971, tokenIndex971, depth971
				}
				if !_rules[ruleSpacing]() {
					goto l969
				}
				depth--
				add(ruleOR, position970)
			}
			return true
		l969:
			position, tokenIndex, depth = position969, tokenIndex969, depth969
			return false
		},
		/* 178 ANDAND <- <('&' '&' Spacing)> */
		func() bool {
			position972, tokenIndex972, depth972 := position, tokenIndex, depth
			{
				position973 := position
				depth++
				if buffer[position] != rune('&') {
					goto l972
				}
				position++
				if buffer[position] != rune('&') {
					goto l972
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l972
				}
				depth--
				add(ruleANDAND, position973)
			}
			return true
		l972:
			position, tokenIndex, depth = position972, tokenIndex972, depth972
			return false
		},
		/* 179 OROR <- <('|' '|' Spacing)> */
		func() bool {
			position974, tokenIndex974, depth974 := position, tokenIndex, depth
			{
				position975 := position
				depth++
				if buffer[position] != rune('|') {
					goto l974
				}
				position++
				if buffer[position] != rune('|') {
					goto l974
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l974
				}
				depth--
				add(ruleOROR, position975)
			}
			return true
		l974:
			position, tokenIndex, depth = position974, tokenIndex974, depth974
			return false
		},
		/* 180 QUERY <- <('?' Spacing)> */
		func() bool {
			position976, tokenIndex976, depth976 := position, tokenIndex, depth
			{
				position977 := position
				depth++
				if buffer[position] != rune('?') {
					goto l976
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l976
				}
				depth--
				add(ruleQUERY, position977)
			}
			return true
		l976:
			position, tokenIndex, depth = position976, tokenIndex976, depth976
			return false
		},
		/* 181 COLON <- <(':' !'>' Spacing)> */
		func() bool {
			position978, tokenIndex978, depth978 := position, tokenIndex, depth
			{
				position979 := position
				depth++
				if buffer[position] != rune(':') {
					goto l978
				}
				position++
				{
					position980, tokenIndex980, depth980 := position, tokenIndex, depth
					if buffer[position] != rune('>') {
						goto l980
					}
					position++
					goto l978
				l980:
					position, tokenIndex, depth = position980, tokenIndex980, depth980
				}
				if !_rules[ruleSpacing]() {
					goto l978
				}
				depth--
				add(ruleCOLON, position979)
			}
			return true
		l978:
			position, tokenIndex, depth = position978, tokenIndex978, depth978
			return false
		},
		/* 182 SEMI <- <(';' Spacing)> */
		func() bool {
			position981, tokenIndex981, depth981 := position, tokenIndex, depth
			{
				position982 := position
				depth++
				if buffer[position] != rune(';') {
					goto l981
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l981
				}
				depth--
				add(ruleSEMI, position982)
			}
			return true
		l981:
			position, tokenIndex, depth = position981, tokenIndex981, depth981
			return false
		},
		/* 183 ELLIPSIS <- <('.' '.' '.' Spacing)> */
		func() bool {
			position983, tokenIndex983, depth983 := position, tokenIndex, depth
			{
				position984 := position
				depth++
				if buffer[position] != rune('.') {
					goto l983
				}
				position++
				if buffer[position] != rune('.') {
					goto l983
				}
				position++
				if buffer[position] != rune('.') {
					goto l983
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l983
				}
				depth--
				add(ruleELLIPSIS, position984)
			}
			return true
		l983:
			position, tokenIndex, depth = position983, tokenIndex983, depth983
			return false
		},
		/* 184 EQU <- <('=' !'=' Spacing)> */
		func() bool {
			position985, tokenIndex985, depth985 := position, tokenIndex, depth
			{
				position986 := position
				depth++
				if buffer[position] != rune('=') {
					goto l985
				}
				position++
				{
					position987, tokenIndex987, depth987 := position, tokenIndex, depth
					if buffer[position] != rune('=') {
						goto l987
					}
					position++
					goto l985
				l987:
					position, tokenIndex, depth = position987, tokenIndex987, depth987
				}
				if !_rules[ruleSpacing]() {
					goto l985
				}
				depth--
				add(ruleEQU, position986)
			}
			return true
		l985:
			position, tokenIndex, depth = position985, tokenIndex985, depth985
			return false
		},
		/* 185 STAREQU <- <('*' '=' Spacing)> */
		func() bool {
			position988, tokenIndex988, depth988 := position, tokenIndex, depth
			{
				position989 := position
				depth++
				if buffer[position] != rune('*') {
					goto l988
				}
				position++
				if buffer[position] != rune('=') {
					goto l988
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l988
				}
				depth--
				add(ruleSTAREQU, position989)
			}
			return true
		l988:
			position, tokenIndex, depth = position988, tokenIndex988, depth988
			return false
		},
		/* 186 DIVEQU <- <('/' '=' Spacing)> */
		func() bool {
			position990, tokenIndex990, depth990 := position, tokenIndex, depth
			{
				position991 := position
				depth++
				if buffer[position] != rune('/') {
					goto l990
				}
				position++
				if buffer[position] != rune('=') {
					goto l990
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l990
				}
				depth--
				add(ruleDIVEQU, position991)
			}
			return true
		l990:
			position, tokenIndex, depth = position990, tokenIndex990, depth990
			return false
		},
		/* 187 MODEQU <- <('%' '=' Spacing)> */
		func() bool {
			position992, tokenIndex992, depth992 := position, tokenIndex, depth
			{
				position993 := position
				depth++
				if buffer[position] != rune('%') {
					goto l992
				}
				position++
				if buffer[position] != rune('=') {
					goto l992
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l992
				}
				depth--
				add(ruleMODEQU, position993)
			}
			return true
		l992:
			position, tokenIndex, depth = position992, tokenIndex992, depth992
			return false
		},
		/* 188 PLUSEQU <- <('+' '=' Spacing)> */
		func() bool {
			position994, tokenIndex994, depth994 := position, tokenIndex, depth
			{
				position995 := position
				depth++
				if buffer[position] != rune('+') {
					goto l994
				}
				position++
				if buffer[position] != rune('=') {
					goto l994
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l994
				}
				depth--
				add(rulePLUSEQU, position995)
			}
			return true
		l994:
			position, tokenIndex, depth = position994, tokenIndex994, depth994
			return false
		},
		/* 189 MINUSEQU <- <('-' '=' Spacing)> */
		func() bool {
			position996, tokenIndex996, depth996 := position, tokenIndex, depth
			{
				position997 := position
				depth++
				if buffer[position] != rune('-') {
					goto l996
				}
				position++
				if buffer[position] != rune('=') {
					goto l996
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l996
				}
				depth--
				add(ruleMINUSEQU, position997)
			}
			return true
		l996:
			position, tokenIndex, depth = position996, tokenIndex996, depth996
			return false
		},
		/* 190 LEFTEQU <- <('<' '<' '=' Spacing)> */
		func() bool {
			position998, tokenIndex998, depth998 := position, tokenIndex, depth
			{
				position999 := position
				depth++
				if buffer[position] != rune('<') {
					goto l998
				}
				position++
				if buffer[position] != rune('<') {
					goto l998
				}
				position++
				if buffer[position] != rune('=') {
					goto l998
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l998
				}
				depth--
				add(ruleLEFTEQU, position999)
			}
			return true
		l998:
			position, tokenIndex, depth = position998, tokenIndex998, depth998
			return false
		},
		/* 191 RIGHTEQU <- <('>' '>' '=' Spacing)> */
		func() bool {
			position1000, tokenIndex1000, depth1000 := position, tokenIndex, depth
			{
				position1001 := position
				depth++
				if buffer[position] != rune('>') {
					goto l1000
				}
				position++
				if buffer[position] != rune('>') {
					goto l1000
				}
				position++
				if buffer[position] != rune('=') {
					goto l1000
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l1000
				}
				depth--
				add(ruleRIGHTEQU, position1001)
			}
			return true
		l1000:
			position, tokenIndex, depth = position1000, tokenIndex1000, depth1000
			return false
		},
		/* 192 ANDEQU <- <('&' '=' Spacing)> */
		func() bool {
			position1002, tokenIndex1002, depth1002 := position, tokenIndex, depth
			{
				position1003 := position
				depth++
				if buffer[position] != rune('&') {
					goto l1002
				}
				position++
				if buffer[position] != rune('=') {
					goto l1002
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l1002
				}
				depth--
				add(ruleANDEQU, position1003)
			}
			return true
		l1002:
			position, tokenIndex, depth = position1002, tokenIndex1002, depth1002
			return false
		},
		/* 193 HATEQU <- <('^' '=' Spacing)> */
		func() bool {
			position1004, tokenIndex1004, depth1004 := position, tokenIndex, depth
			{
				position1005 := position
				depth++
				if buffer[position] != rune('^') {
					goto l1004
				}
				position++
				if buffer[position] != rune('=') {
					goto l1004
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l1004
				}
				depth--
				add(ruleHATEQU, position1005)
			}
			return true
		l1004:
			position, tokenIndex, depth = position1004, tokenIndex1004, depth1004
			return false
		},
		/* 194 OREQU <- <('|' '=' Spacing)> */
		func() bool {
			position1006, tokenIndex1006, depth1006 := position, tokenIndex, depth
			{
				position1007 := position
				depth++
				if buffer[position] != rune('|') {
					goto l1006
				}
				position++
				if buffer[position] != rune('=') {
					goto l1006
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l1006
				}
				depth--
				add(ruleOREQU, position1007)
			}
			return true
		l1006:
			position, tokenIndex, depth = position1006, tokenIndex1006, depth1006
			return false
		},
		/* 195 COMMA <- <(',' Spacing)> */
		func() bool {
			position1008, tokenIndex1008, depth1008 := position, tokenIndex, depth
			{
				position1009 := position
				depth++
				if buffer[position] != rune(',') {
					goto l1008
				}
				position++
				if !_rules[ruleSpacing]() {
					goto l1008
				}
				depth--
				add(ruleCOMMA, position1009)
			}
			return true
		l1008:
			position, tokenIndex, depth = position1008, tokenIndex1008, depth1008
			return false
		},
		/* 196 EOT <- <!.> */
		func() bool {
			position1010, tokenIndex1010, depth1010 := position, tokenIndex, depth
			{
				position1011 := position
				depth++
				{
					position1012, tokenIndex1012, depth1012 := position, tokenIndex, depth
					if !matchDot() {
						goto l1012
					}
					goto l1010
				l1012:
					position, tokenIndex, depth = position1012, tokenIndex1012, depth1012
				}
				depth--
				add(ruleEOT, position1011)
			}
			return true
		l1010:
			position, tokenIndex, depth = position1010, tokenIndex1010, depth1010
			return false
		},
		nil,
	}
	p.rules = _rules
}
